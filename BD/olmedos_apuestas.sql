-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-08-2020 a las 16:22:18
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `olmedos_apuestas`
--
CREATE DATABASE IF NOT EXISTS `olmedos_apuestas` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `olmedos_apuestas`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores_apuestas`
--

CREATE TABLE `administradores_apuestas` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `apuesta_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores_torneos`
--

CREATE TABLE `administradores_torneos` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `torneo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuestas_extras`
--

CREATE TABLE `apuestas_extras` (
  `id` int(11) NOT NULL,
  `apuesta_base_id` int(11) DEFAULT NULL,
  `detalle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_complete` tinyint(1) NOT NULL,
  `valor` int(11) NOT NULL,
  `valor_detalle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuestas_fechas`
--

CREATE TABLE `apuestas_fechas` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `apuesta_base_id` int(11) DEFAULT NULL,
  `fechaModificacion_At` datetime DEFAULT NULL,
  `fecha_base_id` int(11) DEFAULT NULL,
  `desempate_fecha_id` int(11) DEFAULT NULL,
  `is_date_win` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuestas_participantes`
--

CREATE TABLE `apuestas_participantes` (
  `id` int(11) NOT NULL,
  `apuestas_base_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `puntos` int(11) NOT NULL,
  `pe` int(11) NOT NULL,
  `pr` int(11) NOT NULL,
  `pp` int(11) NOT NULL,
  `pts_extras` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_win` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuestas_partidos`
--

CREATE TABLE `apuestas_partidos` (
  `id` int(11) NOT NULL,
  `partido_fecha_id` int(11) DEFAULT NULL,
  `desempate_partido_id` int(11) DEFAULT NULL,
  `apuestas_fechas_id` int(11) DEFAULT NULL,
  `resultado_local` int(11) DEFAULT NULL,
  `resultado_visita` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuesta_base`
--

CREATE TABLE `apuesta_base` (
  `id` int(11) NOT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_limite` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `puntos_part_exactos` int(11) NOT NULL,
  `puntos_part_resultados` int(11) NOT NULL,
  `puntos_part_errados` int(11) NOT NULL,
  `apuesta_simbolica` double NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fechaCreacion_At` datetime NOT NULL,
  `fechaLimiteExtra_At` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuesta_extra_usuario`
--

CREATE TABLE `apuesta_extra_usuario` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `extra_id` int(11) DEFAULT NULL,
  `detalle` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clubes`
--

CREATE TABLE `clubes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_eliminacion`
--

CREATE TABLE `datos_eliminacion` (
  `id` int(11) NOT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `numero_fecha` int(11) DEFAULT NULL,
  `numero_partido` int(11) DEFAULT NULL,
  `tipo_local` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_partido_local` int(11) DEFAULT NULL,
  `tipo_visita` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_partido_visita` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_torneo`
--

CREATE TABLE `datos_torneo` (
  `id` int(11) NOT NULL,
  `cant_equipos` int(11) DEFAULT NULL,
  `cant_grupos` int(11) DEFAULT NULL,
  `equip_grupos` int(11) DEFAULT NULL,
  `forma_grupo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vuelta_grupo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clas_por_grupo` int(11) DEFAULT NULL,
  `clas_adic` int(11) DEFAULT NULL,
  `tipo_clasificacion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechas_adic` int(11) DEFAULT NULL,
  `comienzo_elim` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forma_elim` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `part_tercer_puesto` tinyint(1) DEFAULT NULL,
  `forma_final` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partido_desempate` tinyint(1) DEFAULT NULL,
  `gol_visitante` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `puntos_part_ganados` int(11) DEFAULT NULL,
  `puntos_part_empatados` int(11) DEFAULT NULL,
  `puntos_part_perdidos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desempate_datos`
--

CREATE TABLE `desempate_datos` (
  `id` int(11) NOT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `tipo_id` int(11) DEFAULT NULL,
  `cantidad_equipos` int(11) NOT NULL,
  `gol_visitante` tinyint(1) NOT NULL,
  `forma` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vuelta_grupo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_confirm` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desempate_datos_elimin`
--

CREATE TABLE `desempate_datos_elimin` (
  `id` int(11) NOT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `numero_fecha` int(11) NOT NULL,
  `numero_partido` int(11) NOT NULL,
  `tipo_local` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `num_partido_local` int(11) NOT NULL,
  `tipo_visita` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `num_partido_visita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desempate_fecha`
--

CREATE TABLE `desempate_fecha` (
  `id` int(11) NOT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `equipo_libre_id` int(11) DEFAULT NULL,
  `numero_fecha` int(11) NOT NULL,
  `nombre_fecha` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fechaInicio_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desempate_partido`
--

CREATE TABLE `desempate_partido` (
  `id` int(11) NOT NULL,
  `local_id` int(11) DEFAULT NULL,
  `visita_id` int(11) DEFAULT NULL,
  `numero_partido` int(11) NOT NULL,
  `valor_local` int(11) DEFAULT NULL,
  `valor_visita` int(11) DEFAULT NULL,
  `valor_definicion_local` int(11) DEFAULT NULL,
  `valor_definicion_visita` int(11) DEFAULT NULL,
  `fechaPartido_At` datetime DEFAULT NULL,
  `desempateFecha_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechas_base`
--

CREATE TABLE `fechas_base` (
  `id` int(11) NOT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `numero_fecha` int(11) NOT NULL,
  `nombre_fecha` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fechaInicio_At` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_fecha`
--

CREATE TABLE `grupo_fecha` (
  `id` int(11) NOT NULL,
  `equipo_libre_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `fecha_base_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `emisor_id` int(11) DEFAULT NULL,
  `receptor_id` int(11) DEFAULT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_at` datetime NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_leido` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido_fecha`
--

CREATE TABLE `partido_fecha` (
  `id` int(11) NOT NULL,
  `local_id` int(11) DEFAULT NULL,
  `visita_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `numero_partido` int(11) NOT NULL,
  `valor_local` int(11) DEFAULT NULL,
  `valor_visita` int(11) DEFAULT NULL,
  `valor_definicion_local` int(11) DEFAULT NULL,
  `valor_definicion_visita` int(11) DEFAULT NULL,
  `fechaPartido_At` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pizarra_mensaje`
--

CREATE TABLE `pizarra_mensaje` (
  `id` int(11) NOT NULL,
  `apuestas_base_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fechaInicio_At` datetime NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posiciones_desempate`
--

CREATE TABLE `posiciones_desempate` (
  `id` int(11) NOT NULL,
  `club_id` int(11) DEFAULT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `puntos` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `pe` int(11) NOT NULL,
  `pp` int(11) NOT NULL,
  `gf` int(11) NOT NULL,
  `gc` int(11) NOT NULL,
  `dg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posiciones_liga`
--

CREATE TABLE `posiciones_liga` (
  `id` int(11) NOT NULL,
  `club_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `torneo_id` int(11) DEFAULT NULL,
  `puntos` int(11) NOT NULL,
  `gf` int(11) NOT NULL,
  `gc` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `pe` int(11) NOT NULL,
  `pp` int(11) NOT NULL,
  `dg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recuperar_usuario`
--

CREATE TABLE `recuperar_usuario` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaLimite_At` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_torneo`
--

CREATE TABLE `tipo_torneo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_torneo`
--

INSERT INTO `tipo_torneo` (`id`, `nombre`) VALUES
(3, 'Eliminación'),
(1, 'Liga'),
(2, 'Liga y Eliminación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torneo_base`
--

CREATE TABLE `torneo_base` (
  `id` int(11) NOT NULL,
  `dato_id` int(11) DEFAULT NULL,
  `tipo_id` int(11) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_activo` tinyint(1) NOT NULL,
  `clase` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fechaCreacion_At` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores_apuestas`
--
ALTER TABLE `administradores_apuestas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FCB221D60322AC` (`role_id`),
  ADD KEY `IDX_FCB221A76ED395` (`user_id`),
  ADD KEY `IDX_FCB2218B4C8D7F` (`apuesta_id`);

--
-- Indices de la tabla `administradores_torneos`
--
ALTER TABLE `administradores_torneos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_13BB6C73D60322AC` (`role_id`),
  ADD KEY `IDX_13BB6C73A76ED395` (`user_id`),
  ADD KEY `IDX_13BB6C73A0139802` (`torneo_id`);

--
-- Indices de la tabla `apuestas_extras`
--
ALTER TABLE `apuestas_extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6232B58E7A249F84` (`apuesta_base_id`);

--
-- Indices de la tabla `apuestas_fechas`
--
ALTER TABLE `apuestas_fechas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A0A9DD93A76ED395` (`user_id`),
  ADD KEY `IDX_A0A9DD937A249F84` (`apuesta_base_id`),
  ADD KEY `IDX_A0A9DD9329809816` (`fecha_base_id`),
  ADD KEY `IDX_A0A9DD939F6C532B` (`desempate_fecha_id`);

--
-- Indices de la tabla `apuestas_participantes`
--
ALTER TABLE `apuestas_participantes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B704563892F73AF4` (`apuestas_base_id`),
  ADD KEY `IDX_B7045638A76ED395` (`user_id`);

--
-- Indices de la tabla `apuestas_partidos`
--
ALTER TABLE `apuestas_partidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3762B81093176286` (`partido_fecha_id`),
  ADD KEY `IDX_3762B8106BEF3CE0` (`desempate_partido_id`),
  ADD KEY `IDX_3762B8102886327` (`apuestas_fechas_id`);

--
-- Indices de la tabla `apuesta_base`
--
ALTER TABLE `apuesta_base`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9277F91F3A909126` (`nombre`),
  ADD KEY `IDX_9277F91FA0139802` (`torneo_id`),
  ADD KEY `IDX_9277F91FA76ED395` (`user_id`);

--
-- Indices de la tabla `apuesta_extra_usuario`
--
ALTER TABLE `apuesta_extra_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_74A98A0BA76ED395` (`user_id`),
  ADD KEY `IDX_74A98A0B2B959FC6` (`extra_id`);

--
-- Indices de la tabla `clubes`
--
ALTER TABLE `clubes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_A554B69F3A909126` (`nombre`);

--
-- Indices de la tabla `datos_eliminacion`
--
ALTER TABLE `datos_eliminacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BE9D3AC8A0139802` (`torneo_id`);

--
-- Indices de la tabla `datos_torneo`
--
ALTER TABLE `datos_torneo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `desempate_datos`
--
ALTER TABLE `desempate_datos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1DC492B2A0139802` (`torneo_id`),
  ADD KEY `IDX_1DC492B2A9276E6C` (`tipo_id`);

--
-- Indices de la tabla `desempate_datos_elimin`
--
ALTER TABLE `desempate_datos_elimin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DBC1656A0139802` (`torneo_id`);

--
-- Indices de la tabla `desempate_fecha`
--
ALTER TABLE `desempate_fecha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4DF704FFA0139802` (`torneo_id`),
  ADD KEY `IDX_4DF704FF7AB30A34` (`equipo_libre_id`);

--
-- Indices de la tabla `desempate_partido`
--
ALTER TABLE `desempate_partido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DFCF58465D5A2101` (`local_id`),
  ADD KEY `IDX_DFCF58464EA74B0E` (`visita_id`),
  ADD KEY `IDX_DFCF58468B18764D` (`desempateFecha_id`);

--
-- Indices de la tabla `fechas_base`
--
ALTER TABLE `fechas_base`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_790C6595A0139802` (`torneo_id`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grupo_fecha`
--
ALTER TABLE `grupo_fecha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B11CB6127AB30A34` (`equipo_libre_id`),
  ADD KEY `IDX_B11CB6129C833003` (`grupo_id`),
  ADD KEY `IDX_B11CB61229809816` (`fecha_base_id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6C929C806BDF87DF` (`emisor_id`),
  ADD KEY `IDX_6C929C80386D8D01` (`receptor_id`);

--
-- Indices de la tabla `partido_fecha`
--
ALTER TABLE `partido_fecha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BC568BE55D5A2101` (`local_id`),
  ADD KEY `IDX_BC568BE54EA74B0E` (`visita_id`),
  ADD KEY `IDX_BC568BE59C833003` (`grupo_id`);

--
-- Indices de la tabla `pizarra_mensaje`
--
ALTER TABLE `pizarra_mensaje`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_55BB9B8292F73AF4` (`apuestas_base_id`),
  ADD KEY `IDX_55BB9B82A76ED395` (`user_id`);

--
-- Indices de la tabla `posiciones_desempate`
--
ALTER TABLE `posiciones_desempate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6530932361190A32` (`club_id`),
  ADD KEY `IDX_65309323A0139802` (`torneo_id`);

--
-- Indices de la tabla `posiciones_liga`
--
ALTER TABLE `posiciones_liga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D3F8D7B561190A32` (`club_id`),
  ADD KEY `IDX_D3F8D7B59C833003` (`grupo_id`),
  ADD KEY `IDX_D3F8D7B5A0139802` (`torneo_id`);

--
-- Indices de la tabla `recuperar_usuario`
--
ALTER TABLE `recuperar_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_78C48BD0A76ED395` (`user_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_torneo`
--
ALTER TABLE `tipo_torneo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E95295E63A909126` (`nombre`);

--
-- Indices de la tabla `torneo_base`
--
ALTER TABLE `torneo_base`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_653B31103A909126` (`nombre`),
  ADD UNIQUE KEY `UNIQ_653B3110D72AD60F` (`dato_id`),
  ADD KEY `IDX_653B3110A9276E6C` (`tipo_id`),
  ADD KEY `IDX_653B311061190A32` (`club_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD KEY `IDX_8D93D649D60322AC` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores_apuestas`
--
ALTER TABLE `administradores_apuestas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `administradores_torneos`
--
ALTER TABLE `administradores_torneos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `apuestas_extras`
--
ALTER TABLE `apuestas_extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `apuestas_fechas`
--
ALTER TABLE `apuestas_fechas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `apuestas_participantes`
--
ALTER TABLE `apuestas_participantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `apuestas_partidos`
--
ALTER TABLE `apuestas_partidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `apuesta_base`
--
ALTER TABLE `apuesta_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `apuesta_extra_usuario`
--
ALTER TABLE `apuesta_extra_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clubes`
--
ALTER TABLE `clubes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `datos_eliminacion`
--
ALTER TABLE `datos_eliminacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `datos_torneo`
--
ALTER TABLE `datos_torneo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desempate_datos`
--
ALTER TABLE `desempate_datos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desempate_datos_elimin`
--
ALTER TABLE `desempate_datos_elimin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desempate_fecha`
--
ALTER TABLE `desempate_fecha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desempate_partido`
--
ALTER TABLE `desempate_partido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fechas_base`
--
ALTER TABLE `fechas_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grupo_fecha`
--
ALTER TABLE `grupo_fecha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `partido_fecha`
--
ALTER TABLE `partido_fecha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pizarra_mensaje`
--
ALTER TABLE `pizarra_mensaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posiciones_desempate`
--
ALTER TABLE `posiciones_desempate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posiciones_liga`
--
ALTER TABLE `posiciones_liga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `recuperar_usuario`
--
ALTER TABLE `recuperar_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_torneo`
--
ALTER TABLE `tipo_torneo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `torneo_base`
--
ALTER TABLE `torneo_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administradores_apuestas`
--
ALTER TABLE `administradores_apuestas`
  ADD CONSTRAINT `FK_FCB2218B4C8D7F` FOREIGN KEY (`apuesta_id`) REFERENCES `apuesta_base` (`id`),
  ADD CONSTRAINT `FK_FCB221A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_FCB221D60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `administradores_torneos`
--
ALTER TABLE `administradores_torneos`
  ADD CONSTRAINT `FK_13BB6C73A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`),
  ADD CONSTRAINT `FK_13BB6C73A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_13BB6C73D60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `apuestas_extras`
--
ALTER TABLE `apuestas_extras`
  ADD CONSTRAINT `FK_6232B58E7A249F84` FOREIGN KEY (`apuesta_base_id`) REFERENCES `apuesta_base` (`id`);

--
-- Filtros para la tabla `apuestas_fechas`
--
ALTER TABLE `apuestas_fechas`
  ADD CONSTRAINT `FK_A0A9DD9329809816` FOREIGN KEY (`fecha_base_id`) REFERENCES `fechas_base` (`id`),
  ADD CONSTRAINT `FK_A0A9DD937A249F84` FOREIGN KEY (`apuesta_base_id`) REFERENCES `apuesta_base` (`id`),
  ADD CONSTRAINT `FK_A0A9DD939F6C532B` FOREIGN KEY (`desempate_fecha_id`) REFERENCES `desempate_fecha` (`id`),
  ADD CONSTRAINT `FK_A0A9DD93A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `apuestas_participantes`
--
ALTER TABLE `apuestas_participantes`
  ADD CONSTRAINT `FK_B704563892F73AF4` FOREIGN KEY (`apuestas_base_id`) REFERENCES `apuesta_base` (`id`),
  ADD CONSTRAINT `FK_B7045638A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `apuestas_partidos`
--
ALTER TABLE `apuestas_partidos`
  ADD CONSTRAINT `FK_3762B8102886327` FOREIGN KEY (`apuestas_fechas_id`) REFERENCES `apuestas_fechas` (`id`),
  ADD CONSTRAINT `FK_3762B8106BEF3CE0` FOREIGN KEY (`desempate_partido_id`) REFERENCES `desempate_partido` (`id`),
  ADD CONSTRAINT `FK_3762B81093176286` FOREIGN KEY (`partido_fecha_id`) REFERENCES `partido_fecha` (`id`);

--
-- Filtros para la tabla `apuesta_base`
--
ALTER TABLE `apuesta_base`
  ADD CONSTRAINT `FK_9277F91FA0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`),
  ADD CONSTRAINT `FK_9277F91FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `apuesta_extra_usuario`
--
ALTER TABLE `apuesta_extra_usuario`
  ADD CONSTRAINT `FK_74A98A0B2B959FC6` FOREIGN KEY (`extra_id`) REFERENCES `apuestas_extras` (`id`),
  ADD CONSTRAINT `FK_74A98A0BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `datos_eliminacion`
--
ALTER TABLE `datos_eliminacion`
  ADD CONSTRAINT `FK_BE9D3AC8A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`);

--
-- Filtros para la tabla `desempate_datos`
--
ALTER TABLE `desempate_datos`
  ADD CONSTRAINT `FK_1DC492B2A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`),
  ADD CONSTRAINT `FK_1DC492B2A9276E6C` FOREIGN KEY (`tipo_id`) REFERENCES `tipo_torneo` (`id`);

--
-- Filtros para la tabla `desempate_datos_elimin`
--
ALTER TABLE `desempate_datos_elimin`
  ADD CONSTRAINT `FK_DBC1656A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`);

--
-- Filtros para la tabla `desempate_fecha`
--
ALTER TABLE `desempate_fecha`
  ADD CONSTRAINT `FK_4DF704FF7AB30A34` FOREIGN KEY (`equipo_libre_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_4DF704FFA0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`);

--
-- Filtros para la tabla `desempate_partido`
--
ALTER TABLE `desempate_partido`
  ADD CONSTRAINT `FK_DFCF58464EA74B0E` FOREIGN KEY (`visita_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_DFCF58465D5A2101` FOREIGN KEY (`local_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_DFCF58468B18764D` FOREIGN KEY (`desempateFecha_id`) REFERENCES `desempate_fecha` (`id`);

--
-- Filtros para la tabla `fechas_base`
--
ALTER TABLE `fechas_base`
  ADD CONSTRAINT `FK_790C6595A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`);

--
-- Filtros para la tabla `grupo_fecha`
--
ALTER TABLE `grupo_fecha`
  ADD CONSTRAINT `FK_B11CB61229809816` FOREIGN KEY (`fecha_base_id`) REFERENCES `fechas_base` (`id`),
  ADD CONSTRAINT `FK_B11CB6127AB30A34` FOREIGN KEY (`equipo_libre_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_B11CB6129C833003` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`);

--
-- Filtros para la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `FK_6C929C80386D8D01` FOREIGN KEY (`receptor_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_6C929C806BDF87DF` FOREIGN KEY (`emisor_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `partido_fecha`
--
ALTER TABLE `partido_fecha`
  ADD CONSTRAINT `FK_BC568BE54EA74B0E` FOREIGN KEY (`visita_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_BC568BE55D5A2101` FOREIGN KEY (`local_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_BC568BE59C833003` FOREIGN KEY (`grupo_id`) REFERENCES `grupo_fecha` (`id`);

--
-- Filtros para la tabla `pizarra_mensaje`
--
ALTER TABLE `pizarra_mensaje`
  ADD CONSTRAINT `FK_55BB9B8292F73AF4` FOREIGN KEY (`apuestas_base_id`) REFERENCES `apuesta_base` (`id`),
  ADD CONSTRAINT `FK_55BB9B82A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `posiciones_desempate`
--
ALTER TABLE `posiciones_desempate`
  ADD CONSTRAINT `FK_6530932361190A32` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_65309323A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`);

--
-- Filtros para la tabla `posiciones_liga`
--
ALTER TABLE `posiciones_liga`
  ADD CONSTRAINT `FK_D3F8D7B561190A32` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_D3F8D7B59C833003` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`),
  ADD CONSTRAINT `FK_D3F8D7B5A0139802` FOREIGN KEY (`torneo_id`) REFERENCES `torneo_base` (`id`);

--
-- Filtros para la tabla `recuperar_usuario`
--
ALTER TABLE `recuperar_usuario`
  ADD CONSTRAINT `FK_78C48BD0A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `torneo_base`
--
ALTER TABLE `torneo_base`
  ADD CONSTRAINT `FK_653B311061190A32` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`),
  ADD CONSTRAINT `FK_653B3110A9276E6C` FOREIGN KEY (`tipo_id`) REFERENCES `tipo_torneo` (`id`),
  ADD CONSTRAINT `FK_653B3110D72AD60F` FOREIGN KEY (`dato_id`) REFERENCES `datos_torneo` (`id`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649D60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
