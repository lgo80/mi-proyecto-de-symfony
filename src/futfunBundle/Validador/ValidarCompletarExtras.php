<?php

namespace futfunBundle\Validador;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use futfunBundle\Entity\PartidoFecha;

class ValidarCompletarExtras
{

    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar, "mensaje" => $mensaje);

        return $respuesta;

    }

    public function validarApuesta($apuesta)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($apuesta)) {

            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El nombre del torneo de apuestas no existe!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarParticipante($participante)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($participante)) {

            $isValid = false;
            $lugar = 'trabajo_dbbundle_apuestas_ver_tabla';
            $mensaje = '¡El usuario no esta inscripto en el torneo de apuestas!';
            $nombreError = 'mensaje-danger';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarAlgunoCompletado($extrasApuestas, $form)
    {
        $i = 0;

        foreach ($extrasApuestas as &$valorPart) {

            $i++;

            $extraUsuario = $form->get('extra' . $i)->getData();

            if (!empty($extraUsuario->getDetalle())) {

                return true;

            }

        }

        return false;

    }

    public function validarAdministrador($user, $apuesta)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $entro = false;
        $nombreError = '';

        if (isset($user)) {

            $administradores = $apuesta->getAdministradores();

            foreach ($administradores as &$valor) {

                if ($valor->getUser()->getUsername() == $user->getUsername()) {
                    $entro = true;
                }

            }
        }

        if (!$entro) {
            $isValid = false;
            $lugar = 'trabajo_dbbundle_apuestas_ver_tabla';
            $mensaje = '\n¡EL usuario logueado no es administrador!';
            $nombreError = 'mensaje-danger';
        }

        $respuesta = array("isValid" => $isValid,
            "lugar" => $lugar, "mensaje" => $mensaje,
            "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarFechaNoCumplida($apuesta, $em)
    {

        if ($apuesta->getFechaLimiteExtraAt() != null) {

            $ahora = new \DateTime();

            $fechaComienzo = $apuesta->getFechaLimiteExtraAt();

            return ($fechaComienzo < $ahora);

        }

        $torneo = $apuesta->getTorneo();

        $fechaBase = $em->getRepository('futfunBundle:FechasBase')
            ->devolverFechaDelTorneo($torneo, 1);

        if ($fechaBase != null) {

            $fechaComienzo = $fechaBase->getFechaInicioAt();

            if ($fechaComienzo != null) {

                $ahora = new \DateTime();

                if ($fechaComienzo > $ahora) {

                    return false;

                }

            } else {

                $isValid = $this->validarSinResultados(
                    $apuesta->getTorneo(), $em);

                return $isValid;

            }

        } else {

            return false;

        }

        return true;

    }

    public function validarSinResultados($torneo, $em)
    {

        $fechas = $em->getRepository('futfunBundle:FechasBase')
            ->devolverFechaDelTorneoAll($torneo);

        foreach ($fechas as &$fecha) {

            foreach ($fecha->getGrupoFechas() as &$grupo) {

                $partidos = $em->getRepository('futfunBundle:PartidoFecha')
                    ->devolverResultadosCompletados($grupo);

                if (count($partidos) > 0) {

                    return true;

                }

            }

        }

        return false;

    }

}
