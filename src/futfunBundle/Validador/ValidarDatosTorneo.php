<?php

namespace futfunBundle\Validador;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints as Assert;

class ValidarDatosTorneo
{

    /**
     * Esta funcion se especifica en validar si el valor por Get ingresado existe o no
     * @param $valor Es el valor del parametro que puede ser cualquier formato
     * @return array Devuelve los datos en un array si esta correcto o no el parametro entrante
     */
    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    /**
     * Esta funcion se especifica en validar si el torneo base ingresado existe o no
     * @param $torneo La entidad de torneoBase
     * @return array Devuelve los datos en un array si esta correcto o no el parametro entrante
     */
    public function validarTorneo($torneo)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($torneo == null) {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡No existe el torneo ingresado!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }
}
