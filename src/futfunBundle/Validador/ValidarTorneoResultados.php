<?php

namespace futfunBundle\Validador;

use futfunBundle\Entity\FechasBase;

class ValidarTorneoResultados
{

    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarTorneo($apuesta)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($apuesta)) {

            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El nombre del torneo no existe!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarAdministrador($torneo, $user)
    {

        $entro = false;

        if (isset($user)) {

            $administradores = $torneo->getAdministradores();

            foreach ($administradores as &$valor) {

                if ($valor->getUser()->getUsername()
                    == $user->getUsername()) {

                    $entro = true;
                    break;

                }

            }

        }

        return !$entro;

    }

    public function validarFecha($fecha, $em)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (!isset($fecha)) {


            $isValid = false;
            $lugar = 'trabajo_dbbundle_apuestas_ver_tabla';
            $mensaje = '¡La fecha de la apuesta ingresada no existe!';
            $nombreError = 'mensaje-danger';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarFechaLimite(FechasBase $fechaBase)
    {

        $fechaDeLaFecha = $fechaBase->getFechaInicioAt();

        if (!empty($fechaDeLaFecha)) {
            //$fechaDeLaFecha->add(new \DateInterval('PT5H'));

            $ahora = new \DateTime();

            if ($fechaDeLaFecha < $ahora) {

                return false;

            }
        }

        return true;

    }

    public function validarVacioDeAmbosResult($fechaBase, $form)
    {

        $i = 0;

        foreach ($fechaBase->getGrupoFechas() as &$grupo) {

            $i++;

            foreach ($grupo->getPartidoFechas() as &$partido) {

                $valorLocal = $form->get(
                    'valorLocal' . $i
                    . $partido->getNumeroPartido())->getData();
                $valorVisita = $form->get(
                    'valorVisita' . $i
                    . $partido->getNumeroPartido())->getData();

                if (is_numeric($valorLocal) && is_numeric($valorVisita)
                    && ((!is_numeric($partido->getValorLocal())
                            && !is_numeric($partido->getValorVisita()))
                        || (is_numeric($partido->getValorLocal())
                            && is_numeric($partido->getValorVisita())
                            && ($valorLocal != $partido->getValorLocal()
                                || $valorVisita != $partido->getValorVisita())))
                ) {

                    return true;

                }

            }
        }

        return false;

    }

}
