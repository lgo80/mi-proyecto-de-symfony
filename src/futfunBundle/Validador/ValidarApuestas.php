<?php

namespace futfunBundle\Validador;

use futfunBundle\Entity\ApuestaBase;
use futfunBundle\Entity\FechasBase;
use futfunBundle\Entity\TorneoBase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ValidarApuestas
{

    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarApuesta($apuesta)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($apuesta)) {

            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El nombre del torneo de apuestas no existe!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarParticipante($participante)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($participante)) {

            $isValid = false;
            $lugar = 'trabajo_dbbundle_apuestas_ver_tabla';
            $mensaje = '¡El usuario no esta inscripto en el torneo de apuestas!';
            $nombreError = 'mensaje-danger';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarFecha($fecha, $em)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (!isset($fecha)) {


            $isValid = false;
            $lugar = 'trabajo_dbbundle_apuestas_ver_tabla';
            $mensaje = '¡La fecha de la apuesta ingresada no existe!';
            $nombreError = 'mensaje-danger';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarFechaLimiteCompleta(
        $fechaBase, ApuestaBase $apuesta)
    {

        $isALgunTrue = false;
        $array_respuesta = [];

        $array_respuesta[0] = ($apuesta->getFechaLimite() == "fecha")
            ? $this->validarFechaLimite($fechaBase->getFechaInicioAt())
            : true;

        $cantidadGrupos = ($fechaBase instanceof FechasBase)
            ? count($fechaBase->getGrupoFechas()) : 1;

        for ($i = 1; $i <= $cantidadGrupos; $i++) {

            $array_partidos = [];
            $cantidadPartidos = ($fechaBase instanceof FechasBase)
                ? count($fechaBase->getGrupoFechas()[$i - 1]->getPartidoFechas())
                : count($fechaBase->getDesempatePartidos());

            for ($j = 1; $j <= $cantidadPartidos; $j++) {

                $partidoFecha = ($fechaBase instanceof FechasBase)
                    ? $fechaBase->getGrupoFechas()[$i - 1]->getPartidoFechas()[$j - 1]
                    : $fechaBase->getDesempatePartidos()[$j - 1];

                if ($partidoFecha->getLocal() == null ||
                    $partidoFecha->getVisita() == null
                ) {

                    $array_partidos[$j] = false;
                    continue;

                } elseif ($partidoFecha->getLocal()->getId() == "" ||
                    $partidoFecha->getVisita()->getId() == ""
                ) {

                    $array_partidos[$j] = false;
                    continue;

                }

                if (is_numeric($partidoFecha->getValorLocal()) &&
                    is_numeric($partidoFecha->getValorVisita())
                ) {

                    $array_partidos[$j] = false;
                    continue;

                }

                if ($apuesta->getFechaLimite() == "fecha" &&
                    $fechaBase->getFechaInicioAt() != null
                ) {

                    $array_partidos[$j] = $array_respuesta[0];

                    if ($array_partidos[$j]) {

                        $isALgunTrue = true;

                    }
                    continue;

                }

                $array_partidos[$j] = $this->validarFechaLimite(
                    $partidoFecha->getFechaPartidoAt());

                if ($array_partidos[$j]) {

                    $isALgunTrue = true;

                }

            }

            $array_respuesta[$i] = $array_partidos;

        }

        if (!$isALgunTrue) {

            $array_respuesta[0] = false;

        }

        return $array_respuesta;

    }

    private function validarFechaLimite($fechaEntrante)
    {

        if (!empty($fechaEntrante)) {

            //$fechaEntrante->add(new \DateInterval('PT5H'));

            $ahora = new \DateTime();

            return ($fechaEntrante > $ahora);
        }

        return true;

    }

    public function devolverErroresForm(
        $fechaBase, $array_habilitar, $form, Controller $controler)
    {

        $array_respuesta = [];
        $k = 0;
        $isTodoCompleto = true;
        $isNadaCompleto = true;

        $array_respuesta[0] = false;

        $cantidadGrupos = ($fechaBase instanceof FechasBase)
            ? count($fechaBase->getGrupoFechas()) : 1;

        for ($i = 0; $i < $cantidadGrupos; $i++) {

            $cantidadPartidos = ($fechaBase instanceof FechasBase)
                ? count($fechaBase->getGrupoFechas()[$i]->getPartidoFechas())
                : count($fechaBase->getDesempatePartidos());

            for ($j = 0; $j < $cantidadPartidos; $j++) {

                $partido = ($fechaBase instanceof FechasBase)
                    ? $fechaBase->getGrupoFechas()[$i]->getPartidoFechas()[$j]
                    : $fechaBase->getDesempatePartidos()[$j];
                $k++;
                if ($array_habilitar[$i + 1][$partido->getNumeroPartido()]) {

                    $apuestaFecha = $form
                        ->get('apuestaValor' . $k)
                        ->getData();

                    if (strval($apuestaFecha->getResultadoLocal()) == "" ||
                        strval($apuestaFecha->getResultadoVisita()) == ""
                    ) {
                        $isGrabar = false;
                        $isError = true;
                        $mensaje = $controler
                            ->get('translator')
                            ->trans("The values ​​of these fields were not completed well");
                        $isTodoCompleto = false;

                    } elseif (!is_int($apuestaFecha->getResultadoLocal())
                        || !is_int($apuestaFecha->getResultadoVisita())
                    ) {

                        $isGrabar = false;
                        $isError = true;
                        $mensaje = $controler
                            ->get('translator')
                            ->trans("The values ​​have to be a whole number");
                        $isTodoCompleto = false;

                    } elseif ($apuestaFecha->getResultadoLocal() < 0 ||
                        $apuestaFecha->getResultadoVisita() < 0
                    ) {


                        $isGrabar = false;
                        $isError = true;
                        $mensaje = $controler
                            ->get('translator')
                            ->trans("Values ​​must be a number greater than or equal to 0");
                        $isTodoCompleto = false;

                    } else {

                        $isGrabar = true;
                        $isError = false;
                        $mensaje = "";
                        $isNadaCompleto = false;

                    }

                } else {

                    $isGrabar = false;
                    $isError = false;
                    $mensaje = "";

                }

                $array_respuesta[$k]
                    = array("isGrabar" => $isGrabar, "isError" => $isError,
                    "mensaje" => $mensaje);

            }

        }

        if ($isTodoCompleto) {
            $array_respuesta[0] = array(
                "isGrabar" => true, "isError" => false,
                "mensaje" => $controler
                    ->get('translator')
                    ->trans("The data was saved correctly"));
        } else if ($isNadaCompleto) {
            $array_respuesta[0] = array(
                "isGrabar" => false, "isError" => true,
                "mensaje" => $controler
                    ->get('translator')
                    ->trans("There are no valid results to record"));
        } else {
            $array_respuesta[0] = array(
                "isGrabar" => true, "isError" => true,
                "mensaje" => $controler
                    ->get('translator')
                    ->trans("Only a few results were recorded!"));
        }
        return $array_respuesta;

    }

}
