<?php

namespace futfunBundle\Validador;

use futfunBundle\Entity\FechasBase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use futfunBundle\Entity\PartidoFecha;

class ValidarFechas
{

    /**
     * Esta funcion se especifica en validar el valor que ingresa por GET
     * @param $valor Es el valor que ingresa por Get
     * @return array Devuelve en un array los errores o no de la validacion
     */
    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar, "mensaje" => $mensaje);

        return $respuesta;

    }

    /**
     * Esta funcion se especifica en validar que el torneo ingresado existe y
     * el usuario logueado es el administrador
     * @param $user Es el usuario Logueado
     * @param $torneo Es la entidad Torneo Base
     * @return array Devuelve en un array los errores o no de la validacion
     */
    public function validarAdministrador($user, $torneo)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $entro = false;
        if (!isset($torneo)) {

            $isValid = false;
            $lugar = 'home1';
            $mensaje += '¡El nombre del torneo no existe!';

        } else {

            if (isset($user)) {
                $administradores = $torneo->getAdministradores();

                foreach ($administradores as &$valor) {

                    if ($valor->getUser()->getUsername() == $user->getUsername()) {
                        $entro = true;
                    }

                }
            }

            if (!$entro) {

                $isValid = false;
                $lugar = 'trabajo_dbbundle_torneos_ver_tabla';
                $mensaje = '\n¡EL usuario logueado no es administrador!';

            }

        }

        $respuesta = array("isValid" => $isValid,
            "lugar" => $lugar, "mensaje" => $mensaje);

        return $respuesta;

    }

    /**
     * Esta funcion se especifica en validar que no haya datos vacios y que esten dentro de lo permitido
     * @param $form Son los datos que el usuario completo en la vista de la creacion de la fecha
     * @param $datos los datos obtenidos del controlador que son necesarios para la validacion de la fecha
     * @param Controller $controller
     * @return array Devuelve un array con los errores o no de la validacion
     */
    public function validarFechaVacioYPermitido($form, $datos, Controller $controller)
    {

        $errorList = [];

        $isValid = true;

        for ($i = 1; $i <= $datos["cantGrupos"]; $i++) {

            if ($datos["isHabLibre"]) {

                $equipoLibre = $form->get('equipoLibre' . $i)->getData();
                $passwordConstarint = new Assert\NotBlank(
                    array('message' => "El equipo libre del grupo " . $i . " debe ser completado"));

                $error = $controller->get('validator')->validate(
                    $equipoLibre, $passwordConstarint);

                if (count($error) > 0) {
                    $isValid = false;
                }
                array_push($errorList, $error);

                $messageException1 = $controller->get('translator')->trans("The computer name of date ");
                $messageException2 = $controller->get('translator')->trans(" must be longer than 4 character");
                $messageException4 = $controller->get('translator')->trans(" must be less than 100 characters");
                $passwordConstarint = new Assert\Length(
                    array('min' => 4,
                        'max' => 100,
                        'minMessage' => $messageException1 . $i . $messageException2,
                        'maxMessage' => $messageException1 . $i . $messageException4
                    ));

                $error = $controller->get('validator')->validate(
                    $equipoLibre, $passwordConstarint);

                if (count($error) > 0) {
                    $isValid = false;
                }
                array_push($errorList, $error);

            }


        }

        return compact("isValid", "errorList");

    }

    /**
     * Esta funcion se especifica en validar que no haya datos repetidos
     * @param $form Son los datos que el usuario completo en la vista de la creacion de la fecha
     * @param $datos los datos obtenidos del controlador que son necesarios para la validacion de la fecha
     * @return array Devuelve un array con los errores o no de la validacion
     */
    public function validarFechaRepetido($form, $datos, $isLibre)
    {

        $errorList = [];
        $cantRepetidos = 0;


        for ($i = 1; $i <= $datos["cantGrupos"]; $i++) {

            for ($j = 1; $j <= $datos["cantPartidos"]; $j++) {

                $localStr = $form->get('localPartido' . $i . $j)->getData();
                $visitaStr = $form->get('visitaPartido' . $i . $j)->getData();

                if ($this->validarRepetidos($localStr, $visitaStr,
                    $datos, $j, $i, $form, $isLibre)
                ) {

                    $cantRepetidos++;

                }

            }

            if ($isLibre && $datos["isHabLibre"]) {

                $equipoLibre = $form->get('equipoLibre' . $i)->getData();

                if ($this->validarRepetidos(
                    $equipoLibre, "", $datos, 0, $i + 1, $form, $isLibre)
                ) {

                    $cantRepetidos++;

                }

            }


        }

        if ($cantRepetidos > 0) {

            array_push($errorList, 'Hay ' . $cantRepetidos . ' equipos repetidos');

        }

        return $errorList;

    }

    /**
     * Esta funcion se especifica en devolver un valor booleano si los equipos se encuentran repetidos en
     * los demas campos.
     * @param $local Es un string con el nombre del equipo local
     * @param $visita Es un string con el nombre del equipo visitante
     * @param $datos los datos obtenidos del controlador que son necesarios para la validacion de la fecha
     * @param $partido es un integer con el numero de partidos
     * @param $grupo es un integer con el numero de grupo
     * @param $form Son los datos que el usuario completo en la vista de la creacion de la fecha
     * @return bool Devuelve false si no esta repetido y true si lo esta
     */
    public function validarRepetidos($local, $visita, $datos,
                                     $partido, $grupo, $form, $isLibre)
    {

        $isRepit = false;
        if ($local == $visita) {

            $isRepit = true;
        }

        for ($i = $grupo; $i <= $datos["cantGrupos"]; $i++) {

            $part = ($i > $grupo) ? 1 : $partido + 1;

            for ($j = $part; $j <= $datos["cantPartidos"]; $j++) {

                $localStr = $form->get('localPartido' . $i . $j)->getData();
                $visitaStr = $form->get('visitaPartido' . $i . $j)->getData();

                if ($local == $localStr || $local == $visitaStr
                ) {

                    $isRepit = true;

                }

                if ($visita == $localStr || $visita == $visitaStr
                ) {

                    $isRepit = true;

                }


            }

            if ($isLibre && $datos["isHabLibre"]) {

                $equipoLibre = $form->get('equipoLibre' . $i)->getData();

                if ($local == $equipoLibre
                    || $visita == $equipoLibre
                ) {

                    $isRepit = true;

                }

            }


        }

        return $isRepit;

    }

    public function validarEquiposFaltantes($form, $datos)
    {

        $i = 1;
        $isComplete = false;
        for ($j = 1; $j <= $datos["cantPartidos"]; $j++) {

            $localStr = $form->get('localPartido' . $i . $j)->getData();
            $visitaStr = $form->get('visitaPartido' . $i . $j)->getData();

            $cadenaLocal = substr($localStr, 0, strripos($localStr, " "));
            $cadenaVisita = substr($visitaStr, 0, strripos($visitaStr, " "));

            if (($cadenaLocal == "Ganador del partido" && $cadenaVisita == "Ganador del partido")
                || ($cadenaLocal != "Ganador del partido" && $cadenaVisita != "Ganador del partido")
            ) {

                $isComplete = true;

            }

        }


        $errorList = array("isValid" => $isComplete, "mensaje" => "¡No se puede Modificar!
            Debe haber algun partido con los 2 equipos completos reales");

        return $errorList;

    }

    /**
     * Esta funcion se especifica en validar que el valor que se ingresa por GET
     * del numero de fecha sea un entero y mayor a 0
     * @param $valor Es el valor que ingresa por Get
     * @return array Devuelve en un array los errores o no de la validacion
     */
    public function validarNumeroFecha($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';

        $numero = intval($valor);

        if ($numero <= 0) {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El numero de fecha tiene que ser entero y mayor a 1!';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar, "mensaje" => $mensaje);

        return $respuesta;

    }

    public function isFechaCompletada($fecha, $em)
    {

        $isVacio = true;

        $cantidadGrupos = ($fecha instanceof FechasBase)
            ? count($fecha->getGrupoFechas())
            : 1;

        for ($i = 0; $i < $cantidadGrupos; $i++) {

            $parametroBusqueda = ($fecha instanceof FechasBase)
                ? $fecha->getGrupoFechas()[$i]
                : $fecha;

            $partidosFecha = ($fecha instanceof FechasBase)
                ? $em
                    ->getRepository("futfunBundle:PartidoFecha")
                    ->devolverResultadosCompletados($parametroBusqueda)
                : $em
                    ->getRepository("futfunBundle:DesempatePartido")
                    ->devolverDesempateFechaXFechaCompletados(
                        $parametroBusqueda);

            if (!empty($partidosFecha)) {

                $isVacio = false;
                break;

            }

        }

        return $isVacio;

    }

    public function isFechaNoCompletada($fecha, $em)
    {

        $isVacio = true;

        $cantidadGrupos = ($fecha instanceof FechasBase)
            ? count($fecha->getGrupoFechas())
            : 1;

        for ($i = 0; $i < $cantidadGrupos; $i++) {

            $parametroBusqueda = ($fecha instanceof FechasBase)
                ? $fecha->getGrupoFechas()[$i]
                : $fecha;

            $partidosFecha = ($fecha instanceof FechasBase)
                ? $em
                    ->getRepository("futfunBundle:PartidoFecha")
                    ->devolverResultadosNoCompletados($parametroBusqueda)
                : $em
                    ->getRepository("futfunBundle:DesempatePartido")
                    ->devolverDesempateFechaXFechaNoCompletados(
                        $parametroBusqueda);

            if (!empty($partidosFecha)) {

                $isVacio = false;
                break;

            }

        }

        return $isVacio;

    }

}
