<?php

namespace futfunBundle\Validador;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints as Assert;

class ValidarDesempateTabla
{

    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarTorneo($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($valor)) {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡No exite el torneo ingresado!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarPosiciones($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($valor)) {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡No hay equipos ingresados!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

}
