<?php

namespace futfunBundle\Validador\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Class ContainsAlphanumeric
 * @package Trabajo\bdBundle\Validador\Constraints
 * @Annotation
 */
class ContainsAlphanumeric extends Constraint
{

    public $message = 'The string "{{ string }}" contains an illegal character: it can only contain letters or numbers.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
