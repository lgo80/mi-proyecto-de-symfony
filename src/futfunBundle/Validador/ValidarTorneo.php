<?php

namespace futfunBundle\Validador;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints as Assert;

class ValidarTorneo
{
    public function validar($form, Controller $controller)
    {

        $tipo = $form->get('tipoTorneo')->getData();
        $errorList = [];

        $dato = $form->get('dato')->getData();

        if ($tipo->getNombre() == 'Liga y Eliminación' || $tipo->getNombre() == 'Liga') {

            if ($dato->getFormaGrupo() == null) {
                array_push($errorList, 'La forma del grupo no puede ser nula');
            }

            if ($dato->getFormaGrupo() == "idayvuelta" && $dato->getVueltaGrupo() == null) {
                array_push($errorList, 'La vuelta del grupo no puede ser nula');
            }

            if (!is_numeric($dato->getPuntosPartGanados()) && !is_numeric($dato->getPuntosPartEmpatados())
                && !is_numeric($dato->getPuntosPartPerdidos())
            ) {
                array_push($errorList, 'Los puntos por los partidos tienen que ser completados');
            }

        }

        if ($tipo->getNombre() == 'Liga y Eliminación') {

            if ($dato->getCantGrupos() == null) {
                array_push($errorList, 'La cantidad de grupos no puede ser nula');
            }

            if ($dato->getEquipGrupos() == null) {
                array_push($errorList, 'La cantidad de equipos por grupo no puede ser nula');
            }

            if ($dato->getClasPorGrupo() == null) {
                array_push($errorList, 'La cantidad de clasificados por grupo no puede ser nula');
            }


        }

        if ($tipo->getNombre() == 'Liga y Eliminación' || $tipo->getNombre() == 'Eliminación') {

            if ($dato->getFormaElim() == null) {
                array_push($errorList, 'La forma de la eliminacion no puede ser nula');
            }

            if ($dato->getFormaFinal() == null) {
                array_push($errorList, 'La forma de la final no puede ser nula');
            }

        }

        return $errorList;

    }

}
