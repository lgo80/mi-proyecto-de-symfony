<?php

namespace futfunBundle\Validador;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints as Assert;

class ValidarCrearFechasDesempate
{

    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarTorneo($torneo)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($torneo)) {

            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El nombre del torneo no existe!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarDesempate($desempate)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($desempate == null) {

            $isValid = false;
            $lugar = 'trabajo_dbbundle_torneos_ver_tabla';
            $mensaje = '¡No hay desempate en este torneo!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validar($form)
    {

        $tipo = $form->get('tipoTorneo')->getData();
        $forma = $form->get('forma')->getData();
        $formaVuelta = $form->get('vueltaGrupo')->getData();

        $errorList = [];

        if ($tipo->getNombre() == "Liga" && $forma == "idayvuelta"
            && $formaVuelta == ""
        ) {

            array_push($errorList, 'Debe elegir el tipo de vuelta de la liga');

        }

        return $errorList;

    }

    public function validarAdministrador($desempate, $user)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (isset($user)) {

            $administradores = $desempate->getTorneo()->getAdministradores();

            foreach ($administradores as &$valor) {

                if ($valor->getUser()->getUsername() == $user->getUsername()) {
                    $entro = true;
                }

            }

        }

        if (!$entro) {
            $isValid = false;
            $lugar = 'trabajo_dbbundle_torneos_ver_tabla';
            $mensaje += '\n¡EL usuario logueado no es administrador!';
        }


        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    /**
     * Esta funcion se especifica en validar que el valor que se ingresa por GET
     * del numero de fecha sea un entero y mayor a 0
     * @param $valor Es el valor que ingresa por Get
     * @return array Devuelve en un array los errores o no de la validacion
     */
    public function validarNumeroFecha($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';

        $numero = intval($valor);

        if ($numero <= 0) {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El numero de fecha tiene que ser entero y mayor a 1!';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar, "mensaje" => $mensaje);

        return $respuesta;

    }

    public function validarFechaVacioYPermitido($form, $datos,
                                                Controller $controller)
    {

        $errorList = [];

        $isValid = true;

        if ($datos["isHabLibre"]) {

            $equipoLibre = $form->get('equipoLibre')->getData();
            $passwordConstarint = new Assert\NotBlank(
                array('message' => "El equipo libre debe ser completado"));

            $error = $controller->get('validator')->validate(
                $equipoLibre, $passwordConstarint);

            if (count($error) > 0) {
                $isValid = false;
            }
            array_push($errorList, $error);

            $messageException1 = $controller->get('translator')->trans("The computer name of date ");
            $messageException2 = $controller->get('translator')->trans(" must be longer than 4 character");
            $messageException4 = $controller->get('translator')->trans(" must be less than 100 characters");
            $passwordConstarint = new Assert\Length(
                array('min' => 4,
                    'max' => 100,
                    'minMessage' => $messageException1 . $messageException2,
                    'maxMessage' => $messageException1 . $messageException4
                ));

            $error = $controller->get('validator')->validate(
                $equipoLibre, $passwordConstarint);

            if (count($error) > 0) {
                $isValid = false;
            }
            array_push($errorList, $error);

        }

        return compact("isValid", "errorList");

    }

    /**
     * Esta funcion se especifica en validar que no haya datos repetidos
     * @param $form Son los datos que el usuario completo en la vista de la creacion de la fecha
     * @param $datos los datos obtenidos del controlador que son necesarios para la validacion de la fecha
     * @return array Devuelve un array con los errores o no de la validacion
     */
    public function validarFechaRepetido($form, $datos, $isLibre)
    {

        $errorList = [];
        $cantRepetidos = 0;

        for ($j = 1; $j <= $datos["cantPartidos"]; $j++) {

            $localStr = $form->get('localPartido' . $j)->getData();
            $visitaStr = $form->get('visitaPartido' . $j)->getData();

            if ($this->validarRepetidos(
                $localStr, $visitaStr, $datos, $j, $form, $isLibre)
            ) {

                $cantRepetidos++;

            }

        }

        if (!$isLibre && $datos["isHabLibre"]) {

            $equipoLibre = $form->get('equipoLibre')->getData();

            if ($this->validarRepetidos(
                $equipoLibre, "", $datos, 0, $form, $isLibre)
            ) {

                $cantRepetidos++;

            }

        }


        if ($cantRepetidos > 0) {

            array_push($errorList, 'Hay ' . $cantRepetidos . ' equipos repetidos');

        }

        return $errorList;

    }

    /**
     * Esta funcion se especifica en devolver un valor booleano si los equipos se encuentran repetidos en
     * los demas campos.
     * @param $local Es un string con el nombre del equipo local
     * @param $visita Es un string con el nombre del equipo visitante
     * @param $datos los datos obtenidos del controlador que son necesarios para la validacion de la fecha
     * @param $partido es un integer con el numero de partidos
     * @param $grupo es un integer con el numero de grupo
     * @param $form Son los datos que el usuario completo en la vista de la creacion de la fecha
     * @return bool Devuelve false si no esta repetido y true si lo esta
     */
    public function validarRepetidos($local, $visita, $datos,
                                     $partido, $form, $isLibre)
    {

        $isRepit = false;

        if ($local == $visita) {

            $isRepit = true;

        }

        for ($j = $partido + 1; $j <= $datos["cantPartidos"]; $j++) {

            $localStr = $form->get('localPartido' . $j)->getData();
            $visitaStr = $form->get('visitaPartido' . $j)->getData();

            if ($local == $localStr || $local == $visitaStr
            ) {

                $isRepit = true;

            }

            if ($visita == $localStr || $visita == $visitaStr
            ) {

                $isRepit = true;

            }


        }


        if (!$isLibre && $datos["isHabLibre"]) {

            $equipoLibre = $form->get('equipoLibre')->getData();

            if ($local == $equipoLibre
                || $visita == $equipoLibre
            ) {

                $isRepit = true;

            }

        }

        return $isRepit;

    }

}
