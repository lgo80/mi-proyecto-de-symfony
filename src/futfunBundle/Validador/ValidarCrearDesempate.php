<?php

namespace futfunBundle\Validador;

use futfunBundle\Entity\FechasBase;

class ValidarCrearDesempate
{

    public function validarGet($valor)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($valor == "") {
            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡Faltan Valores por get!';
            $nombreError = '';
        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarTorneo($torneo)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if (empty($torneo)) {

            $isValid = false;
            $lugar = 'home1';
            $mensaje = '¡El nombre del torneo no existe!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validarDesempate($desempate)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';

        if ($desempate == null) {

            $isValid = false;
            $lugar = 'trabajo_dbbundle_torneos_ver_tabla';
            $mensaje = '¡No hay desempate en este torneo!';
            $nombreError = '';

        }

        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

    public function validar($form)
    {

        $tipo = $form->get('tipoTorneo')->getData();
        $forma = $form->get('forma')->getData();
        $formaVuelta = $form->get('vueltaGrupo')->getData();

        $errorList = [];

        if ($tipo->getNombre() == "Liga" && $forma == "idayvuelta"
            && $formaVuelta == ""
        ) {

            array_push($errorList, 'Debe elegir el tipo de vuelta de la liga');

        }

        return $errorList;

    }

    public function validarAdministrador($desempate, $user)
    {

        $isValid = true;
        $lugar = '';
        $mensaje = '';
        $nombreError = '';
        $entro = false;

        if (isset($user)) {

            $administradores = $desempate->getTorneo()->getAdministradores();

            foreach ($administradores as &$valor) {

                if ($valor->getUser()->getUsername() == $user->getUsername()) {
                    $entro = true;
                }

            }

        }

        if (!$entro) {
            $isValid = false;
            $lugar = 'trabajo_dbbundle_torneos_ver_tabla';
            $mensaje += '\n¡EL usuario logueado no es administrador!';
        }


        $respuesta = array("isValid" => $isValid, "lugar" => $lugar,
            "mensaje" => $mensaje, "nombreError" => $nombreError);

        return $respuesta;

    }

}
