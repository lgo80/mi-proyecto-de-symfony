<?php

namespace futfunBundle\Repository;

use futfunBundle\Entity\RecuperarUsuario;
use futfunBundle\Entity\User;
use futfunBundle\Servicios\ServFuncionesUtiles;

/**
 * RecuperarUsuarioRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RecuperarUsuarioRepository extends \Doctrine\ORM\EntityRepository
{

    public function alta(
        User $user)
    {

        $em = $this->getEntityManager();

        $recupero = $em->getRepository("futfunBundle:RecuperarUsuario")
            ->findByUser($user);
        $recuperarUsuario = (empty($recupero) or $recupero == null)
            ? new RecuperarUsuario() : $recupero[0];
        $sevicio = new ServFuncionesUtiles();

        $salt = $sevicio->generateRandomString(5);
        $recuperarUsuario->setSalt($salt);
        $recuperarUsuario->setUser($user);
        $recuperarUsuario->setToken(
            md5($salt . $user->getId()));
        $fecha = new \DateTime();
        $fecha->add(new \DateInterval('PT30M'));
        $recuperarUsuario->setFechaLimiteAt($fecha);
        $recuperarUsuario->setIsActive(true);
        $em->persist($recuperarUsuario);
        $em->flush();

        return $recuperarUsuario->getToken();

    }

    public function modificar(
        User $user)
    {

        $em = $this->getEntityManager();

        $recupero = $em->getRepository("futfunBundle:RecuperarUsuario")
            ->findByUser($user);

        $recupero = $recupero[0];
        $recupero->setIsActive(false);

        $em->persist($recupero);
        $em->flush();

    }

    public function validarRecupero(User $user, $token)
    {

        $fechaActual = new \DateTime();
        $arrayRecupero = $this->getEntityManager()
            ->createQuery(
                'SELECT ru FROM futfunBundle:RecuperarUsuario ru
                        WHERE ru.user = :user AND ru.token = :token
                        AND ru.fechaLimiteAt >= :fechaActual AND
                        ru.isActive = true')
            ->setParameter('user', $user)
            ->setParameter('token', $token)
            ->setParameter('fechaActual', $fechaActual)
            ->getResult();

        return !empty($arrayRecupero)
            ? $arrayRecupero[0] : null;

    }
}
