<?php

namespace futfunBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * DesempateDatosEliminRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DesempateDatosEliminRepository extends EntityRepository
{

    /**
     * Esta funcion se especifica en grabar o actualizar una fecha base
     * @param $datosEliminacion Es una entidad de FechaBase
     * @param $em
     */
    public function grabarNuevo($desempateDatosElim)
    {

        $this->getEntityManager()
            ->persist($desempateDatosElim);

    }

    public function devolverDatosXTorneoXFechaYPartido(
        $torneo, $numeroFecha, $numPartido)
    {

        $datos = $this->getEntityManager()
            ->createQuery(
                'SELECT dde FROM futfunBundle:DesempateDatosElimin dde
                        WHERE dde.torneo = :nombre AND dde.numeroFecha = :numeroFecha
                        AND dde.numeroPartido = :numeroPartido')
            ->setParameter('nombre', $torneo)
            ->setParameter('numeroFecha', $numeroFecha)
            ->setParameter('numeroPartido', $numPartido)
            ->getResult();

        return (!empty($datos)) ? $datos[0] : null;

    }

    public function devolverDatosXTorneoYFecha($torneo, $numeroFecha)
    {

        $datos = $this->getEntityManager()
            ->createQuery(
                'SELECT dde FROM futfunBundle:DesempateDatosElimin dde
                        WHERE dde.torneo = :nombre AND dde.numeroFecha = :numeroFecha
                        ORDER BY dde.numeroPartido')
            ->setParameter('nombre', $torneo)
            ->setParameter('numeroFecha', $numeroFecha)
            ->getResult();

        return $datos;

    }

}
