<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarFechas;
use futfunBundle\Validador\ValidarTorneoTabla;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServTorneoTabla
{

    public function validarDatos($idTorneo, $em, Controller $controller)
    {

        $validar = new ValidarTorneoTabla();

        $respuesta = $validar->validarGet($idTorneo);

        if ($respuesta["isValid"]) {

            $torneo = $em->getRepository('futfunBundle:TorneoBase')->find($idTorneo);

            $respuesta = $validar->validarTorneo($torneo);

            if ($respuesta["isValid"]) {

                $posiciones = $em->getRepository('futfunBundle:PosicionesLiga')
                    ->devolverPosicionesOrderPts($torneo);

                $respuesta = $validar->validarPosiciones($posiciones);

                $titulo = $controller->get('translator')
                    ->trans("View positions");

                $nombreTorneoLugar = "tablatorneo";

            }

        }

        return compact("respuesta", "torneo", "posiciones",
            "titulo", "nombreTorneoLugar");

    }

}