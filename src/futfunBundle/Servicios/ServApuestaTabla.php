<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\ApuestaBase;
use futfunBundle\Entity\FechasBase;
use futfunBundle\Validador\ValidarApuestaTabla;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServApuestaTabla
{

    public function validarDatos(
        $idApuesta, $em, Controller $controller, $filtro, $numFecha)
    {

        $validar = new ValidarApuestaTabla();

        $respuesta = $validar->validarGet($idApuesta);

        if ($respuesta["isValid"]) {

            $apuesta = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->find($idApuesta);

            $respuesta = $validar
                ->validarTorneo($apuesta);

            if ($respuesta["isValid"]) {

                $fechas = $this
                    ->devolverFechasApuestaCompletas(
                        $apuesta, $em);

                $fechaActual = $this
                    ->devolverFechaActual($fechas);

                $posiciones = $this
                    ->devolverPosicionesCorrespondiente(
                        $apuesta, $filtro, $numFecha, $em, $controller
                    );

                $isWinDate = $this->devolverIdWinDate(
                    $apuesta, $numFecha, $em);

                $titulo = $controller->get('translator')
                    ->trans("View positions");

                $nombreTorneoLugar = "tablaapuestas";

            }

        }

        return compact("respuesta", "apuesta", "posiciones",
            "nombreTorneoLugar", "titulo", "fechas",
            "fechaActual", "isWinDate");

    }

    private function devolverPosicionesCorrespondiente(
        ApuestaBase $apuesta, $filtro, $numFecha,
        $em, Controller $controller
    )
    {

        $arrayRespuesta = null;
        if (empty($filtro) || $filtro == "general") {

            $arrayRespuesta = array(
                "nombre" => $controller->get('translator')
                    ->trans("General"),
                "array" => $em
                    ->getRepository('futfunBundle:ApuestasParticipantes')
                    ->getByApuestaBase($apuesta)
            );

        } elseif ($filtro == "f" || $filtro == "df") {

            $fechaCompletada = $this->validarFechaApuesta(
                $apuesta, $numFecha, $em, $filtro);

            if ($fechaCompletada != false) {

                $arrayRespuesta = array(
                    "nombre" => ($filtro == "f")
                        ? utf8_encode("Fecha n� " . $fechaCompletada->getNumeroFecha())
                        : utf8_encode("Fecha desempate n� " . $fechaCompletada->getNumeroFecha()),
                    "array" => $em
                        ->getRepository('futfunBundle:ApuestasParticipantes')
                        ->getPosicionesFechaParticular($apuesta, $fechaCompletada)
                );

            } else {

                $arrayRespuesta = array(
                    "nombre" => $controller->get('translator')
                        ->trans("General"),
                    "array" => $em
                        ->getRepository('futfunBundle:ApuestasParticipantes')
                        ->getByApuestaBase($apuesta)
                );

            }

        } else {

            $arrayRespuesta = array(
                "nombre" => $controller->get('translator')
                    ->trans("General"),
                "array" => $em
                    ->getRepository('futfunBundle:ApuestasParticipantes')
                    ->getByApuestaBase($apuesta)
            );

        }

        return $arrayRespuesta;

    }

    private function devolverFechasApuestaCompletas(
        ApuestaBase $apuesta, $em)
    {

        $arrayRespuesta = [];

        $fechas = $em
            ->getRepository('futfunBundle:FechasBase')
            ->devolverFechaDelTorneoAll(
                $apuesta->getTorneo());

        $arrayRespuesta = $this
            ->agregarFechasAlArray(
                $arrayRespuesta, $fechas, $em);

        $desempate = $em
            ->getRepository('futfunBundle:DesempateDatos')
            ->devolverXTorneo($apuesta->getTorneo());

        if ($desempate != null) {

            $fechasDesempate = $em
                ->getRepository('futfunBundle:DesempateFecha')
                ->devolverFechaDelTorneoAll(
                    $apuesta->getTorneo());

            $arrayRespuesta = $this
                ->agregarFechasAlArray(
                    $arrayRespuesta, $fechasDesempate, $em);

        }

        return $arrayRespuesta;

    }

    private function validarFechaApuesta(
        ApuestaBase $apuesta, $numFecha, $em, $filtro
    )
    {

        $fecha = ($filtro == "f")
            ? $em
                ->getRepository('futfunBundle:FechasBase')
                ->devolverFechaDelTorneo(
                    $apuesta->getTorneo(), $numFecha
                )
            : $em
                ->getRepository('futfunBundle:DesempateFecha')
                ->devolverFechaDelDesempate($apuesta->getTorneo(), $numFecha
                );

        if (!empty($fecha) && $fecha != null) {

            $validar = new ValidarFechas();

            $isVacia = $validar
                ->isFechaCompletada(
                    $fecha, $em
                );

            return (!$isVacia) ? $fecha : false;

        } else {

            return false;

        }

    }

    private function agregarFechasAlArray(
        $arrayRespuesta, $fechas, $em)
    {

        foreach ($fechas as &$fecha) {

            $validar = new ValidarFechas();

            $isVacio = $validar
                ->isFechaCompletada(
                    $fecha, $em
                );

            if (!$isVacio) {

                $arrayRespuesta[] = array(
                    "numFecha" => $fecha->getNumeroFecha(),
                    "tipoFecha" => ($fecha instanceof FechasBase)
                        ? "f" : "df",
                    "valor" => ($fecha instanceof FechasBase)
                        ? utf8_encode("Fecha n� " . $fecha->getNumeroFecha())
                        : utf8_encode("Fecha desempate n� " . $fecha->getNumeroFecha()));

            } else {

                break;

            }

        }

        return $arrayRespuesta;

    }

    private function devolverFechaActual($fechas)
    {


        if (empty($fechas)) {

            return array(
                "numFecha" => 1,
                "tipoFecha" => "f",
                "valor" => "Fecha n� 1"
            );

        } else {

            $fechaActual = $fechas[count($fechas) - 1];

            return array(
                "numFecha" => $fechaActual["numFecha"],
                "tipoFecha" => $fechaActual["tipoFecha"],
                "valor" => $fechaActual["valor"]
            );

        }

    }

    private function devolverIdWinDate($apuesta, $numeroFecha, $em)
    {

        if ($numeroFecha) {

            $fechaBase = $em
                ->getRepository('futfunBundle:FechasBase')
                ->devolverFechaDelTorneo($apuesta->getTorneo(), $numeroFecha);

            $apuestaFecha = $em
                ->getRepository('futfunBundle:ApuestasFechas')
                ->devolverXApuestaXWinDate(
                    $apuesta, $fechaBase, true);

            return ($apuestaFecha)
                ? $apuestaFecha->getUser()->getId() : -1;
        }

        return 0;

    }

}