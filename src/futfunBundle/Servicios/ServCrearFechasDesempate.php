<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\DesempateDatos;
use futfunBundle\Validador\ValidarCrearFechasDesempate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServCrearFechasDesempate
{

    public function devolverDatos(
        $idTorneo, $em, $userLogueado, $numeroFecha,
        $direccion, Controller $controller)
    {

        $validar = new ValidarCrearFechasDesempate();

        $respuesta = $validar->validarGet($idTorneo);

        if ($respuesta["isValid"]) {

            $torneo = $em->getRepository('futfunBundle:TorneoBase')
                ->findById($idTorneo);

            $respuesta = $validar->validarTorneo($torneo);

            if ($respuesta["isValid"]) {

                $desempateDatos = $em->getRepository('futfunBundle:DesempateDatos')
                    ->devolverXTorneo($torneo);

                $respuesta = $validar->validarDesempate($desempateDatos);

                if ($respuesta["isValid"]) {

                    if ($desempateDatos->getIsConfirm()) {

                        $respuesta = $validar->validarAdministrador(
                            $desempateDatos, $userLogueado);

                        if ($respuesta["isValid"]) {

                            $fecha = $em->getRepository('futfunBundle:DesempateFecha')
                                ->devolverFechaCorrespondiente(
                                    $numeroFecha, $desempateDatos, $validar, $direccion);

                            $cantPartidos = $em->getRepository('futfunBundle:DesempateDatos')
                                ->devolverCantidadPartidos($desempateDatos, $fecha);

                            $fechaVuelta = $em->getRepository('futfunBundle:DesempateFecha')
                                ->devolverFechaVuelta($desempateDatos, $fecha);


                            $ultimaFechaIda = $em->getRepository('futfunBundle:DesempateDatos')
                                ->devolverNumeroFechaLast($desempateDatos);

                            $equipos = $em->getRepository('futfunBundle:PosicionesDesempate')
                                ->devolverEquiposDelDesempate(
                                    $desempateDatos, $fecha, $em);

                            $isHabLibre = (($desempateDatos->getCantidadEquipos() % 2) != 0);

                            $titulo = $controller->get('translator')
                                ->trans("Create a tiebreak date");

                            $nombreTorneoLugar = "crearfechadesempate";

                            $botonGuardar = $controller->get('translator')
                                ->trans("Save Date");
                        }

                    } else {

                        $respuesta = array("isValid" => false,
                            "lugar" => 'trabajo_dbbundle_torneos_crear_desempate',
                            "mensaje" => '�El torneo no esta confirmado!',
                            "nombreError" => "mensaje-danger");

                    }

                }

            }


        }

        return compact("respuesta", "desempateDatos", "fecha", "isHabLibre",
            "cantPartidos", "fechaVuelta", "equipos", "ultimaFechaIda",
            "titulo", "nombreTorneoLugar", "botonGuardar");

    }

}