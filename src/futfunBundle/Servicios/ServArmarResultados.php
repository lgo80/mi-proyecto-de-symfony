<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarApuestas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServArmarResultados
{

    public function validarDatos(
        $idApuesta, $em, $userLogueado, $numeroFecha,
        $direccion, Controller $controller)
    {

        $validar = new ValidarApuestas();

        $respuesta = $validar->validarGet($idApuesta);

        if ($respuesta["isValid"]) {

            $apuesta = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->find($idApuesta);

            $respuesta = $validar->validarApuesta($apuesta);

            if ($respuesta["isValid"]) {

                $participante = $em->getRepository('futfunBundle:ApuestasParticipantes')
                    ->getByParticipant($apuesta, $userLogueado);

                $respuesta = $validar->validarParticipante($participante);

                if ($respuesta["isValid"]) {

                    $fechas = $em
                        ->getRepository('futfunBundle:ApuestasFechas')
                        ->devolverFechasParaApuestas($apuesta);

                    if ($fechas != null) {

                        $cantFechas = $em
                            ->getRepository('futfunBundle:TorneoBase')
                            ->devolverCantidadFechasTotales(
                                $apuesta->getTorneo());

                        $fechaBase = $em
                            ->getRepository('futfunBundle:ApuestasFechas')
                            ->devolverFechaApuesta($numeroFecha,
                                $apuesta, $direccion,
                                $cantFechas, $userLogueado);

                        $apuestaFecha = $em
                            ->getRepository('futfunBundle:ApuestasFechas')
                            ->devolverFechaArmada($fechaBase, $userLogueado,
                                $apuesta);

                        $isModificar = $validar
                            ->validarFechaLimiteCompleta(
                                $fechaBase, $apuesta);

                        $isUltimaFecha = $em
                            ->getRepository('futfunBundle:ApuestasFechas')
                            ->verificarUltimaFecha($fechaBase,
                                $apuesta->getTorneo(), $cantFechas);

                        $serviciosUtiles = new ServFuncionesUtiles();
                        $arrayFechas = $serviciosUtiles
                            ->formatearFechaEspanol($fechaBase);

                        $titulo = $controller->get('translator')
                            ->trans("Complete results");

                        $nombreTorneoLugar = "CompletarResultadoApuestas";

                        $botonEditar = $controller->get('translator')
                            ->trans("Edit result");

                        $botonGuardar = $controller->get('translator')
                            ->trans("Save Results");

                    } else {

                        $respuesta = array("isValid" => false,
                            "lugar" => 'trabajo_dbbundle_apuestas_ver_tabla',
                            "mensaje" => '�No hay fechas creadas para completar la fecha de la apuestas todavia!',
                            "nombreError" => "mensaje-danger");

                    }

                }

            }


        }

        return compact("respuesta", "apuesta", "numeroFecha",
            "fechaBase", "fechas", "cantFechas", "apuestaFecha",
            "isModificar", "isUltimaFecha", "arrayFechas", "titulo",
            "nombreTorneoLugar", "botonGuardar", "botonEditar");

    }

    public function obtenerDatos($apuesta, $numeroFecha, $em)
    {


    }

}