<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarDatosTorneo;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServDatosApuesta
{

    public function validarPrimera(
        $idApuesta, $em, Controller $controller)
    {

        $validar = new ValidarDatosTorneo();

        $respuesta = $validar->validarGet($idApuesta);

        $fechas = null;

        if ($respuesta["isValid"]) {

            $apuesta = $em->getRepository('futfunBundle:ApuestaBase')->find($idApuesta);

            if ($respuesta["isValid"]) {

                $fechas = $em->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneo($apuesta, 1);

                $participantes = $em->getRepository('futfunBundle:ApuestasParticipantes')
                    ->getByApuestaBase($apuesta);

                $titulo = $controller->get('translator')
                    ->trans("See bet data");

                $nombreTorneoLugar = "datosapuesta";

            }

        }

        return compact("respuesta", "apuesta", "fechas", "participantes",
            "titulo", "nombreTorneoLugar");

    }

}