<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\FechasBase;
use futfunBundle\Validador\ValidarApuestaTabla;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServFuncionesUtiles
{

    public function armarFechaYHora($fecha, $hora)
    {

        if ((!empty($fecha) && !empty($hora))) {

            return new \DateTime($fecha . $hora);

        } elseif (!empty($fecha)) {

            return new \DateTime($fecha . "01:00 AM");

        } else {

            return null;

        }

    }

    public function formatearFechaEspanol($fechaBase)
    {

        $date = $fechaBase->getFechaInicioAt();
        $array_Respuesta = [];

        $array_Respuesta[0] = $this
            ->formatearFechaEspanolUnica($date);

        $cantGrupos = ($fechaBase instanceof FechasBase)
            ? count($fechaBase->getGrupoFechas())
            : 1;

        for ($i = 1; $i <= $cantGrupos; $i++) {

            $array_partido = [];

            $cantPartidos = ($fechaBase instanceof FechasBase)
                ? count($fechaBase->getGrupoFechas()[$i - 1]
                    ->getPartidoFechas())
                : count($fechaBase->getDesempatePartidos());

            for ($j = 1; $j <= $cantPartidos; $j++) {

                $partido = ($fechaBase instanceof FechasBase)
                    ? $fechaBase->getGrupoFechas()[$i - 1]
                        ->getPartidoFechas()[$j - 1]
                    : $fechaBase->getDesempatePartidos()[$j - 1];

                $date = $partido->getFechaPartidoAt();

                $array_partido[$j] = $this
                    ->formatearFechaEspanolUnica($date);

            }

            $array_Respuesta[$i] = $array_partido;

        }

        return $array_Respuesta;

    }

    private function devolverNombresEspanol()
    {

        $array_Respuesta = [];
        $array_dia = [];
        $array_mes = [];

        $array_dia[1] = "Lunes";
        $array_dia[2] = "Martes";
        $array_dia[3] = utf8_encode("Mi�rcoles");
        $array_dia[4] = "Jueves";
        $array_dia[5] = "Viernes";
        $array_dia[6] = utf8_encode("S�bado");
        $array_dia[7] = "Domingo";

        $array_Respuesta["dia"] = $array_dia;

        $array_mes["01"] = "Enero";
        $array_mes["02"] = "Febrero";
        $array_mes["03"] = "Marzo";
        $array_mes["04"] = "Abril";
        $array_mes["05"] = "Mayo";
        $array_mes["06"] = "Junio";
        $array_mes["07"] = "Julio";
        $array_mes["08"] = "Agosto";
        $array_mes["09"] = "Septiembre";
        $array_mes["10"] = "Octubre";
        $array_mes["11"] = "Noviembre";
        $array_mes["12"] = "Diciembre";

        $array_Respuesta["mes"] = $array_mes;

        return $array_Respuesta;
    }

    private function armarFechaEspanol(
        $diaSemana, $diaNumero, $mes, $anno, $hora)
    {

        $array_espanol = $this->devolverNombresEspanol();

        return $array_espanol["dia"][$diaSemana] . " " .
        $diaNumero . " de " . $array_espanol["mes"][$mes]
        . " del " . $anno . " a las " . $hora;

    }

    public function formatearFechaEspanolUnica($date)
    {

        return ($date != null)
            ? $this->armarFechaEspanol(
                $date->format('N'), $date->format('d'),
                $date->format('m'), $date->format('Y'),
                $date->format('H:i:s')
            ) : null;

    }

    public function generateRandomString($length) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

}