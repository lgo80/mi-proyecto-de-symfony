<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\PartidoFecha;
use futfunBundle\Validador\ValidarApuestas;
use futfunBundle\Entity\FechasBase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServVerApuestas
{

    public function validarDatos(
        $idApuesta, $em, $userEntrante, $numeroFecha,
        $direccion, $usuarioLogueado, Controller $controller)
    {

        $validar = new ValidarApuestas();

        $respuesta = $validar->validarGet($idApuesta);

        if ($respuesta["isValid"]) {

            $apuesta = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->find($idApuesta);

            $respuesta = $validar->validarApuesta($apuesta);

            if ($respuesta["isValid"]) {

                $fechas = $em
                    ->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneoAll($apuesta->getTorneo());

                if (!empty($fechas)) {

                    $participantes = $em
                        ->getRepository('futfunBundle:ApuestasParticipantes')
                        ->getByApuestaBase($apuesta);

                    if (!empty($participantes)) {

                        $cantFechas = $em
                            ->getRepository('futfunBundle:TorneoBase')
                            ->devolverCantidadFechasTotales(
                                $apuesta->getTorneo());

                        $fechaBase = $em
                            ->getRepository('futfunBundle:FechasBase')
                            ->devolverFechaEntrante($numeroFecha,
                                $apuesta->getTorneo(), $direccion,
                                $cantFechas);

                        if (empty($fechaBase)) {

                            $fechaBase = $em
                                ->getRepository('futfunBundle:FechasBase')
                                ->devolverFechaDelTorneo(
                                    $apuesta->getTorneo(), 1);

                        }

                        $usuarioDeLaApuesta = $this->obtenerUsuarioApuesta(
                            $apuesta, $em, $userEntrante, $usuarioLogueado,
                            $participantes);

                        $fechaBase = (is_array($fechaBase))
                            ? $fechaBase[0] : $fechaBase;

                        $resultados = $this->obtenerResultadosApuesta(
                            $apuesta, $em, $fechaBase,
                            $usuarioDeLaApuesta, $usuarioLogueado);

                        $titulo = $controller->get('translator')
                            ->trans("See results");

                        $nombreTorneoLugar = "verResultadoApuestas";

                        $serviciosUtiles = new ServFuncionesUtiles();
                        $arrayFechas = $serviciosUtiles
                            ->formatearFechaEspanol($fechaBase);

                    } else {

                        $respuesta = array("isValid" => false,
                            "lugar" => 'trabajo_dbbundle_apuestas_ver_tabla',
                            "mensaje" => '�No hay fechas creadas para completar la fecha de la apuestas todavia!',
                            "nombreError" => "mensaje-danger");

                    }


                } else {

                    $respuesta = array("isValid" => false,
                        "lugar" => 'trabajo_dbbundle_apuestas_ver_tabla',
                        "mensaje" => '�No hay fechas creadas para completar la fecha de la apuestas todavia!',
                        "nombreError" => "mensaje-danger");

                }

            }


        }

        return compact("respuesta", "apuesta", "numeroFecha", "fechaBase", "fechas",
            "cantFechas", "participantes", "resultados", "usuarioDeLaApuesta",
            "titulo", "nombreTorneoLugar", "arrayFechas");

    }

    public function obtenerResultadosApuesta(
        $apuesta, $em, $fechaBase, $userABuscar, $userLogueado)
    {

        $validar = new ValidarApuestas();

        $isModificar = $validar
            ->validarFechaLimiteCompleta(
                $fechaBase, $apuesta);

        $resultados = [];

        $cantidadGrupos = ($fechaBase instanceof FechasBase)
            ? count($fechaBase->getGrupoFechas()) : 1;

        $apuestaFecha = ($fechaBase instanceof FechasBase)
            ? $em
                ->getRepository('futfunBundle:ApuestasFechas')
                ->devolverXApuestaXUserXFecha($apuesta,
                    $userABuscar, $fechaBase)
            : $em
                ->getRepository('futfunBundle:ApuestasFechas')
                ->devolverXApuestaXUserXDesempate($apuesta,
                    $userABuscar, $fechaBase);

        $k = 0;
        $ndT = false;
        $sdT = false;
        $ptsT = false;
        $sumaTotal = 0;

        for ($i = 1; $i <= $cantidadGrupos; $i++) {

            $grupoR = [];

            $cantidadPartidos = ($fechaBase instanceof FechasBase)
                ? count($fechaBase->getGrupoFechas()[$i - 1]->getPartidoFechas())
                : count($fechaBase);

            $ndG = false;
            $sdG = false;
            $ptsG = false;
            $sumaGrupo = 0;

            for ($j = 1; $j <= $cantidadPartidos; $j++) {

                $k++;

                $partido = ($fechaBase instanceof FechasBase)
                    ? $fechaBase->getGrupoFechas()[$i - 1]->getPartidoFechas()[$j - 1]
                    : $fechaBase->getDesempatePartidos()[$j - 1];

                $real = (is_numeric($partido->getValorLocal()))
                    ? $partido->getValorLocal() . " - " .
                    $partido->getValorVisita()
                    : "s/c";

                if ($isModificar[$i][$partido->getNumeroPartido()]) {

                    if ($userLogueado == null || $userLogueado->getUsername()
                        != $userABuscar->getUsername()
                    ) {

                        $apuestaPart = "n/d";
                        $puntos = "n/d";
                        $ndG = true;

                    } else {

                        if ($apuestaFecha != null) {

                            $apuestaPartido = $apuestaFecha
                                ->getApuestasPartidos()[$k - 1];

                            if (is_numeric($apuestaPartido->getResultadoLocal())
                                && is_numeric($apuestaPartido->getResultadoVisita())
                            ) {

                                $apuestaPart = $apuestaPartido->getResultadoLocal()
                                    . " - " . $apuestaPartido->getResultadoVisita();

                                if ($real != "s/c") {

                                    $puntos = $apuestaPartido->getPuntos();
                                    $sumaGrupo += $puntos;
                                    $ptsG = true;

                                } else {

                                    $puntos = "s/d";
                                    $sdG = true;

                                }

                            } else {

                                $apuestaPart = "s/c";
                                $puntos = "s/d";
                                $sdG = true;

                            }

                        } else {

                            $apuestaPart = "s/c";
                            $puntos = "s/d";
                            $sdG = true;

                        }

                    }

                } else {

                    if ($apuestaFecha != null) {

                        $apuestaPartido = $apuestaFecha
                            ->getApuestasPartidos()[$k - 1];

                        if (is_numeric($apuestaPartido->getResultadoLocal())
                            && is_numeric($apuestaPartido->getResultadoVisita())
                        ) {

                            $apuestaPart = $apuestaPartido->getResultadoLocal()
                                . " - " . $apuestaPartido->getResultadoVisita();

                            if ($real != "s/c") {

                                $puntos = $apuestaPartido->getPuntos();
                                $sumaGrupo += $puntos;
                                $ptsG = true;

                            } else {
                                $puntos = "s/d";
                                $sdG = true;

                            }

                        } else {

                            $apuestaPart = "s/c";
                            $puntos = "s/d";
                            $sdG = true;

                        }

                    } else {

                        $apuestaPart = "n/c";
                        $puntos = "s/d";
                        $sdG = true;

                    }

                }

                $partidoArmado = $this->devolverPartido(
                    $em, $partido, $fechaBase);

                $partidoR = array(
                    "partido" => $partidoArmado,
                    "real" => $real,
                    "apuesta" => $apuestaPart,
                    "puntos" => $puntos
                );

                $grupoR[$partido->getNumeroPartido()] = $partidoR;

            }

            if (!$ptsG) {

                $sumaGrupo = ($sdG) ? "s/d" : "n/d";

                if ($sdG) {
                    $sdT = true;
                }

            } else {

                $sumaTotal += $sumaGrupo;
                $ptsT = true;

            }

            $resultados[$i] = array(
                "grupoDatos" => $grupoR, "sumaGrupo" => $sumaGrupo);

        }

        if (!$ptsT) {

            $sumaTotal = ($sdT) ? "s/d" : "n/d";

        }

        $resultados[0] = $sumaTotal;

        return $resultados;

    }

    public function obtenerUsuarioApuesta(
        $apuesta, $em, $userEntrante, $usuarioLogueado, $participantes)
    {

        if ($userEntrante != "") {

            $userABuscar = $em->getRepository('futfunBundle:User')
                ->getByUsername($userEntrante);

        } elseif ($usuarioLogueado != "") {

            $participante = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByParticipant($apuesta, $usuarioLogueado);

            if (!empty($participante)) {

                $userABuscar = $usuarioLogueado;

            } else {

                $userABuscar = $participantes[0]->getUser();

            }

        } else {

            $userABuscar = $participantes[0]->getUser();

        }

        return $userABuscar;

    }

    private function devolverPartido($em, $partidoFecha, $fechaBase)
    {

        $numFecha = $fechaBase->getNumeroFecha();
        $torneo = $fechaBase->getTorneo();

        if ($partidoFecha->getLocal() == null ||
            $partidoFecha->getVisita() == null
        ) {

            $datosElimin = ($partidoFecha instanceof PartidoFecha)
                ? $em
                    ->getRepository('futfunBundle:DatosEliminacion')
                    ->devolverDatosXTorneoXFechaYPartido(
                        $torneo, $numFecha, $partidoFecha->getNumeroPartido())
                : $em
                    ->getRepository('futfunBundle:DesempateDatosElimin')
                    ->devolverDatosXTorneoXFechaYPartido(
                        $torneo, $numFecha, $partidoFecha->getNumeroPartido());

        } else {

            return $partidoFecha->getLocal()->getNombre()
            . " vs " . $partidoFecha->getVisita()->getNombre();

        }

        $local = ($partidoFecha->getLocal() == null)
            ? $datosElimin->getTipoLocal()
            : $partidoFecha->getLocal()->getNombre();

        $visita = ($partidoFecha->getVisita() == null)
            ? $datosElimin->getTipoVisita()
            : $partidoFecha->getVisita()->getNombre();

        return $local . " vs " . $visita;

    }

}