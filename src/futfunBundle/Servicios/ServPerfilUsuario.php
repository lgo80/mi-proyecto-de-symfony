<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarApuestaTabla;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServPerfilUsuario
{

    public function obtenerDatos(
        $user, $em, Controller $controller)
    {

        $torneosOrganizados = $em
            ->getRepository('futfunBundle:AdministradoresTorneos')
            ->findByUser($user);

        $apuestasOrganizadas = $em
            ->getRepository('futfunBundle:AdministradoresApuestas')
            ->findByUser($user);

        $apuestasParticipanteActivas = $em
            ->getRepository('futfunBundle:ApuestasParticipantes')
            ->getByParticipantActive($user);

        $apuestasParticipanteFinalizadas = $em
            ->getRepository('futfunBundle:ApuestasParticipantes')
            ->getByParticipantFinished($user);

        $apuestasGanadas = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasPorWinAll($user);

        $titulo = $controller->get('translator')->trans("User information");
        $nombreTorneoLugar = "";

        return compact("user", "titulo", "nombreTorneoLugar",
            "torneosOrganizados", "apuestasOrganizadas",
            "apuestasParticipanteActivas", "apuestasParticipanteFinalizadas",
            "apuestasGanadas");

    }

}