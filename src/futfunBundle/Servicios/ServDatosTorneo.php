<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarDatosTorneo;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServDatosTorneo
{

    /**
     * Esta funcion se especifica en obtener los datos del torneo necesarios para mostrarselo al cliente
     * @param $idTorneo Es el id del torneo que entra por GET
     * @param $em Es la conexion necesaria a la base de datos
     * @param $userLogueado
     * @return array Devuelve los datos necesarios para la pagina dever los datos del torneo
     */
    public function validarPrimera($idTorneo, $em, Controller $controller)
    {

        $validar = new ValidarDatosTorneo();

        $respuesta = $validar->validarGet($idTorneo);

        $fechas = null;

        if ($respuesta["isValid"]) {

            $torneo = $em
                ->getRepository('futfunBundle:TorneoBase')
                ->find($idTorneo);

            $respuesta = $validar->validarTorneo($torneo);

            if ($respuesta["isValid"]) {

                $fechas = $em->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneo($torneo, 1);

                $servicio = new ServFuncionesUtiles();

                $fechaComienzo = $servicio
                    ->formatearFechaEspanolUnica((isset($fechas)) ?
                        $fechas->getFechaInicioAt() : null);

                $titulo = $controller->get('translator')
                    ->trans("See data of the Tournament");

                $nombreTorneoLugar = "datostorneo";

            }

        }

        return compact("respuesta", "torneo", "fechas", "titulo",
            "nombreTorneoLugar", "fechaComienzo");

    }

}