<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\ApuestasParticipantes;
use futfunBundle\Validador\ValidarCompletarExtras;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServInicio
{

    public function validarDatos(
        $user, $em, Controller $controller)
    {

        $apuestasParticipando = $em
            ->getRepository('futfunBundle:ApuestasParticipantes')
            ->getByParticipantActive($user);

        $apuestasInscripcion = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasPorEstado(true, "En espera");

        $extras = $this->devolverExtras(
            $em, $controller, $user);

        $fechasApuestas = $this
            ->devolverFechasApuestas($em, $controller, $user);

        $torneosEjecucion = $em
            ->getRepository('futfunBundle:TorneoBase')
            ->findByEstado("En progreso");

        $deleteFormAjax = $this->createCustomForm(
            ':USER_ID', 'POST',
            'trabajo_dbbundle_apuestas_agregar_usuario',
            $controller);

        $titulo = $controller->get('translator')
            ->trans("Welcome to FutFun");

        $nombreTorneoLugar = "inicio";

        return array(
            "nombreTorneoLugar" => $nombreTorneoLugar,
            "titulo" => $titulo,
            "apuestasParticipando" => $apuestasParticipando,
            "apuestasInscripcion" => $apuestasInscripcion,
            "extras" => $extras,
            "fechasApuestas" => $fechasApuestas,
            "delete_form_ajax" => $deleteFormAjax->createView(),
            "torneosEjecucion" => $torneosEjecucion
        );

    }

    public function getDatosApuestasElegida(
        $user, $em, $opt)
    {

        $arrayRespuesta = null;

        switch ($opt) {
            case "reg":

                $arrayRespuesta = $em
                    ->getRepository('futfunBundle:ApuestaBase')
                    ->devolverApuestasPorEstado(true, "En espera");
                break;
            case "pro":

                $arrayRespuesta = $em
                    ->getRepository('futfunBundle:ApuestaBase')
                    ->devolverApuestasPorEstado(true, "Comenzado");
                break;
            case "tpro":

                $arrayRespuesta = $em
                    ->getRepository('futfunBundle:TorneoBase')
                    ->findByEstado("En progreso");
                break;
            case "tfin":

                $arrayRespuesta = $em
                    ->getRepository('futfunBundle:TorneoBase')
                    ->findByEstado("Finalizado");
                break;
            default:
                $arrayRespuesta = $em
                    ->getRepository('futfunBundle:ApuestaBase')
                    ->devolverApuestasPorEstado(true, "Finalizado");
        }

        return $this->getArrayApuestas(
            $arrayRespuesta, $opt, $em, $user);

    }

    private function getArrayApuestas(
        $array, $opt, $em, $user)
    {

        $arrayRespuesta = [];

        foreach ($array as &$apuesta) {

            $participante = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByParticipant($apuesta, $user);

            if ($opt == "reg" || (($opt == "pro" || $opt == "fin")
                    && $participante == null) ||
                ($opt == "fiy" && $participante != null
                    || $opt == "tpro" || $opt == "tfin")
            ) {

                $id = $apuesta->getId();
                $nombre = $apuesta->getNombre();

                $arrayRespuesta[] = compact("id", "nombre");
            }


        }

        return (!empty($arrayRespuesta)) ? $arrayRespuesta : "No hay apuesta creadas";

    }

    public function getArrayAccesos(
        $idApuesta, $opt, $em, $user)
    {

        $apuesta = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->findById($idApuesta);

        $participant = $em
            ->getRepository('futfunBundle:ApuestasParticipantes')
            ->getByParticipant($apuesta, $user);

        $anotarse = ($opt == "reg" && $participant == null && $user != null);
        $datos = true;
        $extras = ($participant != null);
        $completarResul = ($opt == "reg" && $participant != null);
        $posiciones = true;
        $resultados = ($opt != "reg");

        return compact("anotarse", "datos", "extras",
            "completarResul", "posiciones", "resultados");

    }

    public function getArrayAccesosT(
        $idTorneo, $opt, $em, $user)
    {

        $torneo = $em
            ->getRepository('futfunBundle:TorneoBase')
            ->findById($idTorneo);

        $administrador = $em
            ->getRepository('futfunBundle:AdministradoresTorneos')
            ->findByUserAndTorneo($user, $torneo);

        $desempate = $em
            ->getRepository('futfunBundle:DesempateDatos')
            ->devolverXTorneo($torneo);

        $datos = true;
        $crear = ($opt == 'tpro' && $administrador != null);
        $posiciones = true;
        $completarResul = true;
        $desempate = ($desempate != null);

        return compact("datos", "crear", "posiciones",
            "completarResul", "desempate");

    }

    private function validarDatos2(
        $user, $em, Controller $controller)
    {

        $apuestasParticipando = $em
            ->getRepository('futfunBundle:ApuestasParticipantes')
            ->getByParticipantActive($user);

        $apuestasInscripcion = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasPorEstado(true, "En espera");

        $apuestasFinalizadasParticipe = $em
            ->getRepository('futfunBundle:ApuestasParticipantes')
            ->getByParticipantFinished($user);

        $apuestasResto = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasXActivoXNoEstado(true, "En espera");

        $torneosEjecucion = $em
            ->getRepository('futfunBundle:TorneoBase')
            ->findByEstado("En progreso");

        $torneosFinalizados = $em
            ->getRepository('futfunBundle:TorneoBase')
            ->findByEstado("Finalizado");

        $isAdministradoresT = $this
            ->devolverArrayAdministradores(
                $torneosEjecucion, $torneosFinalizados,
                "administrador", $user, $em);

        $desempates = $this
            ->devolverArrayAdministradores(
                $torneosEjecucion, $torneosFinalizados,
                "desempate", $user, $em);

        $extras = $this->devolverExtras(
            $em, $controller, $user);

        $fechasApuestas = $this
            ->devolverFechasApuestas($em, $controller, $user);

        $fechasLimitesInscripcion = $this
            ->devolverFechasLimiteInscripcion(
                $apuestasInscripcion, $em, $controller, $user);

        $deleteFormAjax = $this->createCustomForm(
            ':USER_ID', 'POST',
            'trabajo_dbbundle_apuestas_agregar_usuario',
            $controller);

        $restoApuestasDatos = $this
            ->devolverRestoDatos(
                $apuestasResto, $user, $em);

        $arrayParticipante = $this->devolverArrayParticipantes(
            $apuestasFinalizadasParticipe, $em);

        $titulo = $controller->get('translator')
            ->trans("Welcome to FutFun");

        $nombreTorneoLugar = "inicio";

        return array(
            "nombreTorneoLugar" => $nombreTorneoLugar,
            "titulo" => $titulo,
            "apuestasParticipando" => $apuestasParticipando,
            "torneosEjecucion" => $torneosEjecucion,
            "isAdministradoresT" => $isAdministradoresT,
            "desempates" => $desempates,
            "apuestasInscripcion" => $apuestasInscripcion,
            "apuestasFinalizadasParticipe" => $apuestasFinalizadasParticipe,
            "apuestasResto" => $apuestasResto,
            "extras" => $extras,
            "fechasApuestas" => $fechasApuestas,
            "fechasLimitesInscripcion" => $fechasLimitesInscripcion,
            "delete_form_ajax" => $deleteFormAjax->createView(),
            "torneosFinalizados" => $torneosFinalizados,
            "restoApuestasDatos" => $restoApuestasDatos,
            "arrayParticipante" => $arrayParticipante
        );

    }

    private function devolverArrayAdministradores(
        $torneosEjecucion, $torneosFinalizados, $accion,
        $user, $em)
    {

        $arrayRespuesta = [];

        foreach ($torneosEjecucion as &$torneo) {

            $arrayRespuesta[$torneo->getId()] =
                $this->procesarArrayAdministrador(
                    $accion, $torneo, $user, $em
                );

        }

        foreach ($torneosEjecucion as &$torneo) {

            $arrayRespuesta[$torneo->getId()] =
                $this->procesarArrayAdministrador(
                    $accion, $torneo, $user, $em
                );

        }

        return $arrayRespuesta;

    }

    private function procesarArrayAdministrador(
        $accion, $torneo, $user, $em
    )
    {

        if ($accion == "administrador") {

            $validar = new ValidarFechas();

            $respuesta = $validar
                ->validarAdministrador($user, $torneo);

            return $respuesta["isValid"];

        } else {

            return $em
                ->getRepository('futfunBundle:DesempateDatos')
                ->devolverXTorneo($torneo);

        }

    }

    private function devolverExtras(
        $em, $controller, $user)
    {

        $apuestas = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->getAllActive();

        $arrayRespuesta = [];

        foreach ($apuestas as &$apuesta) {

            $extrasApuesta = $em
                ->getRepository('futfunBundle:ApuestasExtras')
                ->devolverExtrasApuesta($apuesta);

            if (count($extrasApuesta) == 0) {

                $arrayRespuesta[$apuesta->getId()] = array(
                    "valor" => "1",
                    "fecha" => $controller->get('translator')
                        ->trans("There are no extras"),
                    "isCompleto" => false
                );

                continue;

            }

            if ($apuesta->getFechaLimiteExtraAt() == null) {

                $valor = 2;
                $fechaLimite = $controller->get('translator')
                    ->trans("There is no date");
                $horaAt = "";

            } else {

                $valor = 3;
                $fechaLimite = $apuesta->getFechaLimiteExtraAt()
                    ->format('d/m/Y');
                $horaAt = $apuesta->getFechaLimiteExtraAt()
                    ->format('H:i:s');

            }

            $isCorrecto = $em
                ->getRepository('futfunBundle:ApuestaExtraUsuario')
                ->devolverIsCompleto($extrasApuesta, $user);


            $arrayRespuesta[$apuesta->getId()] = array(
                "valor" => $valor,
                "fecha" => $fechaLimite,
                "hora" => $horaAt,
                "isCompleto" => $isCorrecto
            );

        }


        return $arrayRespuesta;

    }

    private function devolverFechasApuestas(
        $em, $controller, $user)
    {

        $apuestasComenzadas = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasXActivoXNoEstado(true, "Finalizado");

        $arrayRespuesta = [];

        foreach ($apuestasComenzadas as &$apuesta) {

            $fechaBaseActual = $em
                ->getRepository('futfunBundle:FechasBase')
                ->devolverFechasNoEjecridas(
                    $apuesta->getTorneo(), new \DateTime());

            if ($fechaBaseActual != null && !empty($fechaBaseActual)) {

                $fechaActual = $fechaBaseActual[0];

                $fechaLimite = $fechaActual->getFechaInicioAt()
                    ->format('d/m/Y');
                $horaAt = $fechaActual->getFechaInicioAt()
                    ->format('H:i:s');
                $numeroFechaApu = $fechaActual->getNumeroFecha();
                $ApuestaVacia = $em
                    ->getRepository('futfunBundle:ApuestasFechas')
                    ->devolverIsComplete(
                        $apuesta, $user, $fechaActual);

            } else {

                $fechaLimite = null;
                $horaAt = null;
                $numeroFechaApu = null;
                $ApuestaVacia = false;

            }

            $arrayRespuesta[$apuesta->getId()] = array(
                "fechaLimite" => $fechaLimite,
                "horaLimite" => $horaAt,
                "numFechaApu" => $numeroFechaApu,
                "isComplete" => $ApuestaVacia,
                "numeroFecha" => ($fechaBaseActual != null
                    && !empty($fechaBaseActual))
                    ? $fechaBaseActual[0]->getNumeroFecha()
                    : "n/g"
            );

        }

        return $arrayRespuesta;

    }

    private function devolverFechasLimiteInscripcion(
        $apuestasEnInscripcion, $em, $controller, $user)
    {

        $arrayRespuesta = [];

        foreach ($apuestasEnInscripcion as &$apuesta) {

            $primeraFecha = $em
                ->getRepository('futfunBundle:FechasBase')
                ->devolverFechaDelTorneo(
                    $apuesta->getTorneo(), 1);

            $fechaLimite = ($primeraFecha == null
                || $primeraFecha->getFechaInicioAt() == null)
                ? $fechaLimite = $controller->get('translator')
                    ->trans("There is no date")
                : $primeraFecha->getFechaInicioAt()
                    ->format('d/m/Y');
            $horaAt = ($primeraFecha != null
                && $primeraFecha->getFechaInicioAt() != null)
                ? $primeraFecha->getFechaInicioAt()
                    ->format('H:i:s')
                : null;

            $participantes = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByApuestaBase($apuesta);

            $isInscripto = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByParticipant($apuesta, $user);

            $arrayRespuesta[$apuesta->getId()] = array(
                "fechaLimite" => $fechaLimite,
                "horaLimite" => $horaAt,
                "participantes" => $participantes,
                "isAnotarse" => ($isInscripto == null)
            );

        }

        return $arrayRespuesta;

    }

    /**
     * Esta funcion se especifica en el formulario que va ir en el menu de apuestas para cuando
     * se presiona en la apuesta deseada te rediriga automaticamente
     * @param $id Es el id de apuesta que se eligio en el menu
     * @param $method Es el metodo que puede ser POST o GET
     * @param $route Es la ruta al que tiene que redirigir la vista
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createCustomForm($id, $method, $route, $controller)
    {

        $form = $controller->createFormBuilder()
            ->setAction($controller->generateUrl($route, array('idApuesta' => $id)))
            ->setMethod($method)
            ->getForm();

        return $form;

    }

    private function devolverRestoDatos(
        $apuestasResto, $user, $em)
    {

        $arrayComenzado = [];
        $arrayFinalizado = [];
        $fechaBaseActual = [];
        $arrayParticipantes = [];

        foreach ($apuestasResto as &$apuesta) {

            $participante = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByParticipant($apuesta, $user);

            if ($participante == null) {

                if ($apuesta->getEstado() == "Comenzado") {

                    $arrayComenzado[] = $apuesta;

                    $fechaBaseActual[$apuesta->getId()] = $em
                        ->getRepository('futfunBundle:FechasBase')
                        ->devolverFechaEnMarcha(
                            $apuesta->getTorneo());

                } else {

                    $arrayFinalizado[] = $apuesta;

                }

            }

            $arrayParticipantes[$apuesta->getId()] = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByApuestaBase($apuesta);

        }

        return compact("arrayComenzado", "arrayFinalizado",
            "fechaBaseActual", "arrayParticipantes");

    }

    private function devolverArrayParticipantes(
        $apuestasFinalizadasParticipante, $em)
    {

        $arrayParticipantes = [];

        foreach ($apuestasFinalizadasParticipante as &$participante) {

            $arrayParticipantes[$participante->getApuestasBase()->getId()] = $em
                ->getRepository('futfunBundle:ApuestasParticipantes')
                ->getByApuestaBase($participante->getApuestasBase());

        }

        return compact("arrayParticipantes");

    }

}