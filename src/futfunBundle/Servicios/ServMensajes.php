<?php

namespace futfunBundle\Servicios;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServMensajes
{

    public function validarDatos(
        $user, $em, Controller $controller, $bandeja, $page)
    {

        $paginacion = $this->devolverDatosPaginacion(
            $page, $bandeja, $em, $user);

        $titulo = $controller->get('translator')->trans("Message tray");
        $nombreTorneoLugar = "";

        return compact("bandeja", "paginacion",
            "nombreTorneoLugar", "titulo");

    }

    private function devolverDatosPaginacion(
        $page, $bandeja, $em, $user)
    {

        $cantidadRegistros = ($bandeja == "r")
            ? $em
                ->getRepository("futfunBundle:Mensajes")
                ->devolverCantidadMensajesRecibidos($user)
            : $em
                ->getRepository("futfunBundle:Mensajes")
                ->devolverCantidadMensajesEnviados($user);

        $porPagina = 10;

        $totalPaginas = ($cantidadRegistros > 0)
            ? ceil($cantidadRegistros / $porPagina)
            : 1;

        $page = $this->validarPage(
            $page, $totalPaginas, $porPagina, $cantidadRegistros);

        $offset = 0;

        if ($page > 1) {
            $offset = $porPagina * ($page - 1);
        }

        $mensajes = ($bandeja == "r")
            ? $em
                ->getRepository("futfunBundle:Mensajes")
                ->devolverMensajesRecibidos(
                    $user, $offset, $porPagina)
            : $em
                ->getRepository("futfunBundle:Mensajes")
                ->devolverMensajesEnviados(
                    $user, $offset, $porPagina);

        return compact("mensajes", "page",
            "totalPaginas", "cantidadRegistros");

    }

    private function validarPage(
        $page, $totalPaginas, $porPagina, $cantidadRegistros)
    {

        $page = (!is_numeric($page))
            ? 1 : floor($page);

        if ($cantidadRegistros <= $porPagina) {
            $page = 1;
        }

        if (($page * $porPagina) > $cantidadRegistros) {
            $page = $totalPaginas;
        }

        return $page;
    }

}