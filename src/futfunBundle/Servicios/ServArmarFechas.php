<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\TorneoBase;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServArmarFechas
{

    /**
     * Esta funcion se especifica en devolver los datos necesarios para que el controlador pueda mandar
     * toda la informacion a la vista de la fecha correspondiente
     * @param $idTorneo Es el id del torneo base ingresado por GET
     * @param $em
     * @param $userLogueado
     * @param $numeroFecha Es un integer con la fecha ingresada por GET
     * @return array Devuelve un array con los datos necesarios para el armado de la fecha correspondiente
     */
    public function validarPrimera($idTorneo, $em, $userLogueado,
                                   $numeroFecha, $direccion)
    {

        $validar = new ValidarFechas();

        $user = (isset($userLogueado))
            ? $em->getRepository('futfunBundle:User')
                ->findOneByUsername($userLogueado->getUsername())
            : null;

        $respuesta = $validar->validarGet($idTorneo);

        $fechas = null;

        $page = "";

        if ($respuesta["isValid"]) {

            $torneo = $em->getRepository('futfunBundle:TorneoBase')->find($idTorneo);

            $respuesta = $validar->validarAdministrador($user, $torneo);

            if ($respuesta["isValid"]) {

                $fechas = $em->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneoAll($torneo);

            }

        }

        $isHabilitarGrupo = null;
        $valorGrupo = null;

        if ($fechas != null) {

            $tipoTorneo = $em->getRepository('futfunBundle:TipoTorneo')
                ->devolverTipoTorneo($torneo->getTipoTorneo());

            $fechas = $em->getRepository('futfunBundle:FechasBase')
                ->devolverFechaCorrespondiente(
                    $numeroFecha, $torneo, $validar, $direccion);

            switch ($tipoTorneo) {

                case "l":

                    $page = "liga";
                    break;

                case "lye":

                    if ($respuesta["isValid"]) {

                        $numFechaInt = (!is_int($fechas))
                            ? $fechas->getNumeroFecha() : (int)$fechas;

                        $cantidadPartidosGrupos = $em->getRepository('futfunBundle:TorneoBase')
                            ->devolverCantidadFechasLiga($torneo);

                        $page = ($numFechaInt > $cantidadPartidosGrupos) ? "eliminacion" : "liga";

                    } else {

                        $page = "liga";

                    }

                    break;

                case "e":

                    $page = "eliminacion";
                    break;

            }

        } else {

            $page = "primera";

            $tipoTorneo = $em->getRepository('futfunBundle:TipoTorneo')
                ->devolverTipoTorneo($torneo->getTipoTorneo());

            switch ($tipoTorneo) {

                case "l":

                    $isHabilitarGrupo = true;
                    $valorGrupo = "Unico";

                    break;

                case "lye":

                    $cantidadPartidosGrupos = $em->getRepository('futfunBundle:TorneoBase')
                        ->devolverCantidadFechasLiga($torneo);

                    $isHabilitarGrupo = ($numeroFecha > $cantidadPartidosGrupos);
                    $valorGrupo = ($numeroFecha > $cantidadPartidosGrupos) ? "No lleva nombre de grupo" : "";
                    break;

                case "e":

                    $valorGrupo = "No lleva nombre de grupo";
                    $isHabilitarGrupo = true;
                    break;

            }


        }

        return compact("respuesta", "torneo", "fechas", "page", "isHabilitarGrupo",
            "valorGrupo");

    }

    /**
     * Esta funcion se especifica en devolver los datos necesarios para que el controlador pueda mandar
     * toda la informacion a la vista de la fecha correspondiente de liga
     * @param $torneo Es el torneo que al usuario interesa
     * @param $numeroFecha Es un integer con el numero de fecha correspondiente
     * @param $em
     * @return array Esta funcion devuelve los datos en un array para la fecha de liga
     */
    public function obtenerDatosPrimera(
        $torneo, $numeroFecha, $em, Controller $controller)
    {

        $cantGrupos = ($torneo->getDato()->getCantGrupos() == null)
            ? 1 : $torneo->getDato()->getCantGrupos();

        $cantFechas = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverCantidadFechas($torneo->getId());

        $fechaVuelta = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverFechaVuelta($torneo->getId(), $numeroFecha);

        $tipoTorneo = $em->getRepository('futfunBundle:TipoTorneo')
            ->devolverTipoTorneo($torneo->getTipoTorneo());

        $cantEquiposParcial = ($tipoTorneo == "lye")
            ? (int)$torneo->getDato()->getEquipGrupos()
            : (int)$torneo->getDato()->getCantEquipos();

        $cantPartidos = (int)($cantEquiposParcial / 2);

        $isHabLibre = (($cantEquiposParcial % 2) != 0);

        $ultimaFechaIda = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverNumeroFechaLast($torneo);

        $titulo = $controller->get('translator')
            ->trans("Create date");

        $nombreTorneoLugar = "armarFecha";

        $botonGuardar = $controller->get('translator')
            ->trans("Save Date");

        $respuesta = compact("cantGrupos", "cantPartidos", "isHabLibre",
            "cantFechas", "torneo", "fechaVuelta", "ultimaFechaIda",
            "titulo", "nombreTorneoLugar", "botonGuardar");

        return $respuesta;

    }

    /**
     * Esta funcion se especifica en devolver los datos necesarios para que el controlador pueda mandar
     * toda la informacion a la vista de la fecha correspondiente de eliminacion
     * @param $torneo Es el torneo que al usuario interesa
     * @param $numeroFecha Es un integer con el numero de fecha correspondiente
     * @param $em
     * @return array Esta funcion devuelve los datos en un array para la fecha de liga
     */
    public function obtenerDatosEliminacion(
        $torneo, $numeroFecha, $em, Controller $controller)
    {

        $cantGrupos = 1;

        $cantFechas = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverCantidadFechas($torneo->getId());

        $fechaVuelta = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverFechaVuelta($torneo->getId(), $numeroFecha);

        $finalConTercero = $this->devolverSiEsFinalConTercerPuesto(
            $torneo, $numeroFecha, $em);

        $cantPartidos = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverCantidadPartidos($torneo, $numeroFecha);

        $equipos = $this->devolverEquiposEliminacion($torneo, $numeroFecha,
            $em, $finalConTercero);

        $numFechaGet = (is_int($numeroFecha))
            ? $numeroFecha : $numeroFecha->getNumeroFecha();

        $ultimaFechaIda = $em->getRepository('futfunBundle:TorneoBase')
            ->devolverNumeroFechaLast($torneo);

        $titulo = $controller->get('translator')
            ->trans("Create date");

        $nombreTorneoLugar = "armarEliminacion";

        $botonGuardar = $controller->get('translator')
            ->trans("Save Date");

        $respuesta = compact("torneo", "cantGrupos", "fechaVuelta",
            "cantPartidos", "cantFechas", "equipos", "finalConTercero",
            "numFechaGet", "ultimaFechaIda", "titulo",
            "nombreTorneoLugar", "botonGuardar");

        return $respuesta;

    }

    private function devolverEquiposEliminacion(TorneoBase $torneo, $fechaEntrante,
                                                $em, $finalConTercero)
    {

        $tipoTorneo = $em->getRepository('futfunBundle:TipoTorneo')
            ->devolverTipoTorneo($torneo->getTipoTorneo());

        if ($tipoTorneo == "lye") {

            $isPrimeraEliminacion = $em->getRepository('futfunBundle:TorneoBase')
                ->verificarPrimeraFecha($torneo, (is_int($fechaEntrante))
                    ? $fechaEntrante : $fechaEntrante->getNumeroFecha());

            if ($isPrimeraEliminacion) {

                return $em->getRepository('futfunBundle:PosicionesLiga')
                    ->devolverEquiposParaPrimeraEliminatoria($torneo);

            }

        }

        $valorARestar = ($torneo->getDato()->getFormaElim() == "idayvuelta") ? 2 : 1;

        $fechaAnteriorNumero = (is_int($fechaEntrante))
            ? $fechaEntrante - $valorARestar : $fechaEntrante->getNumeroFecha() - $valorARestar;

        return $this->devolverArrayEquipos($fechaAnteriorNumero, $torneo, $em, $finalConTercero);

    }

    private function devolverArrayEquipos($numeroFechaAnterior, TorneoBase $torneo,
                                          $em, $finalConTercero)
    {

        $respuesta = [];

        if ($numeroFechaAnterior > 0) {

            $cantPartidos = $em->getRepository('futfunBundle:TorneoBase')
                ->devolverCantidadPartidos($torneo, $numeroFechaAnterior);

            $j = 0;

            for ($i = 1; $i <= $cantPartidos; $i++) {

                $j++;
                $respuesta[$j] = $em->getRepository('futfunBundle:PosicionesLiga')
                    ->devolverEquipoGanador($numeroFechaAnterior, $torneo, $i);

                if ($respuesta[$j] == null) {

                    $respuesta[$j] = "Ganador del partido " . $i;

                    if ($finalConTercero == "2") {
                        $j++;
                        $respuesta[$j] = "Perdedor del partido " . $i;
                    }

                } else {

                    if ($finalConTercero == "2") {

                        $fechaIda = $em->getRepository('futfunBundle:FechasBase')
                            ->devolverFechaDelTorneo($torneo, $numeroFechaAnterior);

                        $partidoIda = $fechaIda->getGrupoFechas()[0]->getPartidoFechas()[$i - 1];

                        $respuesta[$j] = $respuesta[$j] . " (G)";

                        $j++;
                        $respuesta[$j] = ($partidoIda->getLocal()->getNombre() . " (G)" == $respuesta[$j - 1])
                            ? $partidoIda->getVisita()->getNombre() . " (P)"
                            : $partidoIda->getLocal()->getNombre() . " (P)";

                    }

                }

            }

        } else {

            $primeraFecha = $em->getRepository('futfunBundle:FechasBase')
                ->devolverFechaDelTorneo($torneo, 1);

            foreach ($primeraFecha->getGrupoFechas()[0]->getPartidoFechas() as &$partido) {


                $respuesta[] = $partido->getLocal()->getNombre();
                $respuesta[] = $partido->getVisita()->getNombre();

            }

        }

        return $respuesta;

    }

    private function devolverSiEsFinalConTercerPuesto(TorneoBase $torneo, $numeroFecha, $em)
    {

        $fechaFinalIda = $em->getRepository('futfunBundle:FechasBase')
            ->devolverFechaFinalIda($torneo);

        $numFechaEnNum = (is_int($numeroFecha))
            ? $numeroFecha : $numeroFecha->getNumeroFecha();

        if ($fechaFinalIda <= $numFechaEnNum) {

            return ($torneo->getDato()->getPartTercerPuesto()) ? "2" : "1";

        } else {

            return "0";

        }

    }

}