<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarCrearDesempate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServCrearDesempate
{

    public function devolverDatos(
        $idTorneo, $em, $usuarioLogueado, Controller $controller)
    {

        $validar = new ValidarCrearDesempate();

        $respuesta = $validar->validarGet($idTorneo);

        if ($respuesta["isValid"]) {

            $torneo = $em->getRepository('futfunBundle:TorneoBase')
                ->findById($idTorneo);

            $respuesta = $validar->validarTorneo($torneo);

            if ($respuesta["isValid"]) {

                $desempateDatos = $em->getRepository('futfunBundle:DesempateDatos')
                    ->devolverXTorneo($torneo);

                $respuesta = $validar->validarDesempate($desempateDatos);

                if ($respuesta["isValid"]) {

                    if ($desempateDatos->getIsConfirm()) {

                        $respuesta = array("isValid" => false,
                            "lugar" => 'trabajo_dbbundle_torneos_ver_datos_desempate',
                            "mensaje" => '�El torneo ya esta confirmado!',
                            "nombreError" => "mensaje-danger");

                    } else {

                        $respuesta = $validar->validarAdministrador(
                            $desempateDatos, $usuarioLogueado);

                        $titulo = $controller->get('translator')
                            ->trans("Create tiebreaker");
                        $subtitulo = $controller->get('translator')
                            ->trans("Complete data");

                        $nombreTorneoLugar = "crearDesempate";

                    }

                }

            }


        }

        return compact("respuesta", "torneo", "desempateDatos",
            "titulo", "subtitulo", "nombreTorneoLugar");

    }


}