<?php

namespace futfunBundle\Servicios;

use futfunBundle\Validador\ValidarCompletarExtras;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServCompletarExtras
{

    public function validarDatos(
        $idApuesta, $em, $userLogueado,
        Controller $controler, $idUsuario)
    {

        $validar = new ValidarCompletarExtras();

        $respuesta = $validar->validarGet($idApuesta);

        if ($respuesta["isValid"]) {

            $apuesta = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->find($idApuesta);

            $respuesta = $validar->validarApuesta($apuesta);

            if ($respuesta["isValid"]) {

                $usuarioEntrante = (empty($idUsuario))
                    ? $userLogueado
                    : $em
                        ->getRepository('futfunBundle:User')
                        ->find($idUsuario);

                $participante = $em
                    ->getRepository('futfunBundle:ApuestasParticipantes')
                    ->getByParticipant($apuesta, $usuarioEntrante);

                $respuesta = $validar
                    ->validarParticipante($participante);

                if ($respuesta["isValid"]) {

                    $extrasApuesta = $em
                        ->getRepository('futfunBundle:ApuestasExtras')
                        ->devolverExtrasApuesta($apuesta);

                    if (count($extrasApuesta) == 0) {

                        $respuesta = array(
                            "isValid" => false,
                            "lugar" => "trabajo_dbbundle_apuestas_ver_tabla",
                            "mensaje" => 'No hay extras ingresados para completar',
                            "nombreError" => 'mensaje-danger');

                    }

                    $apuestaExtraUsuario = $em
                        ->getRepository('futfunBundle:ApuestaExtraUsuario')
                        ->devolverExtrasUsuario($extrasApuesta, $usuarioEntrante);

                    $fecha1 = $em
                        ->getRepository('futfunBundle:FechasBase')
                        ->devolverFechaDelTorneo($apuesta->getTorneo(), 1);

                    $isFechaCumplida = $validar->validarFechaNoCumplida(
                        $apuesta, $em);

                    $serviciosUtiles = new ServFuncionesUtiles();
                    $arrayFechas = $serviciosUtiles
                        ->formatearFechaEspanolUnica(
                            $apuesta->getFechaLimiteExtraAt());

                    $participantes = $em
                        ->getRepository('futfunBundle:ApuestasParticipantes')
                        ->getByApuestaBase($apuesta);

                    $isUsuarioLogueado = ($userLogueado->getId()
                        == $usuarioEntrante->getId());

                    $titulo = $controler->get('translator')
                        ->trans("Complete the betting extras");

                    $subtitulo1 = $controler->get('translator')
                        ->trans("Deadline:");

                    $subtitulo2 = ($arrayFechas != null)
                        ? $arrayFechas
                        : $controler->get('translator')
                            ->trans("It is not defined yet.");

                    $subtitulo = $subtitulo1 . " " . $subtitulo2;

                    $nombreTorneoLugar = "apuesta";

                    $botonGuardar = $controler->get('translator')
                        ->trans("Save extras");


                }

            }


        }

        return compact("respuesta", "apuesta", "extrasApuesta",
            "apuestaExtraUsuario", "fecha1", "isFechaCumplida"
            , "arrayFechas", "titulo", "subtitulo", "isUsuarioLogueado",
            "nombreTorneoLugar", "botonGuardar", "participantes",
            "usuarioEntrante");

    }

    public function validarDatosConfirmar(
        $idApuesta, $em, $userLogueado, Controller $controler)
    {

        $validar = new ValidarCompletarExtras();

        $respuesta = $validar->validarGet($idApuesta);

        if ($respuesta["isValid"]) {

            $apuesta = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->find($idApuesta);

            $respuesta = $validar->validarApuesta($apuesta);

            if ($respuesta["isValid"]) {

                $respuesta = $validar
                    ->validarAdministrador($userLogueado, $apuesta);

                if ($respuesta["isValid"]) {

                    $extrasApuesta = $em
                        ->getRepository('futfunBundle:ApuestasExtras')
                        ->devolverExtrasApuesta($apuesta);

                    if (count($extrasApuesta) == 0) {

                        $respuesta = array(
                            "isValid" => false,
                            "lugar" => "trabajo_dbbundle_apuestas_ver_tabla",
                            "mensaje" => 'No hay extras ingresados para completar',
                            "nombreError" => 'mensaje-danger');

                    }

                    $participantes = $em
                        ->getRepository('futfunBundle:ApuestasParticipantes')
                        ->getByApuestaBase($apuesta);

                    $apuestaExtraUsuarios = $em
                        ->getRepository('futfunBundle:ApuestaExtraUsuario')
                        ->devolverExtrasParaConfirmar(
                            $participantes, $extrasApuesta);

                    $serviciosUtiles = new ServFuncionesUtiles();
                    $arrayFechas = $serviciosUtiles
                        ->formatearFechaEspanolUnica(
                            $apuesta->getFechaLimiteExtraAt());

                    $titulo = $controler->get('translator')
                        ->trans("Confirm extras");

                    $nombreTorneoLugar = "confirmarapuesta";

                    $botonEditar = $controler->get('translator')
                        ->trans("Save");

                    $botonGuardar = $controler->get('translator')
                        ->trans("Save and confirm");

                }

            }


        }

        return compact("respuesta", "apuesta", "extrasApuesta",
            "apuestaExtraUsuarios", "participantes", "arrayFechas",
            "titulo", "nombreTorneoLugar", "botonEditar", "botonGuardar");

    }

}