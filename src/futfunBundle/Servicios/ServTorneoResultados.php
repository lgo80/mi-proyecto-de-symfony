<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\TorneoBase;
use futfunBundle\Validador\ValidarTorneoResultados;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServTorneoResultados
{

    public function validarDatos(
        $idTorneo, $em, $userLogueado, $numeroFecha,
        $direccion, Controller $controller)
    {

        $validar = new ValidarTorneoResultados();

        $respuesta = $validar->validarGet($idTorneo);

        if ($respuesta["isValid"]) {

            $torneo = $em->getRepository('futfunBundle:TorneoBase')->find($idTorneo);

            $respuesta = $validar->validarTorneo($torneo);

            if ($respuesta["isValid"]) {

                $fechas = $em->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneoAll($torneo);

                if (!empty($fechas)) {

                    $cantFechas = $em->getRepository('futfunBundle:TorneoBase')
                        ->devolverCantidadFechasTotales($torneo);

                    $fechaBase = $em->getRepository('futfunBundle:FechasBase')
                        ->devolverFechaEntrante(
                            $numeroFecha, $torneo, $direccion, $cantFechas);

                    $fechaBase = $this->devolverFechaYaCreada(
                        $em, $numeroFecha, $torneo, $fechaBase);

                    if ($fechaBase != null) {

                        $isHabEditar = $validar->validarAdministrador(
                            $torneo, $userLogueado);

                        $definicion = $em
                            ->getRepository('futfunBundle:FechasBase')
                            ->devolverIsDefinicion(
                                $torneo, $fechaBase->getNumeroFecha());

                        $isGolVisitante = (!$definicion)
                            ? $this->isGolVisitante($torneo,
                                $cantFechas, $fechaBase->getNumeroFecha())
                            : false;

                        $fechaAnterior = (!$definicion)
                            ? $this->devolverFechaIda(
                                $torneo, $cantFechas,
                                $fechaBase->getNumeroFecha(), $em)
                            : null;

                        $datosEliminacion = $em
                            ->getRepository('futfunBundle:DatosEliminacion')
                            ->devolverDatosXTorneoYFecha(
                                $torneo, $fechaBase->getNumeroFecha());

                        $serviciosUtiles = new ServFuncionesUtiles();
                        $arrayFechas = $serviciosUtiles
                            ->formatearFechaEspanol($fechaBase);

                        $titulo = $controller->get('translator')
                            ->trans("Complete/edit results");

                        $nombreTorneoLugar = "CompletarResultadoFecha";

                        $botonEditar = $controller->get('translator')
                            ->trans("Edit result");

                        $botonGuardar = $controller->get('translator')
                            ->trans("Save Results");

                    } else {

                        if (is_numeric($numeroFecha)) {
                            $respuesta = array("isValid" => false,
                                "lugar" => 'trabajo_dbbundle_torneos_comprobarFecha',
                                "mensaje" => utf8_encode('�La fecha ingresada no esta creada todavia!'),
                                "nombreError" => "mensaje-danger");
                        } else {

                            $fechas = $em->getRepository('futfunBundle:FechasBase')
                                ->devolverFechaDelTorneo($torneo, 1);

                        }


                    }


                } else {

                    $respuesta = array("isValid" => false,
                        "lugar" => 'trabajo_dbbundle_torneos_comprobarFecha',
                        "mensaje" => utf8_encode('�No hay fechas creadas para completar algun resultado todavia!'),
                        "nombreError" => "mensaje-danger");

                }


            }


        }

        return compact("respuesta", "torneo", "numeroFecha", "fechaBase", "fechas",
            "cantFechas", "isHabEditar", "definicion", "isGolVisitante",
            "fechaAnterior", "datosEliminacion", "arrayFechas", "titulo",
            "nombreTorneoLugar", "botonGuardar", "botonEditar");

    }

    public function armarFechaConResultados($fechaBase, $form)
    {

        $i = 0;

        foreach ($fechaBase->getGrupoFechas() as &$valor) {

            $i++;

            foreach ($valor->getPartidoFechas() as &$valorPart) {

                $valorLocal = $form
                    ->get('valorLocal' . $i . $valorPart->getNumeroPartido())
                    ->getData();
                $valorVisita = $form
                    ->get('valorVisita' . $i . $valorPart->getNumeroPartido())
                    ->getData();

                $valorPart->setvalorLocal(
                    (is_numeric($valorVisita) && is_numeric($valorLocal))
                        ? $valorLocal : null);
                $valorPart->setvalorVisita(
                    (is_numeric($valorVisita) && is_numeric($valorLocal))
                        ? $valorVisita : null);

                $valorDefLocal = $form
                    ->get('valorDefinicionLocal' . $i . $valorPart->getNumeroPartido())
                    ->getData();
                $valorDefVisita = $form
                    ->get('valorDefinicionVisita' . $i . $valorPart->getNumeroPartido())
                    ->getData();

                $valorPart->setValorDefinicionLocal(
                    (is_numeric($valorDefLocal) && is_numeric($valorDefVisita))
                        ? $valorDefLocal : null);
                $valorPart->setValorDefinicionVisita(
                    (is_numeric($valorDefLocal) && is_numeric($valorDefVisita))
                        ? $valorDefVisita : null);

            }

        }

        return $fechaBase;

    }

    public function verSiEsLiga(TorneoBase $torneo, $numerFecha, $em)
    {

        $tipoTorneo = $em
            ->getRepository('futfunBundle:TipoTorneo')
            ->devolverTipoTorneo($torneo->getTipoTorneo());

        switch ($tipoTorneo) {

            case "l":

                return true;

            case "lye":

                $cantFechasLiga = $em
                    ->getRepository('futfunBundle:TorneoBase')
                    ->devolverCantidadFechasLiga($torneo);

                return ($numerFecha <= $cantFechasLiga);

            case "e":

                return false;

            default:

                return false;

        }


    }

    public function verSiHayApuestaCreada($torneo, $em)
    {

        $apuestasBase = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasPorTorneo($torneo);

        return $apuestasBase;

    }

    private function isGolVisitante(TorneoBase $torneo, $cantFechas, $numeroFecha)
    {

        $dato = $torneo->getDato();

        if ($numeroFecha == $cantFechas) {

            return ($dato->getGolVisitante() == "si");

        } else {

            return ($dato->getGolVisitante() == "si"
                || $dato->getGolVisitante() == "finalno");

        }

    }

    private function devolverFechaIda(TorneoBase $torneo, $cantFechas, $numeroFecha, $em)
    {

        $dato = $torneo->getDato();

        if ($numeroFecha == $cantFechas) {

            return ($dato->getFormaFinal() == "soloida") ? null
                : $em
                    ->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneo($torneo, $numeroFecha - 1);

        } else {

            return ($dato->getFormaElim() == "soloida") ? null
                : $em
                    ->getRepository('futfunBundle:FechasBase')
                    ->devolverFechaDelTorneo($torneo, $numeroFecha - 1);

        }


    }

    private function devolverFechaYaCreada($em, $numeroFecha, $torneo, $fechaBase)
    {

        if ($fechaBase == null && !is_numeric($numeroFecha)) {

            $fechaBase = $em->getRepository('futfunBundle:FechasBase')
                ->devolverFechaDelTorneo($torneo, 1);

        }

        return $fechaBase;

    }

}