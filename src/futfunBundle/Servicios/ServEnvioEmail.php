<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\User;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServEnvioEmail
{

    private $PATHURL = "http://localhost/futfun/web/";
    private $EMAILBASE = "leolma1980@gmail.com";

    public function mandarMailParaConfirmar(User $user, $token)
    {

        $url = $this->PATHURL . "confirmarru?ui=" .
            $user->getId() . "&tkn=" . $token . "&mtv=ac";
        $sfrom = $this->EMAILBASE; //cuenta que envia
        $ssubject = "Activación de nuevo usuario"; //subject
        $shtml = "Bienvenido estimado usuario a FutFun:<br /><br />";
        $shtml .= "Gracias por elegir nuestro sitio, ojala su permanecia sea
        lo mas placentera y exitosa posible<br />";
        $shtml .= "Para seguir con el proceso de activación del usuario
                tiene que acceder a este link:<br /><br />";
        $shtml .= "<a href='$url'>Ir a activar usuario</a><br /><br />"; //mensaje
        $shtml .= "Muchas gracias y los mejores deseos<br />";
        $shtml .= "de parte del equipo de FutFun<br /><br />";
        $sheader = "From: FutFun <" . $sfrom . ">\nReply-To:" . $sfrom . "\n";
        $sheader = $sheader . "X-Mailer:PHP/" . phpversion() . "\n";
        $sheader = $sheader . "Mime-Version: 1.0\n";
        $sheader = $sheader . "Content-Type: text/html";
        mail($user->getEmail(), $ssubject, $shtml, $sheader);

    }

    public function mandarMailParaClave(User $user, $token)
    {

        $url = $this->PATHURL . "confirmarru?ui=" .
            $user->getId() . "&tkn=" . $token . "&mtv=cl";
        $sfrom = $this->EMAILBASE; //cuenta que envia
        $ssubject = "Pedido de modificación de contraseña"; //subject
        $shtml = "Estimados usuario:<br /><br />";
        $shtml .= "El pedido de cambio de contraseña a sido procesado.<br />";
        $shtml .= "Para seguir con el proceso del cambio tiene que acceder a este link:<br /><br />";
        $shtml .= "<a href='$url'>Ir a recuperar contraseña</a><br /><br />"; //mensaje
        $shtml .= "Muchas gracias y los mejores deseos<br />";
        $shtml .= "de parte del equipo de FutFun<br /><br />";
        $sheader = "From: FutFun <" . $sfrom . ">\nReply-To:" . $sfrom . "\n";
        $sheader = $sheader . "X-Mailer:PHP/" . phpversion() . "\n";
        $sheader = $sheader . "Mime-Version: 1.0\n";
        $sheader = $sheader . "Content-Type: text/html";
        mail($user->getEmail(), $ssubject, $shtml, $sheader);

    }

}