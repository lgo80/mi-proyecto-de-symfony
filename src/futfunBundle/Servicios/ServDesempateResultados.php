<?php

namespace futfunBundle\Servicios;

use futfunBundle\Entity\DesempateDatos;
use futfunBundle\Entity\DesempateFecha;
use futfunBundle\Entity\TorneoBase;
use futfunBundle\Validador\ValidarDesempateResultados;
use futfunBundle\Validador\ValidarTorneoResultados;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class ServDesempateResultados
{

    public function validarDatos(
        $idTorneo, $em, $userLogueado, $numeroFecha,
        $direccion, Controller $controller)
    {

        $validar = new ValidarDesempateResultados();

        $respuesta = $validar->validarGet($idTorneo);

        if ($respuesta["isValid"]) {

            $torneo = $em->getRepository('futfunBundle:TorneoBase')->find($idTorneo);

            $respuesta = $validar->validarTorneo($torneo);

            if ($respuesta["isValid"]) {

                $desempateDatos = $em->getRepository('futfunBundle:DesempateDatos')
                    ->devolverXTorneo($torneo);

                $respuesta = $validar->validarDesempate($desempateDatos);

                if ($respuesta["isValid"]) {

                    $fechasDesempate = $em->getRepository('futfunBundle:DesempateFecha')
                        ->devolverFechaDelTorneoAll($torneo);

                    if (!empty($fechasDesempate)) {

                        $cantFechas = $em->getRepository('futfunBundle:DesempateDatos')
                            ->devolverCantidadFechasTotales($desempateDatos);

                        $fechaDesempate = $em->getRepository('futfunBundle:DesempateFecha')
                            ->devolverFechaEntrante($numeroFecha, $torneo, $direccion, $cantFechas);

                        if ($fechaDesempate != null) {

                            $isHabEditar = $validar->validarAdministrador($torneo, $userLogueado);

                            $definicion = $em->getRepository('futfunBundle:DesempateFecha')
                                ->devolverIsDefinicion($desempateDatos, $fechaDesempate->getNumeroFecha());

                            $isGolVisitante = ($definicion)
                                ? $desempateDatos->getGolVisitante()
                                : false;

                            $fechaAnterior = ($definicion)
                                ? $this->devolverFechaIda($desempateDatos
                                    , $fechaDesempate->getNumeroFecha(), $em)
                                : null;

                            $datosEliminacion = $em->getRepository('futfunBundle:DesempateDatosElimin')
                                ->devolverDatosXTorneoYFecha($torneo, $fechaDesempate->getNumeroFecha());

                            $serviciosUtiles = new ServFuncionesUtiles();
                            $arrayFechas = $serviciosUtiles
                                ->formatearFechaEspanol($fechaDesempate);

                            $titulo = $controller->get('translator')
                                ->trans("Complete/edit results");

                            $nombreTorneoLugar = "CompletarResultadoDesempate";

                            $botonEditar = $controller->get('translator')
                                ->trans("Edit result");

                            $botonGuardar = $controller->get('translator')
                                ->trans("Save Results");

                        } else {

                            $respuesta = array("isValid" => false,
                                "lugar" => 'trabajo_dbbundle_torneos_crear_fechas_desempate',
                                "mensaje" => '�La fecha ingresada no esta creada todavia!',
                                "nombreError" => "mensaje-danger");

                        }


                    } else {

                        $respuesta = array("isValid" => false,
                            "lugar" => 'trabajo_dbbundle_torneos_ver_datos_desempate',
                            "mensaje" => '�No hay fechas creadas para completar algun resultado todavia!',
                            "nombreError" => "mensaje-danger");

                    }

                }

            }


        }

        return compact("respuesta", "desempateDatos", "numeroFecha",
            "fechaDesempate", "fechasDesempate", "cantFechas",
            "isHabEditar", "definicion", "isGolVisitante",
            "fechaAnterior", "datosEliminacion", "arrayFechas",
            "titulo", "nombreTorneoLugar", "botonEditar", "botonGuardar");

    }

    public function armarFechaConResultados(
        DesempateFecha $desempateFecha, $form)
    {

        foreach ($desempateFecha->getDesempatePartidos() as &$valorPart) {

            $valorLocal = $form->get('valorLocal' . $valorPart->getNumeroPartido())->getData();
            $valorVisita = $form->get('valorVisita' . $valorPart->getNumeroPartido())->getData();

            $valorPart->setvalorLocal(
                (is_numeric($valorVisita) && is_numeric($valorLocal))
                    ? $valorLocal : null);
            $valorPart->setvalorVisita(
                (is_numeric($valorVisita) && is_numeric($valorLocal))
                    ? $valorVisita : null);

            $valorDefLocal = $form->get('valorDefinicionLocal' . $valorPart->getNumeroPartido())->getData();
            $valorDefVisita = $form->get('valorDefinicionVisita' . $valorPart->getNumeroPartido())->getData();

            $valorPart->setValorDefinicionLocal(
                (is_numeric($valorDefLocal) && is_numeric($valorDefVisita))
                    ? $valorDefLocal : null);
            $valorPart->setValorDefinicionVisita(
                (is_numeric($valorDefLocal) && is_numeric($valorDefVisita))
                    ? $valorDefVisita : null);

        }


        return $desempateFecha;

    }

    public function verSiEsLiga(TorneoBase $torneo, $numerFecha, $em)
    {

        $tipoTorneo = $em->getRepository('futfunBundle:TipoTorneo')
            ->devolverTipoTorneo($torneo->getTipoTorneo());

        switch ($tipoTorneo) {

            case "l":

                return true;

            case "lye":

                $cantFechasLiga = $em->getRepository('futfunBundle:TorneoBase')
                    ->devolverCantidadFechasLiga($torneo);

                return ($numerFecha <= $cantFechasLiga);

            case "e":

                return false;

            default:

                return false;

        }


    }

    public function verSiHayApuestaCreada($torneo, $em)
    {

        $apuestasBase = $em->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasPorTorneo($torneo);

        return $apuestasBase;

    }

    private function devolverFechaIda(DesempateDatos $desempateDatos, $numeroFecha, $em)
    {

        return ($desempateDatos->getForma() == "soloida") ? null
            : $em->getRepository('futfunBundle:DesempateFecha')
                ->devolverFechaDelDesempate($desempateDatos->getTorneo(), $numeroFecha - 1);

    }

}