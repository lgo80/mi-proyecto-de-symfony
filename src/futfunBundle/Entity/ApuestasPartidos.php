<?php

namespace futfunBundle\Entity;

/**
 * ApuestasPartidos
 */
class ApuestasPartidos
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $resultadoLocal;

    /**
     * @var integer
     */
    private $resultadoVisita;

    /**
     * @var integer
     */
    private $puntos;

    /**
     * @var \futfunBundle\Entity\PartidoFecha
     */
    private $partidoFecha;

    /**
     * @var \futfunBundle\Entity\DesempatePartido
     */
    private $desempatePartido;

    /**
     * @var \futfunBundle\Entity\ApuestasFechas
     */
    private $apuestasFecha;

    public function __construct()
    {
        $this->puntos = null;
        $this->desempatePartido = null;
        $this->partidoFecha = null;
        $this->resultadoLocal = null;
        $this->resultadoVisita = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resultadoLocal
     *
     * @param integer $resultadoLocal
     *
     * @return ApuestasPartidos
     */
    public function setResultadoLocal($resultadoLocal)
    {
        $this->resultadoLocal = $resultadoLocal;

        return $this;
    }

    /**
     * Get resultadoLocal
     *
     * @return integer
     */
    public function getResultadoLocal()
    {
        return $this->resultadoLocal;
    }

    /**
     * Set resultadoVisita
     *
     * @param integer $resultadoVisita
     *
     * @return ApuestasPartidos
     */
    public function setResultadoVisita($resultadoVisita)
    {
        $this->resultadoVisita = $resultadoVisita;

        return $this;
    }

    /**
     * Get resultadoVisita
     *
     * @return integer
     */
    public function getResultadoVisita()
    {
        return $this->resultadoVisita;
    }

    /**
     * Set puntos
     *
     * @param integer $puntos
     *
     * @return ApuestasPartidos
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos
     *
     * @return integer
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set partidoFecha
     *
     * @param \futfunBundle\Entity\PartidoFecha $partidoFecha
     *
     * @return ApuestasPartidos
     */
    public function setPartidoFecha(\futfunBundle\Entity\PartidoFecha $partidoFecha = null)
    {
        $this->partidoFecha = $partidoFecha;

        return $this;
    }

    /**
     * Get partidoFecha
     *
     * @return \futfunBundle\Entity\PartidoFecha
     */
    public function getPartidoFecha()
    {
        return $this->partidoFecha;
    }

    /**
     * Set desempatePartido
     *
     * @param \futfunBundle\Entity\DesempatePartido $desempatePartido
     *
     * @return ApuestasPartidos
     */
    public function setDesempatePartido(\futfunBundle\Entity\DesempatePartido $desempatePartido = null)
    {
        $this->desempatePartido = $desempatePartido;

        return $this;
    }

    /**
     * Get desempatePartido
     *
     * @return \futfunBundle\Entity\DesempatePartido
     */
    public function getDesempatePartido()
    {
        return $this->desempatePartido;
    }

    /**
     * Set apuestasFecha
     *
     * @param \futfunBundle\Entity\ApuestasFechas $apuestasFecha
     *
     * @return ApuestasPartidos
     */
    public function setApuestasFecha(\futfunBundle\Entity\ApuestasFechas $apuestasFecha = null)
    {
        $this->apuestasFecha = $apuestasFecha;

        return $this;
    }

    /**
     * Get apuestasFecha
     *
     * @return \futfunBundle\Entity\ApuestasFechas
     */
    public function getApuestasFecha()
    {
        return $this->apuestasFecha;
    }
}
