<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * datosTorneo
 */
class DatosTorneo
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $cantEquipos;

    /**
     * @var integer
     */
    private $cantGrupos;

    /**
     * @var integer
     */
    private $equipGrupos;

    /**
     * @var string
     */
    private $formaGrupo;

    /**
     * @var string
     */
    private $vueltaGrupo;

    /**
     * @var integer
     */
    private $clasPorGrupo;

    /**
     * @var integer
     */
    private $clasAdic;

    /**
     * @var string
     */
    private $tipoClasificacion;

    /**
     * @var integer
     */
    private $fechasAdic;

    /**
     * @var string
     */
    private $comienzoElim;

    /**
     * @var string
     */
    private $formaElim;

    /**
     * @var boolean
     */
    private $partTercerPuesto;

    /**
     * @var string
     */
    private $formaFinal;

    /**
     * @var boolean
     */
    private $partidoDesempate;

    /**
     * @var string
     */
    private $golVisitante;

    /**
     * @var integer
     */
    private $puntosPartGanados;

    /**
     * @var integer
     */
    private $puntosPartEmpatados;

    /**
     * @var integer
     */
    private $puntosPartPerdidos;

    public function __construct()
    {
        $this->cantGrupos = null;
        $this->equipGrupos = null;
        $this->formaGrupo = null;
        $this->vueltaGrupo = null;
        $this->clasPorGrupo = null;
        $this->clasAdic = null;
        $this->fechasAdic = null;
        $this->comienzoElim = null;
        $this->formaElim = null;
        $this->partTercerPuesto = null;
        $this->golVisitante = null;
        $this->formaFinal = null;
        $this->puntosPartGanados = 3;
        $this->puntosPartEmpatados = 1;
        $this->puntosPartPerdidos = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantEquipos
     *
     * @param integer $cantEquipos
     * @return DatosTorneo
     */
    public function setCantEquipos($cantEquipos)
    {
        $this->cantEquipos = $cantEquipos;

        return $this;
    }

    /**
     * Get cantEquipos
     *
     * @return integer
     */
    public function getCantEquipos()
    {
        return $this->cantEquipos;
    }

    /**
     * Set cantGrupos
     *
     * @param integer $cantGrupos
     * @return DatosTorneo
     */
    public function setCantGrupos($cantGrupos)
    {
        $this->cantGrupos = $cantGrupos;

        return $this;
    }

    /**
     * Get cantGrupos
     *
     * @return integer
     */
    public function getCantGrupos()
    {
        return $this->cantGrupos;
    }

    /**
     * Set equipGrupos
     *
     * @param integer $equipGrupos
     * @return DatosTorneo
     */
    public function setEquipGrupos($equipGrupos)
    {
        $this->equipGrupos = $equipGrupos;

        return $this;
    }

    /**
     * Get equipGrupos
     *
     * @return integer
     */
    public function getEquipGrupos()
    {
        return $this->equipGrupos;
    }

    /**
     * Set formaGrupo
     *
     * @param string $formaGrupo
     * @return DatosTorneo
     */
    public function setFormaGrupo($formaGrupo)
    {
        $this->formaGrupo = $formaGrupo;

        return $this;
    }

    /**
     * Get formaGrupo
     *
     * @return string
     */
    public function getFormaGrupo()
    {
        return $this->formaGrupo;
    }

    /**
     * Set vueltaGrupo
     *
     * @param string $vueltaGrupo
     * @return DatosTorneo
     */
    public function setVueltaGrupo($vueltaGrupo)
    {
        $this->vueltaGrupo = $vueltaGrupo;

        return $this;
    }

    /**
     * Get vueltaGrupo
     *
     * @return string
     */
    public function getVueltaGrupo()
    {
        return $this->vueltaGrupo;
    }

    /**
     * Set clasPorGrupo
     *
     * @param integer $clasPorGrupo
     * @return DatosTorneo
     */
    public function setClasPorGrupo($clasPorGrupo)
    {
        $this->clasPorGrupo = $clasPorGrupo;

        return $this;
    }

    /**
     * Get clasPorGrupo
     *
     * @return integer
     */
    public function getClasPorGrupo()
    {
        return $this->clasPorGrupo;
    }

    /**
     * Set clasAdic
     *
     * @param integer $clasAdic
     * @return DatosTorneo
     */
    public function setClasAdic($clasAdic)
    {
        $this->clasAdic = $clasAdic;

        return $this;
    }

    /**
     * Get clasAdic
     *
     * @return integer
     */
    public function getClasAdic()
    {
        return $this->clasAdic;
    }

    /**
     * Set tipoClasificacion
     *
     * @param string $tipoClasificacion
     * @return DatosTorneo
     */
    public function setTipoClasificacion($tipoClasificacion)
    {
        $this->tipoClasificacion = $tipoClasificacion;

        return $this;
    }

    /**
     * Get tipoClasificacion
     *
     * @return string
     */
    public function getTipoClasificacion()
    {
        return $this->tipoClasificacion;
    }

    /**
     * Set fechasAdic
     *
     * @param integer $fechasAdic
     * @return DatosTorneo
     */
    public function setFechasAdic($fechasAdic)
    {
        $this->fechasAdic = $fechasAdic;

        return $this;
    }

    /**
     * Get fechasAdic
     *
     * @return integer
     */
    public function getFechasAdic()
    {
        return $this->fechasAdic;
    }

    /**
     * Set comienzoElim
     *
     * @param string $comienzoElim
     * @return DatosTorneo
     */
    public function setComienzoElim($comienzoElim)
    {
        $this->comienzoElim = $comienzoElim;

        return $this;
    }

    /**
     * Get comienzoElim
     *
     * @return string
     */
    public function getComienzoElim()
    {
        return $this->comienzoElim;
    }

    /**
     * Set formaElim
     *
     * @param string $formaElim
     * @return DatosTorneo
     */
    public function setFormaElim($formaElim)
    {
        $this->formaElim = $formaElim;

        return $this;
    }

    /**
     * Get formaElim
     *
     * @return string
     */
    public function getFormaElim()
    {
        return $this->formaElim;
    }

    /**
     * Set partTercerPuesto
     *
     * @param boolean $partTercerPuesto
     * @return DatosTorneo
     */
    public function setPartTercerPuesto($partTercerPuesto)
    {
        $this->partTercerPuesto = $partTercerPuesto;

        return $this;
    }

    /**
     * Get partTercerPuesto
     *
     * @return boolean
     */
    public function getPartTercerPuesto()
    {
        return $this->partTercerPuesto;
    }

    /**
     * Set formaFinal
     *
     * @param string $formaFinal
     * @return DatosTorneo
     */
    public function setFormaFinal($formaFinal)
    {
        $this->formaFinal = $formaFinal;

        return $this;
    }

    /**
     * Get formaFinal
     *
     * @return string
     */
    public function getFormaFinal()
    {
        return $this->formaFinal;
    }

    /**
     * Set partidoDesempate
     *
     * @param boolean $partidoDesempate
     * @return DatosTorneo
     */
    public function setPartidoDesempate($partidoDesempate)
    {
        $this->partidoDesempate = $partidoDesempate;

        return $this;
    }

    /**
     * Get partidoDesempate
     *
     * @return boolean
     */
    public function getPartidoDesempate()
    {
        return $this->partidoDesempate;
    }

    /**
     * Set golVisitante
     *
     * @param string $golVisitante
     * @return DatosTorneo
     */
    public function setGolVisitante($golVisitante)
    {
        $this->golVisitante = $golVisitante;

        return $this;
    }

    /**
     * Get golVisitante
     *
     * @return string
     */
    public function getGolVisitante()
    {
        return $this->golVisitante;
    }

    /**
     * Set puntosPartGanados
     *
     * @param integer $puntosPartGanados
     * @return DatosTorneo
     */
    public function setPuntosPartGanados($puntosPartGanados)
    {
        $this->puntosPartGanados = $puntosPartGanados;

        return $this;
    }

    /**
     * Get puntosPartGanados
     *
     * @return integer
     */
    public function getPuntosPartGanados()
    {
        return $this->puntosPartGanados;
    }

    /**
     * Set puntosPartEmpatados
     *
     * @param integer $puntosPartEmpatados
     * @return DatosTorneo
     */
    public function setPuntosPartEmpatados($puntosPartEmpatados)
    {
        $this->puntosPartEmpatados = $puntosPartEmpatados;

        return $this;
    }

    /**
     * Get puntosPartEmpatados
     *
     * @return integer
     */
    public function getPuntosPartEmpatados()
    {
        return $this->puntosPartEmpatados;
    }

    /**
     * Set puntosPartPerdidos
     *
     * @param integer $puntosPartPerdidos
     * @return DatosTorneo
     */
    public function setPuntosPartPerdidos($puntosPartPerdidos)
    {
        $this->puntosPartPerdidos = $puntosPartPerdidos;

        return $this;
    }

    /**
     * Get puntosPartPerdidos
     *
     * @return integer
     */
    public function getPuntosPartPerdidos()
    {
        return $this->puntosPartPerdidos;
    }
    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $dato;


    /**
     * Set dato
     *
     * @param \futfunBundle\Entity\TorneoBase $dato
     * @return DatosTorneo
     */
    public function setDato(\futfunBundle\Entity\TorneoBase $dato = null)
    {
        $this->dato = $dato;

        return $this;
    }

    /**
     * Get dato
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getDato()
    {
        return $this->dato;
    }
}
