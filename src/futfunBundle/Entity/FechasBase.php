<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * FechasBase
 */
class FechasBase
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $numeroFecha;

    /**
     * @var string
     */
    private $nombreFecha;

    /**
     * @var \DateTime
     */
    private $fechaInicioAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $grupoFechas;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;

    public function __construct()
    {

        $this->grupoFechas = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroFecha
     *
     * @param integer $numeroFecha
     * @return FechasBase
     */
    public function setNumeroFecha($numeroFecha)
    {
        $this->numeroFecha = $numeroFecha;

        return $this;
    }

    /**
     * Get numeroFecha
     *
     * @return integer
     */
    public function getNumeroFecha()
    {
        return $this->numeroFecha;
    }

    /**
     * Set nombreFecha
     *
     * @param string $nombreFecha
     * @return FechasBase
     */
    public function setNombreFecha($nombreFecha)
    {
        $this->nombreFecha = $nombreFecha;

        return $this;
    }

    /**
     * Get nombreFecha
     *
     * @return string
     */
    public function getNombreFecha()
    {
        return $this->nombreFecha;
    }

    /**
     * Set fechaInicioAt
     *
     * @param \DateTime $fechaInicioAt
     * @return FechasBase
     */
    public function setFechaInicioAt($fechaInicioAt)
    {
        $this->fechaInicioAt = $fechaInicioAt;

        return $this;
    }

    /**
     * Get fechaInicioAt
     *
     * @return \DateTime
     */
    public function getFechaInicioAt()
    {
        return $this->fechaInicioAt;
    }

    /**
     * Add grupoFechas
     *
     * @param \futfunBundle\Entity\GrupoFecha $grupoFechas
     * @return FechasBase
     */
    public function addGrupoFecha(\futfunBundle\Entity\GrupoFecha $grupoFechas)
    {
        $this->grupoFechas[] = $grupoFechas;

        return $this;
    }

    /**
     * Remove grupoFechas
     *
     * @param \futfunBundle\Entity\GrupoFecha $grupoFechas
     */
    public function removeGrupoFecha(\futfunBundle\Entity\GrupoFecha $grupoFechas)
    {
        $this->grupoFechas->removeElement($grupoFechas);
    }

    /**
     * Get grupoFechas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupoFechas()
    {
        return $this->grupoFechas;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return FechasBase
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }
}
