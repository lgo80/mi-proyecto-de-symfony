<?php

namespace futfunBundle\Entity;

/**
 * PizarraMensaje
 */
class PizarraMensaje
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaAt;

    /**
     * @var string
     */
    private $mensaje;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @var \futfunBundle\Entity\ApuestaBase
     */
    private $pizarraApuesta;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $emisor;

    public function __construct()
    {
        $this->fechaAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaAt
     *
     * @param \DateTime $fechaAt
     *
     * @return PizarraMensaje
     */
    public function setFechaAt($fechaAt)
    {
        $this->fechaAt = $fechaAt;

        return $this;
    }

    /**
     * Get fechaAt
     *
     * @return \DateTime
     */
    public function getFechaAt()
    {
        return $this->fechaAt;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     *
     * @return PizarraMensaje
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return PizarraMensaje
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set pizarraApuesta
     *
     * @param \futfunBundle\Entity\ApuestaBase $pizarraApuesta
     *
     * @return PizarraMensaje
     */
    public function setPizarraApuesta(\futfunBundle\Entity\ApuestaBase $pizarraApuesta = null)
    {
        $this->pizarraApuesta = $pizarraApuesta;

        return $this;
    }

    /**
     * Get pizarraApuesta
     *
     * @return \futfunBundle\Entity\ApuestaBase
     */
    public function getPizarraApuesta()
    {
        return $this->pizarraApuesta;
    }

    /**
     * Set emisor
     *
     * @param \futfunBundle\Entity\User $emisor
     *
     * @return PizarraMensaje
     */
    public function setEmisor(\futfunBundle\Entity\User $emisor = null)
    {
        $this->emisor = $emisor;

        return $this;
    }

    /**
     * Get emisor
     *
     * @return \futfunBundle\Entity\User
     */
    public function getEmisor()
    {
        return $this->emisor;
    }
}
