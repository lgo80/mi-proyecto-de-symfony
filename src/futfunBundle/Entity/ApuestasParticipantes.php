<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApuestasParticipantes
 */
class ApuestasParticipantes
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $puntos;

    /**
     * @var integer
     */
    private $dateWin;

    /**
     * @var integer
     */
    private $pe;

    /**
     * @var integer
     */
    private $pr;

    /**
     * @var integer
     */
    private $pp;

    /**
     * @var integer
     */
    private $ptsExtras;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var \futfunBundle\Entity\ApuestaBase
     */
    private $apuestasBase;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $user;

    public function __construct()
    {
        $this->isActive = true;
        $this->puntos = 0;
        $this->dateWin = 0;
        $this->pe = 0;
        $this->pr = 0;
        $this->pp = 0;
        $this->ptsExtras = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set puntos
     *
     * @param integer $puntos
     *
     * @return ApuestasParticipantes
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos
     *
     * @return integer
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set dateWin
     *
     * @param integer $dateWin
     *
     * @return ApuestasParticipantes
     */
    public function setDateWin($dateWin)
    {
        $this->dateWin = $dateWin;

        return $this;
    }

    /**
     * Get dateWin
     *
     * @return integer
     */
    public function getDateWin()
    {
        return $this->dateWin;
    }

    /**
     * Set pe
     *
     * @param integer $pe
     *
     * @return ApuestasParticipantes
     */
    public function setPe($pe)
    {
        $this->pe = $pe;

        return $this;
    }

    /**
     * Get pe
     *
     * @return integer
     */
    public function getPe()
    {
        return $this->pe;
    }

    /**
     * Set pr
     *
     * @param integer $pr
     *
     * @return ApuestasParticipantes
     */
    public function setPr($pr)
    {
        $this->pr = $pr;

        return $this;
    }

    /**
     * Get pr
     *
     * @return integer
     */
    public function getPr()
    {
        return $this->pr;
    }

    /**
     * Set pp
     *
     * @param integer $pp
     *
     * @return ApuestasParticipantes
     */
    public function setPp($pp)
    {
        $this->pp = $pp;

        return $this;
    }

    /**
     * Get pp
     *
     * @return integer
     */
    public function getPp()
    {
        return $this->pp;
    }

    /**
     * Set ptsExtras
     *
     * @param integer $ptsExtras
     *
     * @return ApuestasParticipantes
     */
    public function setPtsExtras($ptsExtras)
    {
        $this->ptsExtras = $ptsExtras;

        return $this;
    }

    /**
     * Get ptsExtras
     *
     * @return integer
     */
    public function getPtsExtras()
    {
        return $this->ptsExtras;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return ApuestasParticipantes
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set apuestasBase
     *
     * @param \futfunBundle\Entity\ApuestaBase $apuestasBase
     *
     * @return ApuestasParticipantes
     */
    public function setApuestasBase(\futfunBundle\Entity\ApuestaBase $apuestasBase = null)
    {
        $this->apuestasBase = $apuestasBase;

        return $this;
    }

    /**
     * Get apuestasBase
     *
     * @return \futfunBundle\Entity\ApuestaBase
     */
    public function getApuestasBase()
    {
        return $this->apuestasBase;
    }

    /**
     * Set user
     *
     * @param \futfunBundle\Entity\User $user
     *
     * @return ApuestasParticipantes
     */
    public function setUser(\futfunBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \futfunBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
