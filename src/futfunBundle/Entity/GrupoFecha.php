<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GrupoFecha
 */
class GrupoFecha
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $partidoFechas;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $equipoLibre;

    /**
     * @var \futfunBundle\Entity\Grupo
     */
    private $grupo;

    /**
     * @var \futfunBundle\Entity\FechasBase
     */
    private $fechaBase;

    public function __construct()
    {

        $this->partidoFechas = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add partidoFechas
     *
     * @param \futfunBundle\Entity\PartidoFecha $partidoFechas
     * @return GrupoFecha
     */
    public function addPartidoFecha(\futfunBundle\Entity\PartidoFecha $partidoFechas)
    {
        $this->partidoFechas[] = $partidoFechas;

        return $this;
    }

    /**
     * Remove partidoFechas
     *
     * @param \futfunBundle\Entity\PartidoFecha $partidoFechas
     */
    public function removePartidoFecha(\futfunBundle\Entity\PartidoFecha $partidoFechas)
    {
        $this->partidoFechas->removeElement($partidoFechas);
    }

    /**
     * Get partidoFechas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartidoFechas()
    {
        return $this->partidoFechas;
    }

    /**
     * Set equipoLibre
     *
     * @param \futfunBundle\Entity\Clubes $equipoLibre
     * @return GrupoFecha
     */
    public function setEquipoLibre(\futfunBundle\Entity\Clubes $equipoLibre = null)
    {
        $this->equipoLibre = $equipoLibre;

        return $this;
    }

    /**
     * Get equipoLibre
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getEquipoLibre()
    {
        return $this->equipoLibre;
    }

    /**
     * Set grupo
     *
     * @param \futfunBundle\Entity\Grupo $grupo
     * @return GrupoFecha
     */
    public function setGrupo(\futfunBundle\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \futfunBundle\Entity\Grupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set fechaBase
     *
     * @param \futfunBundle\Entity\FechasBase $fechaBase
     * @return GrupoFecha
     */
    public function setFechaBase(\futfunBundle\Entity\FechasBase $fechaBase = null)
    {
        $this->fechaBase = $fechaBase;

        return $this;
    }

    /**
     * Get fechaBase
     *
     * @return \futfunBundle\Entity\FechasBase
     */
    public function getFechaBase()
    {
        return $this->fechaBase;
    }
}
