<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Grupo
 */
class Grupo
{
    public function __construct()
    {
        $this->posiciones = new ArrayCollection();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $posiciones;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add posiciones
     *
     * @param \futfunBundle\Entity\PosicionesLiga $posiciones
     * @return Grupo
     */
    public function addPosicione(\futfunBundle\Entity\PosicionesLiga $posiciones)
    {
        $this->posiciones[] = $posiciones;

        return $this;
    }

    /**
     * Remove posiciones
     *
     * @param \futfunBundle\Entity\PosicionesLiga $posiciones
     */
    public function removePosicione(\futfunBundle\Entity\PosicionesLiga $posiciones)
    {
        $this->posiciones->removeElement($posiciones);
    }

    /**
     * Get posiciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }
}
