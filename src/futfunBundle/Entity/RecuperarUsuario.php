<?php

namespace futfunBundle\Entity;

/**
 * RecuperarUsuario
 */
class RecuperarUsuario
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var string
     */
    private $token;

    /**
     * @var \DateTime
     */
    private $fechaLimiteAt;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $user;

    /**
     * @var boolean
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return RecuperarUsuario
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return RecuperarUsuario
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set fechaLimiteAt
     *
     * @param \DateTime $fechaLimiteAt
     *
     * @return RecuperarUsuario
     */
    public function setFechaLimiteAt($fechaLimiteAt)
    {
        $this->fechaLimiteAt = $fechaLimiteAt;

        return $this;
    }

    /**
     * Get fechaLimiteAt
     *
     * @return \DateTime
     */
    public function getFechaLimiteAt()
    {
        return $this->fechaLimiteAt;
    }

    /**
     * Set user
     *
     * @param \futfunBundle\Entity\User $user
     *
     * @return RecuperarUsuario
     */
    public function setUser(\futfunBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \futfunBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return RecuperarUsuario
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
