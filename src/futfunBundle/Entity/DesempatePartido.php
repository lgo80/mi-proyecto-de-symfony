<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesempatePartido
 */
class DesempatePartido
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $numeroPartido;

    /**
     * @var int
     */
    private $valorLocal;

    /**
     * @var int
     */
    private $valorVisita;

    /**
     * @var integer
     */
    private $valorDefinicionLocal;

    /**
     * @var integer
     */
    private $valorDefinicionVisita;

    /**
     * @var \DateTime
     */
    private $fechaPartidoAt;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $local;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $visita;

    /**
     * @var \futfunBundle\Entity\DesempateFecha
     */
    private $desempateFecha;

    public function __construct()
    {
        $this->valorLocal = null;
        $this->valorVisita = null;
        $this->valorDefinicionLocal = null;
        $this->valorDefinicionVisita = null;
        $this->fechaPartidoAt = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroPartido
     *
     * @param integer $numeroPartido
     * @return DesempatePartido
     */
    public function setNumeroPartido($numeroPartido)
    {
        $this->numeroPartido = $numeroPartido;

        return $this;
    }

    /**
     * Get numeroPartido
     *
     * @return integer
     */
    public function getNumeroPartido()
    {
        return $this->numeroPartido;
    }

    /**
     * Set valorLocal
     *
     * @param integer $valorLocal
     * @return DesempatePartido
     */
    public function setValorLocal($valorLocal)
    {
        $this->valorLocal = $valorLocal;

        return $this;
    }

    /**
     * Get valorLocal
     *
     * @return integer
     */
    public function getValorLocal()
    {
        return $this->valorLocal;
    }

    /**
     * Set valorVisita
     *
     * @param integer $valorVisita
     * @return DesempatePartido
     */
    public function setValorVisita($valorVisita)
    {
        $this->valorVisita = $valorVisita;

        return $this;
    }

    /**
     * Get valorVisita
     *
     * @return integer
     */
    public function getValorVisita()
    {
        return $this->valorVisita;
    }

    /**
     * Set valorDefinicionLocal
     *
     * @param integer $valorDefinicionLocal
     * @return DesempatePartido
     */
    public function setValorDefinicionLocal($valorDefinicionLocal)
    {
        $this->valorDefinicionLocal = $valorDefinicionLocal;

        return $this;
    }

    /**
     * Get valorDefinicionLocal
     *
     * @return integer
     */
    public function getValorDefinicionLocal()
    {
        return $this->valorDefinicionLocal;
    }

    /**
     * Set valorDefinicionVisita
     *
     * @param integer $valorDefinicionVisita
     * @return DesempatePartido
     */
    public function setValorDefinicionVisita($valorDefinicionVisita)
    {
        $this->valorDefinicionVisita = $valorDefinicionVisita;

        return $this;
    }

    /**
     * Get valorDefinicionVisita
     *
     * @return integer
     */
    public function getValorDefinicionVisita()
    {
        return $this->valorDefinicionVisita;
    }

    /**
     * Set fechaPartidoAt
     *
     * @param \DateTime $fechaPartidoAt
     * @return DesempatePartido
     */
    public function setFechaPartidoAt($fechaPartidoAt)
    {
        $this->fechaPartidoAt = $fechaPartidoAt;

        return $this;
    }

    /**
     * Get fechaPartidoAt
     *
     * @return \DateTime
     */
    public function getFechaPartidoAt()
    {
        return $this->fechaPartidoAt;
    }

    /**
     * Set local
     *
     * @param \futfunBundle\Entity\Clubes $local
     * @return DesempatePartido
     */
    public function setLocal(\futfunBundle\Entity\Clubes $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set visita
     *
     * @param \futfunBundle\Entity\Clubes $visita
     * @return DesempatePartido
     */
    public function setVisita(\futfunBundle\Entity\Clubes $visita = null)
    {
        $this->visita = $visita;

        return $this;
    }

    /**
     * Get visita
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getVisita()
    {
        return $this->visita;
    }

    /**
     * Set desempateFecha
     *
     * @param \futfunBundle\Entity\DesempateFecha $desempateFecha
     * @return DesempatePartido
     */
    public function setDesempateFecha(\futfunBundle\Entity\DesempateFecha $desempateFecha = null)
    {
        $this->desempateFecha = $desempateFecha;

        return $this;
    }

    /**
     * Get desempateFecha
     *
     * @return \futfunBundle\Entity\DesempateFecha
     */
    public function getDesempateFecha()
    {
        return $this->desempateFecha;
    }
}
