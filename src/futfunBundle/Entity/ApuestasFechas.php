<?php

namespace futfunBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ApuestasFechas
 */
class ApuestasFechas
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaModificacionAt;

    /**
     * @var boolean
     */
    private $isDateWin;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $apuestasPartidos;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $user;

    /**
     * @var \futfunBundle\Entity\ApuestaBase
     */
    private $apuestaBase;

    /**
     * @var \futfunBundle\Entity\FechasBase
     */
    private $fechaBase;

    /**
     * @var \futfunBundle\Entity\DesempateFecha
     */
    private $fechaDesempate;

    public function __construct()
    {
        $this->fechaModificacionAt = new \DateTime();
        $this->apuestasPartidos = new ArrayCollection();
        $this->isDateWin = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaModificacionAt
     *
     * @param \DateTime $fechaModificacionAt
     *
     * @return ApuestasFechas
     */
    public function setFechaModificacionAt($fechaModificacionAt)
    {
        $this->fechaModificacionAt = $fechaModificacionAt;

        return $this;
    }

    /**
     * Get fechaModificacionAt
     *
     * @return \DateTime
     */
    public function getFechaModificacionAt()
    {
        return $this->fechaModificacionAt;
    }

    /**
     * Set isDateWin
     *
     * @param boolean $isDateWin
     *
     * @return ApuestasFechas
     */
    public function setIsDateWin($isDateWin)
    {
        $this->isDateWin = $isDateWin;

        return $this;
    }

    /**
     * Get isDateWin
     *
     * @return boolean
     */
    public function getIsDateWin()
    {
        return $this->isDateWin;
    }

    /**
     * Add apuestasPartido
     *
     * @param \futfunBundle\Entity\ApuestasPartidos $apuestasPartido
     *
     * @return ApuestasFechas
     */
    public function addApuestasPartido(\futfunBundle\Entity\ApuestasPartidos $apuestasPartido)
    {
        $this->apuestasPartidos[] = $apuestasPartido;

        return $this;
    }

    /**
     * Remove apuestasPartido
     *
     * @param \futfunBundle\Entity\ApuestasPartidos $apuestasPartido
     */
    public function removeApuestasPartido(\futfunBundle\Entity\ApuestasPartidos $apuestasPartido)
    {
        $this->apuestasPartidos->removeElement($apuestasPartido);
    }

    /**
     * Get apuestasPartidos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApuestasPartidos()
    {
        return $this->apuestasPartidos;
    }

    /**
     * Set user
     *
     * @param \futfunBundle\Entity\User $user
     *
     * @return ApuestasFechas
     */
    public function setUser(\futfunBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \futfunBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set apuestaBase
     *
     * @param \futfunBundle\Entity\ApuestaBase $apuestaBase
     *
     * @return ApuestasFechas
     */
    public function setApuestaBase(\futfunBundle\Entity\ApuestaBase $apuestaBase = null)
    {
        $this->apuestaBase = $apuestaBase;

        return $this;
    }

    /**
     * Get apuestaBase
     *
     * @return \futfunBundle\Entity\ApuestaBase
     */
    public function getApuestaBase()
    {
        return $this->apuestaBase;
    }

    /**
     * Set fechaBase
     *
     * @param \futfunBundle\Entity\FechasBase $fechaBase
     *
     * @return ApuestasFechas
     */
    public function setFechaBase(\futfunBundle\Entity\FechasBase $fechaBase = null)
    {
        $this->fechaBase = $fechaBase;

        return $this;
    }

    /**
     * Get fechaBase
     *
     * @return \futfunBundle\Entity\FechasBase
     */
    public function getFechaBase()
    {
        return $this->fechaBase;
    }

    /**
     * Set fechaDesempate
     *
     * @param \futfunBundle\Entity\DesempateFecha $fechaDesempate
     *
     * @return ApuestasFechas
     */
    public function setFechaDesempate(\futfunBundle\Entity\DesempateFecha $fechaDesempate = null)
    {
        $this->fechaDesempate = $fechaDesempate;

        return $this;
    }

    /**
     * Get fechaDesempate
     *
     * @return \futfunBundle\Entity\DesempateFecha
     */
    public function getFechaDesempate()
    {
        return $this->fechaDesempate;
    }
}
