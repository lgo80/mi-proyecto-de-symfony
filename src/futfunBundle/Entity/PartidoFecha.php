<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FechaBaseDetalle
 */
class PartidoFecha
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $numeroPartido;

    /**
     * @var integer
     */
    private $valorLocal;

    /**
     * @var integer
     */
    private $valorVisita;

    /**
     * @var integer
     */
    private $valorDefinicionLocal;

    /**
     * @var integer
     */
    private $valorDefinicionVisita;

    /**
     * @var \DateTime
     */
    private $fechaPartidoAt;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $local;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $visita;

    /**
     * @var \futfunBundle\Entity\GrupoFecha
     */
    private $grupoFecha;


    public function __construct()
    {
        $this->valorLocal = null;
        $this->valorVisita = null;
        $this->valorDefinicionLocal = null;
        $this->valorDefinicionVisita = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroPartido
     *
     * @param integer $numeroPartido
     * @return PartidoFecha
     */
    public function setNumeroPartido($numeroPartido)
    {
        $this->numeroPartido = $numeroPartido;

        return $this;
    }

    /**
     * Get numeroPartido
     *
     * @return integer
     */
    public function getNumeroPartido()
    {
        return $this->numeroPartido;
    }

    /**
     * Set valorLocal
     *
     * @param integer $valorLocal
     * @return PartidoFecha
     */
    public function setValorLocal($valorLocal)
    {
        $this->valorLocal = $valorLocal;

        return $this;
    }

    /**
     * Get valorLocal
     *
     * @return integer
     */
    public function getValorLocal()
    {
        return $this->valorLocal;
    }

    /**
     * Set valorVisita
     *
     * @param integer $valorVisita
     * @return PartidoFecha
     */
    public function setValorVisita($valorVisita)
    {
        $this->valorVisita = $valorVisita;

        return $this;
    }

    /**
     * Get valorVisita
     *
     * @return integer
     */
    public function getValorVisita()
    {
        return $this->valorVisita;
    }

    /**
     * Set fechaPartidoAt
     *
     * @param \DateTime $fechaPartidoAt
     * @return PartidoFecha
     */
    public function setFechaPartidoAt($fechaPartidoAt)
    {
        $this->fechaPartidoAt = $fechaPartidoAt;

        return $this;
    }

    /**
     * Get fechaPartidoAt
     *
     * @return \DateTime
     */
    public function getFechaPartidoAt()
    {
        return $this->fechaPartidoAt;
    }

    /**
     * Set local
     *
     * @param \futfunBundle\Entity\Clubes $local
     * @return PartidoFecha
     */
    public function setLocal(\futfunBundle\Entity\Clubes $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set visita
     *
     * @param \futfunBundle\Entity\Clubes $visita
     * @return PartidoFecha
     */
    public function setVisita(\futfunBundle\Entity\Clubes $visita = null)
    {
        $this->visita = $visita;

        return $this;
    }

    /**
     * Get visita
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getVisita()
    {
        return $this->visita;
    }

    /**
     * Set grupoFecha
     *
     * @param \futfunBundle\Entity\GrupoFecha $grupoFecha
     * @return PartidoFecha
     */
    public function setGrupoFecha(\futfunBundle\Entity\GrupoFecha $grupoFecha = null)
    {
        $this->grupoFecha = $grupoFecha;

        return $this;
    }

    /**
     * Get grupoFecha
     *
     * @return \futfunBundle\Entity\GrupoFecha
     */
    public function getGrupoFecha()
    {
        return $this->grupoFecha;
    }

    /**
     * Set valorDefinicionLocal
     *
     * @param integer $valorDefinicionLocal
     * @return PartidoFecha
     */
    public function setValorDefinicionLocal($valorDefinicionLocal)
    {
        $this->valorDefinicionLocal = $valorDefinicionLocal;

        return $this;
    }

    /**
     * Get valorDefinicionLocal
     *
     * @return integer
     */
    public function getValorDefinicionLocal()
    {
        return $this->valorDefinicionLocal;
    }

    /**
     * Set valorDefinicionVisita
     *
     * @param integer $valorDefinicionVisita
     * @return PartidoFecha
     */
    public function setValorDefinicionVisita($valorDefinicionVisita)
    {
        $this->valorDefinicionVisita = $valorDefinicionVisita;

        return $this;
    }

    /**
     * Get valorDefinicionVisita
     *
     * @return integer
     */
    public function getValorDefinicionVisita()
    {
        return $this->valorDefinicionVisita;
    }
}
