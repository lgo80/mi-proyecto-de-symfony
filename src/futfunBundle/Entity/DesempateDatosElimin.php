<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesempateDatosElimin
 */
class DesempateDatosElimin
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $numeroFecha;

    /**
     * @var integer
     */
    private $numeroPartido;

    /**
     * @var string
     */
    private $tipoLocal;

    /**
     * @var integer
     */
    private $numPartidoLocal;

    /**
     * @var string
     */
    private $tipoVisita;

    /**
     * @var integer
     */
    private $numPartidoVisita;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroFecha
     *
     * @param integer $numeroFecha
     * @return DesempateDatosElimin
     */
    public function setNumeroFecha($numeroFecha)
    {
        $this->numeroFecha = $numeroFecha;

        return $this;
    }

    /**
     * Get numeroFecha
     *
     * @return integer 
     */
    public function getNumeroFecha()
    {
        return $this->numeroFecha;
    }

    /**
     * Set numeroPartido
     *
     * @param integer $numeroPartido
     * @return DesempateDatosElimin
     */
    public function setNumeroPartido($numeroPartido)
    {
        $this->numeroPartido = $numeroPartido;

        return $this;
    }

    /**
     * Get numeroPartido
     *
     * @return integer 
     */
    public function getNumeroPartido()
    {
        return $this->numeroPartido;
    }

    /**
     * Set tipoLocal
     *
     * @param string $tipoLocal
     * @return DesempateDatosElimin
     */
    public function setTipoLocal($tipoLocal)
    {
        $this->tipoLocal = $tipoLocal;

        return $this;
    }

    /**
     * Get tipoLocal
     *
     * @return string 
     */
    public function getTipoLocal()
    {
        return $this->tipoLocal;
    }

    /**
     * Set numPartidoLocal
     *
     * @param integer $numPartidoLocal
     * @return DesempateDatosElimin
     */
    public function setNumPartidoLocal($numPartidoLocal)
    {
        $this->numPartidoLocal = $numPartidoLocal;

        return $this;
    }

    /**
     * Get numPartidoLocal
     *
     * @return integer 
     */
    public function getNumPartidoLocal()
    {
        return $this->numPartidoLocal;
    }

    /**
     * Set tipoVisita
     *
     * @param string $tipoVisita
     * @return DesempateDatosElimin
     */
    public function setTipoVisita($tipoVisita)
    {
        $this->tipoVisita = $tipoVisita;

        return $this;
    }

    /**
     * Get tipoVisita
     *
     * @return string 
     */
    public function getTipoVisita()
    {
        return $this->tipoVisita;
    }

    /**
     * Set numPartidoVisita
     *
     * @param integer $numPartidoVisita
     * @return DesempateDatosElimin
     */
    public function setNumPartidoVisita($numPartidoVisita)
    {
        $this->numPartidoVisita = $numPartidoVisita;

        return $this;
    }

    /**
     * Get numPartidoVisita
     *
     * @return integer 
     */
    public function getNumPartidoVisita()
    {
        return $this->numPartidoVisita;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return DesempateDatosElimin
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }
}
