<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesempateDatos
 */
class DesempateDatos
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $cantidadEquipos;

    /**
     * @var boolean
     */
    private $golVisitante;

    /**
     * @var string
     */
    private $forma;

    /**
     * @var string
     */
    private $vueltaGrupo;

    /**
     * @var boolean
     */
    private $isConfirm;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;

    /**
     * @var \futfunBundle\Entity\TipoTorneo
     */
    private $tipoTorneo;

    public function __construct($torneo, $cantidad)
    {
        $this->torneo = $torneo;
        $this->cantidadEquipos = $cantidad;
        $this->golVisitante = false;
        $this->forma = null;
        $this->isConfirm = false;
        $this->tipoTorneo = null;
        $this->vueltaGrupo = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidadEquipos
     *
     * @param integer $cantidadEquipos
     * @return DesempateDatos
     */
    public function setCantidadEquipos($cantidadEquipos)
    {
        $this->cantidadEquipos = $cantidadEquipos;

        return $this;
    }

    /**
     * Get cantidadEquipos
     *
     * @return integer
     */
    public function getCantidadEquipos()
    {
        return $this->cantidadEquipos;
    }

    /**
     * Set golVisitante
     *
     * @param boolean $golVisitante
     * @return DesempateDatos
     */
    public function setGolVisitante($golVisitante)
    {
        $this->golVisitante = $golVisitante;

        return $this;
    }

    /**
     * Get golVisitante
     *
     * @return boolean
     */
    public function getGolVisitante()
    {
        return $this->golVisitante;
    }

    /**
     * Set forma
     *
     * @param string $forma
     * @return DesempateDatos
     */
    public function setForma($forma)
    {
        $this->forma = $forma;

        return $this;
    }

    /**
     * Get forma
     *
     * @return string
     */
    public function getForma()
    {
        return $this->forma;
    }

    /**
     * Set vueltaGrupo
     *
     * @param string $vueltaGrupo
     * @return DesempateDatos
     */
    public function setVueltaGrupo($vueltaGrupo)
    {
        $this->vueltaGrupo = $vueltaGrupo;

        return $this;
    }

    /**
     * Get vueltaGrupo
     *
     * @return string
     */
    public function getVueltaGrupo()
    {
        return $this->vueltaGrupo;
    }

    /**
     * Set isConfirm
     *
     * @param boolean $isConfirm
     * @return DesempateDatos
     */
    public function setIsConfirm($isConfirm)
    {
        $this->isConfirm = $isConfirm;

        return $this;
    }

    /**
     * Get isConfirm
     *
     * @return boolean
     */
    public function getIsConfirm()
    {
        return $this->isConfirm;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return DesempateDatos
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }

    /**
     * Set tipoTorneo
     *
     * @param \futfunBundle\Entity\TipoTorneo $tipoTorneo
     * @return DesempateDatos
     */
    public function setTipoTorneo(\futfunBundle\Entity\TipoTorneo $tipoTorneo = null)
    {
        $this->tipoTorneo = $tipoTorneo;

        return $this;
    }

    /**
     * Get tipoTorneo
     *
     * @return \futfunBundle\Entity\TipoTorneo
     */
    public function getTipoTorneo()
    {
        return $this->tipoTorneo;
    }
}
