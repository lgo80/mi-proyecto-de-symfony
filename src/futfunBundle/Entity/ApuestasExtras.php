<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApuestasExtras
 */
class ApuestasExtras
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $detalle;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var boolean
     */
    private $isComplete;

    /**
     * @var integer
     */
    private $valor;

    /**
     * @var string
     */
    private $valorDetalle;

    /**
     * @var \futfunBundle\Entity\ApuestaBase
     */
    private $base;

    public function __construct()
    {
        $this->valorDetalle = "";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return ApuestasExtras
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ApuestasExtras
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set isComplete
     *
     * @param boolean $isComplete
     * @return ApuestasExtras
     */
    public function setIsComplete($isComplete)
    {
        $this->isComplete = $isComplete;

        return $this;
    }

    /**
     * Get isComplete
     *
     * @return boolean
     */
    public function getIsComplete()
    {
        return $this->isComplete;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     * @return ApuestasExtras
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return integer
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set valorDetalle
     *
     * @param string $valorDetalle
     * @return ApuestasExtras
     */
    public function setValorDetalle($valorDetalle)
    {
        $this->valorDetalle = $valorDetalle;

        return $this;
    }

    /**
     * Get valorDetalle
     *
     * @return string
     */
    public function getValorDetalle()
    {
        return $this->valorDetalle;
    }

    /**
     * Set base
     *
     * @param \futfunBundle\Entity\ApuestaBase $base
     * @return ApuestasExtras
     */
    public function setBase(\futfunBundle\Entity\ApuestaBase $base = null)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return \futfunBundle\Entity\ApuestaBase
     */
    public function getBase()
    {
        return $this->base;
    }
}
