<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * ApuestaBase
 */
class ApuestaBase
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $fechaLimite;

    /**
     * @var integer
     */
    private $puntosPartExactos;

    /**
     * @var integer
     */
    private $puntosPartResultados;

    /**
     * @var integer
     */
    private $puntosPartErrados;

    /**
     * @var float
     */
    private $apuestaSimbolica;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var \DateTime
     */
    private $fechaCreacionAt;

    /**
     * @var \DateTime
     */
    private $fechaLimiteExtraAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $extras;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $administradores;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $campeon;

    public function __construct()
    {
        $this->isActive = true;
        $this->extras = new ArrayCollection();
        $this->estado = $this->devolverEstado(1);
        $this->apuestaSimbolica = 0;
        $this->campeon = null;
        $this->fechaCreacionAt = new \DateTime();
        $this->fechaLimite = "partido";
    }

    public function devolverEstado($opcion)
    {

        switch ($opcion) {

            case 1:
                return "En espera";

            case 2:
                return "Comenzado";

            case 3:
                return "Finalizado";

            default:
                return "";
        }

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ApuestaBase
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaLimite
     *
     * @param string $fechaLimite
     * @return ApuestaBase
     */
    public function setFechaLimite($fechaLimite)
    {
        $this->fechaLimite = $fechaLimite;

        return $this;
    }

    /**
     * Get fechaLimite
     *
     * @return string
     */
    public function getFechaLimite()
    {
        return $this->fechaLimite;
    }

    /**
     * Set puntosPartExactos
     *
     * @param integer $puntosPartExactos
     * @return ApuestaBase
     */
    public function setPuntosPartExactos($puntosPartExactos)
    {
        $this->puntosPartExactos = $puntosPartExactos;

        return $this;
    }

    /**
     * Get puntosPartExactos
     *
     * @return integer
     */
    public function getPuntosPartExactos()
    {
        return $this->puntosPartExactos;
    }

    /**
     * Set puntosPartResultados
     *
     * @param integer $puntosPartResultados
     * @return ApuestaBase
     */
    public function setPuntosPartResultados($puntosPartResultados)
    {
        $this->puntosPartResultados = $puntosPartResultados;

        return $this;
    }

    /**
     * Get puntosPartResultados
     *
     * @return integer
     */
    public function getPuntosPartResultados()
    {
        return $this->puntosPartResultados;
    }

    /**
     * Set puntosPartErrados
     *
     * @param integer $puntosPartErrados
     * @return ApuestaBase
     */
    public function setPuntosPartErrados($puntosPartErrados)
    {
        $this->puntosPartErrados = $puntosPartErrados;

        return $this;
    }

    /**
     * Get puntosPartErrados
     *
     * @return integer
     */
    public function getPuntosPartErrados()
    {
        return $this->puntosPartErrados;
    }

    /**
     * Set apuestaSimbolica
     *
     * @param float $apuestaSimbolica
     * @return ApuestaBase
     */
    public function setApuestaSimbolica($apuestaSimbolica)
    {
        $this->apuestaSimbolica = $apuestaSimbolica;

        return $this;
    }

    /**
     * Get apuestaSimbolica
     *
     * @return float
     */
    public function getApuestaSimbolica()
    {
        return $this->apuestaSimbolica;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return ApuestaBase
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ApuestaBase
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaCreacionAt
     *
     * @param \DateTime $fechaCreacionAt
     * @return ApuestaBase
     */
    public function setFechaCreacionAt($fechaCreacionAt)
    {
        $this->fechaCreacionAt = $fechaCreacionAt;

        return $this;
    }

    /**
     * Get fechaCreacionAt
     *
     * @return \DateTime
     */
    public function getFechaCreacionAt()
    {
        return $this->fechaCreacionAt;
    }

    /**
     * Set fechaLimiteExtraAt
     *
     * @param \DateTime $fechaLimiteExtraAt
     * @return ApuestaBase
     */
    public function setFechaLimiteExtraAt($fechaLimiteExtraAt)
    {
        $this->fechaLimiteExtraAt = $fechaLimiteExtraAt;

        return $this;
    }

    /**
     * Get fechaLimiteExtraAt
     *
     * @return \DateTime
     */
    public function getFechaLimiteExtraAt()
    {
        return $this->fechaLimiteExtraAt;
    }

    /**
     * Add extras
     *
     * @param \futfunBundle\Entity\ApuestasExtras $extras
     * @return ApuestaBase
     */
    public function addExtra(\futfunBundle\Entity\ApuestasExtras $extras)
    {
        $this->extras[] = $extras;

        return $this;
    }

    /**
     * Remove extras
     *
     * @param \futfunBundle\Entity\ApuestasExtras $extras
     */
    public function removeExtra(\futfunBundle\Entity\ApuestasExtras $extras)
    {
        $this->extras->removeElement($extras);
    }

    /**
     * Get extras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExtras()
    {
        return $this->extras;
    }

    /**
     * Add administradores
     *
     * @param \futfunBundle\Entity\AdministradoresApuestas $administradores
     * @return ApuestaBase
     */
    public function addAdministradore(\futfunBundle\Entity\AdministradoresApuestas $administradores)
    {
        $this->administradores[] = $administradores;

        return $this;
    }

    /**
     * Remove administradores
     *
     * @param \futfunBundle\Entity\AdministradoresApuestas $administradores
     */
    public function removeAdministradore(\futfunBundle\Entity\AdministradoresApuestas $administradores)
    {
        $this->administradores->removeElement($administradores);
    }

    /**
     * Get administradores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdministradores()
    {
        return $this->administradores;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return ApuestaBase
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }

    /**
     * Set campeon
     *
     * @param \futfunBundle\Entity\User $campeon
     * @return ApuestaBase
     */
    public function setCampeon(\futfunBundle\Entity\User $campeon = null)
    {
        $this->campeon = $campeon;

        return $this;
    }

    /**
     * Get campeon
     *
     * @return \futfunBundle\Entity\User
     */
    public function getCampeon()
    {
        return $this->campeon;
    }
}
