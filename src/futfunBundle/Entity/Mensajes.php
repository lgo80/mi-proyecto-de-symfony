<?php

namespace futfunBundle\Entity;

/**
 * Mensajes
 */
class Mensajes
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titulo;

    /**
     * @var \DateTime
     */
    private $fechaAt;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var boolean
     */
    private $isLeido;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $emisor;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $receptor;

    public function __construct($emisor)
    {
        $this->fechaAt = new \DateTime();
        $this->isActive = true;
        $this->isLeido = false;
        $this->emisor = $emisor;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Mensajes
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set fechaAt
     *
     * @param \DateTime $fechaAt
     *
     * @return Mensajes
     */
    public function setFechaAt($fechaAt)
    {
        $this->fechaAt = $fechaAt;

        return $this;
    }

    /**
     * Get fechaAt
     *
     * @return \DateTime
     */
    public function getFechaAt()
    {
        return $this->fechaAt;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Mensajes
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set isLeido
     *
     * @param boolean $isLeido
     *
     * @return Mensajes
     */
    public function setIsLeido($isLeido)
    {
        $this->isLeido = $isLeido;

        return $this;
    }

    /**
     * Get isLeido
     *
     * @return boolean
     */
    public function getIsLeido()
    {
        return $this->isLeido;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Mensajes
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set emisor
     *
     * @param \futfunBundle\Entity\User $emisor
     *
     * @return Mensajes
     */
    public function setEmisor(\futfunBundle\Entity\User $emisor = null)
    {
        $this->emisor = $emisor;

        return $this;
    }

    /**
     * Get emisor
     *
     * @return \futfunBundle\Entity\User
     */
    public function getEmisor()
    {
        return $this->emisor;
    }

    /**
     * Set receptor
     *
     * @param \futfunBundle\Entity\User $receptor
     *
     * @return Mensajes
     */
    public function setReceptor(\futfunBundle\Entity\User $receptor = null)
    {
        $this->receptor = $receptor;

        return $this;
    }

    /**
     * Get receptor
     *
     * @return \futfunBundle\Entity\User
     */
    public function getReceptor()
    {
        return $this->receptor;
    }
}
