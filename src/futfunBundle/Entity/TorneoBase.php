<?php

namespace futfunBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * torneoBase
 */
class TorneoBase
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var boolean
     */
    private $isActivo;

    /**
     * @var string
     */
    private $clase;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var \DateTime
     */
    private $fechaCreacionAt;

    /**
     * @var \futfunBundle\Entity\DatosTorneo
     */
    private $dato;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $administradores;

    /**
     * @var \futfunBundle\Entity\TipoTorneo
     */
    private $tipoTorneo;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $campeon;

    public function __construct()
    {
        $this->isActivo = true;
        $this->estado = $this->getElEstado(2);
        $this->administradores = new ArrayCollection();
        $this->campeon = null;
        $this->fechaCreacionAt = new \DateTime();
        $this->dato = new DatosTorneo();
    }

    public function getElEstado($opcion)
    {

        switch ($opcion) {

            case 1:
                return "En construcción";

            case 2:
                return "En progreso";

            case 3:
                return "Finalizado";

            default:
                return "";
        }

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TorneoBase
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set isActivo
     *
     * @param boolean $isActivo
     * @return TorneoBase
     */
    public function setIsActivo($isActivo)
    {
        $this->isActivo = $isActivo;

        return $this;
    }

    /**
     * Get isActivo
     *
     * @return boolean
     */
    public function getIsActivo()
    {
        return $this->isActivo;
    }

    /**
     * Set clase
     *
     * @param string $clase
     * @return TorneoBase
     */
    public function setClase($clase)
    {
        $this->clase = $clase;

        return $this;
    }

    /**
     * Get clase
     *
     * @return string
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TorneoBase
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaCreacionAt
     *
     * @param \DateTime $fechaCreacionAt
     * @return TorneoBase
     */
    public function setFechaCreacionAt($fechaCreacionAt)
    {
        $this->fechaCreacionAt = $fechaCreacionAt;

        return $this;
    }

    /**
     * Get fechaCreacionAt
     *
     * @return \DateTime
     */
    public function getFechaCreacionAt()
    {
        return $this->fechaCreacionAt;
    }

    /**
     * Set dato
     *
     * @param \futfunBundle\Entity\DatosTorneo $dato
     * @return TorneoBase
     */
    public function setDato(\futfunBundle\Entity\DatosTorneo $dato = null)
    {
        $this->dato = $dato;

        return $this;
    }

    /**
     * Get dato
     *
     * @return \futfunBundle\Entity\DatosTorneo
     */
    public function getDato()
    {
        return $this->dato;
    }

    /**
     * Add administradores
     *
     * @param \futfunBundle\Entity\AdministradoresTorneos $administradores
     * @return TorneoBase
     */
    public function addAdministradore(\futfunBundle\Entity\AdministradoresTorneos $administradores)
    {
        $this->administradores[] = $administradores;

        return $this;
    }

    /**
     * Remove administradores
     *
     * @param \futfunBundle\Entity\AdministradoresTorneos $administradores
     */
    public function removeAdministradore(\futfunBundle\Entity\AdministradoresTorneos $administradores)
    {
        $this->administradores->removeElement($administradores);
    }

    /**
     * Get administradores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdministradores()
    {
        return $this->administradores;
    }

    /**
     * Set tipoTorneo
     *
     * @param \futfunBundle\Entity\TipoTorneo $tipoTorneo
     * @return TorneoBase
     */
    public function setTipoTorneo(\futfunBundle\Entity\TipoTorneo $tipoTorneo = null)
    {
        $this->tipoTorneo = $tipoTorneo;

        return $this;
    }

    /**
     * Get tipoTorneo
     *
     * @return \futfunBundle\Entity\TipoTorneo
     */
    public function getTipoTorneo()
    {
        return $this->tipoTorneo;
    }

    /**
     * Set campeon
     *
     * @param \futfunBundle\Entity\Clubes $campeon
     * @return TorneoBase
     */
    public function setCampeon(\futfunBundle\Entity\Clubes $campeon = null)
    {
        $this->campeon = $campeon;

        return $this;
    }

    /**
     * Get campeon
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getCampeon()
    {
        return $this->campeon;
    }
}
