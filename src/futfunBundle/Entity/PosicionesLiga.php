<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PosicionesLiga
 */
class PosicionesLiga
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $puntos;

    /**
     * @var integer
     */
    private $gf;

    /**
     * @var integer
     */
    private $gc;

    /**
     * @var integer
     */
    private $pg;

    /**
     * @var integer
     */
    private $pe;

    /**
     * @var integer
     */
    private $pp;

    /**
     * @var integer
     */
    private $dg;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $club;

    /**
     * @var \futfunBundle\Entity\Grupo
     */
    private $grupo;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;

    public function __construct()
    {
        $this->puntos = 0;
        $this->pg = 0;
        $this->pe = 0;
        $this->pp = 0;
        $this->gf = 0;
        $this->gc = 0;
        $this->dg = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set puntos
     *
     * @param integer $puntos
     * @return PosicionesLiga
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos
     *
     * @return integer
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set gf
     *
     * @param integer $gf
     * @return PosicionesLiga
     */
    public function setGf($gf)
    {
        $this->gf = $gf;

        return $this;
    }

    /**
     * Get gf
     *
     * @return integer
     */
    public function getGf()
    {
        return $this->gf;
    }

    /**
     * Set gc
     *
     * @param integer $gc
     * @return PosicionesLiga
     */
    public function setGc($gc)
    {
        $this->gc = $gc;

        return $this;
    }

    /**
     * Get gc
     *
     * @return integer
     */
    public function getGc()
    {
        return $this->gc;
    }

    /**
     * Set pg
     *
     * @param integer $pg
     * @return PosicionesLiga
     */
    public function setPg($pg)
    {
        $this->pg = $pg;

        return $this;
    }

    /**
     * Get pg
     *
     * @return integer
     */
    public function getPg()
    {
        return $this->pg;
    }

    /**
     * Set pe
     *
     * @param integer $pe
     * @return PosicionesLiga
     */
    public function setPe($pe)
    {
        $this->pe = $pe;

        return $this;
    }

    /**
     * Get pe
     *
     * @return integer
     */
    public function getPe()
    {
        return $this->pe;
    }

    /**
     * Set pp
     *
     * @param integer $pp
     * @return PosicionesLiga
     */
    public function setPp($pp)
    {
        $this->pp = $pp;

        return $this;
    }

    /**
     * Get pp
     *
     * @return integer
     */
    public function getPp()
    {
        return $this->pp;
    }

    /**
     * Set dg
     *
     * @param integer $dg
     * @return PosicionesLiga
     */
    public function setDg($dg)
    {
        $this->dg = $dg;

        return $this;
    }

    /**
     * Get dg
     *
     * @return integer
     */
    public function getDg()
    {
        return $this->dg;
    }

    /**
     * Set club
     *
     * @param \futfunBundle\Entity\Clubes $club
     * @return PosicionesLiga
     */
    public function setClub(\futfunBundle\Entity\Clubes $club = null)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get club
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Set grupo
     *
     * @param \futfunBundle\Entity\Grupo $grupo
     * @return PosicionesLiga
     */
    public function setGrupo(\futfunBundle\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \futfunBundle\Entity\Grupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return PosicionesLiga
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }
}
