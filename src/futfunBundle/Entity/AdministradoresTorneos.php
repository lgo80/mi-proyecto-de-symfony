<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdministradoresTorneos
 */
class AdministradoresTorneos
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \futfunBundle\Entity\Roles
     */
    private $role;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $user;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param \futfunBundle\Entity\Roles $role
     * @return AdministradoresTorneos
     */
    public function setRole(\futfunBundle\Entity\Roles $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \futfunBundle\Entity\Roles
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set user
     *
     * @param \futfunBundle\Entity\User $user
     * @return AdministradoresTorneos
     */
    public function setUser(\futfunBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \futfunBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return AdministradoresTorneos
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }
}
