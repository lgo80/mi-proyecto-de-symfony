<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApuestaExtraUsuario
 */
class ApuestaExtraUsuario
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $detalle;

    /**
     * @var boolean
     */
    private $isCorrect;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var \futfunBundle\Entity\User
     */
    private $user;

    /**
     * @var \futfunBundle\Entity\ApuestasExtras
     */
    private $extra;

    public function __construct()
    {
        $this->isActive = true;
        $this->isCorrect = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return ApuestaExtraUsuario
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set isCorrect
     *
     * @param boolean $isCorrect
     * @return ApuestaExtraUsuario
     */
    public function setIsCorrect($isCorrect)
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    /**
     * Get isCorrect
     *
     * @return boolean 
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return ApuestaExtraUsuario
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set user
     *
     * @param \futfunBundle\Entity\User $user
     * @return ApuestaExtraUsuario
     */
    public function setUser(\futfunBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \futfunBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set extra
     *
     * @param \futfunBundle\Entity\ApuestasExtras $extra
     * @return ApuestaExtraUsuario
     */
    public function setExtra(\futfunBundle\Entity\ApuestasExtras $extra = null)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return \futfunBundle\Entity\ApuestasExtras
     */
    public function getExtra()
    {
        return $this->extra;
    }

}
