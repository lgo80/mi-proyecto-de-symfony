<?php

namespace futfunBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesempateFecha
 */
class DesempateFecha
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $numeroFecha;

    /**
     * @var string
     */
    private $nombreFecha;

    /**
     * @var \DateTime
     */
    private $fechaInicioAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $desempatePartidos;

    /**
     * @var \futfunBundle\Entity\TorneoBase
     */
    private $torneo;

    /**
     * @var \futfunBundle\Entity\Clubes
     */
    private $equipoLibre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->desempatePartidos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equipoLibre = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroFecha
     *
     * @param integer $numeroFecha
     * @return DesempateFecha
     */
    public function setNumeroFecha($numeroFecha)
    {
        $this->numeroFecha = $numeroFecha;

        return $this;
    }

    /**
     * Get numeroFecha
     *
     * @return integer
     */
    public function getNumeroFecha()
    {
        return $this->numeroFecha;
    }

    /**
     * Set nombreFecha
     *
     * @param string $nombreFecha
     * @return DesempateFecha
     */
    public function setNombreFecha($nombreFecha)
    {
        $this->nombreFecha = $nombreFecha;

        return $this;
    }

    /**
     * Get nombreFecha
     *
     * @return string
     */
    public function getNombreFecha()
    {
        return $this->nombreFecha;
    }

    /**
     * Set fechaInicioAt
     *
     * @param \DateTime $fechaInicioAt
     * @return DesempateFecha
     */
    public function setFechaInicioAt($fechaInicioAt)
    {
        $this->fechaInicioAt = $fechaInicioAt;

        return $this;
    }

    /**
     * Get fechaInicioAt
     *
     * @return \DateTime
     */
    public function getFechaInicioAt()
    {
        return $this->fechaInicioAt;
    }

    /**
     * Add desempatePartidos
     *
     * @param \futfunBundle\Entity\DesempatePartido $desempatePartidos
     * @return DesempateFecha
     */
    public function addDesempatePartido(\futfunBundle\Entity\DesempatePartido $desempatePartidos)
    {
        $this->desempatePartidos[] = $desempatePartidos;

        return $this;
    }

    /**
     * Remove desempatePartidos
     *
     * @param \futfunBundle\Entity\DesempatePartido $desempatePartidos
     */
    public function removeDesempatePartido(\futfunBundle\Entity\DesempatePartido $desempatePartidos)
    {
        $this->desempatePartidos->removeElement($desempatePartidos);
    }

    /**
     * Get desempatePartidos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesempatePartidos()
    {
        return $this->desempatePartidos;
    }

    /**
     * Set torneo
     *
     * @param \futfunBundle\Entity\TorneoBase $torneo
     * @return DesempateFecha
     */
    public function setTorneo(\futfunBundle\Entity\TorneoBase $torneo = null)
    {
        $this->torneo = $torneo;

        return $this;
    }

    /**
     * Get torneo
     *
     * @return \futfunBundle\Entity\TorneoBase
     */
    public function getTorneo()
    {
        return $this->torneo;
    }

    /**
     * Set equipoLibre
     *
     * @param \futfunBundle\Entity\Clubes $equipoLibre
     * @return DesempateFecha
     */
    public function setEquipoLibre(\futfunBundle\Entity\Clubes $equipoLibre = null)
    {
        $this->equipoLibre = $equipoLibre;

        return $this;
    }

    /**
     * Get equipoLibre
     *
     * @return \futfunBundle\Entity\Clubes
     */
    public function getEquipoLibre()
    {
        return $this->equipoLibre;
    }
}
