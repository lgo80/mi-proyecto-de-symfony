<?php

namespace futfunBundle\Form;

use futfunBundle\Entity\ApuestaExtraUsuario;

class ApuestaExtraPartEspecialType
{

    /**
     * Esta funcion se especifica en armar el formulario para la vista de completar los extra del torneo de
     * apuesta particular de cada usuario
     * @param $datos Es un array con datos necesarios para crear el formulario
     * @param $em
     * @param $controler
     * @param $user Es la entidad User quien completo los extras
     * @return mixed El formulario formateado con los datos segun los parametros ingresados
     */
    public function crearGrupo($datos, $em, $controler, $userLogueado)
    {

        $extrasApuesta = $datos["extrasApuesta"];
        $usuarioEntrante = $datos["usuarioEntrante"];
        $isFechaCumplida = $datos["isFechaCumplida"];

        if (($userLogueado->getId() == $usuarioEntrante->getId())
            or ($userLogueado->getId() != $usuarioEntrante->getId()
                and $isFechaCumplida)
        ) {

            $defaultData = $this->devolverDatosDelForm(
                $extrasApuesta, $usuarioEntrante, $em);
            $builder = $controler->createFormBuilder(
                $defaultData);

            $i = 0;

            foreach ($extrasApuesta as &$extra) {

                $i++;

                $builder
                    ->add('extra' . $i, new ApuestaExtraUsuarioType());


            }

        } else {

            $builder = $controler->createFormBuilder();

        }

        return $builder;

    }

    /**
     * Esta funcion se especifica en procesar los datos de los extras para llenar el builder del formulario
     * @param $extrasApuesta Es un array con los extras elegidos por el usuario
     * @param $user La entidad user que eligio los extras
     * @param $em
     * @return mixed devuelve un array con los datos necesarios
     */
    public function devolverDatosDelForm($extrasApuesta, $user, $em)
    {

        $i = 0;

        foreach ($extrasApuesta as &$extra) {

            $i++;

            $apuestaExtraUsu = $em->getRepository('futfunBundle:ApuestaExtraUsuario')
                ->devolverExtrasApuestaUsu($user, $extra);

            if ($apuestaExtraUsu == null) {

                $apuestaExtraUsu = new ApuestaExtraUsuario();

                $apuestaExtraUsu->setExtra($extra);

                $apuestaExtraUsu->setUser($user);

            }

            $respuesta['extra' . $i] = $apuestaExtraUsu;


        }

        return $respuesta;

    }

}
