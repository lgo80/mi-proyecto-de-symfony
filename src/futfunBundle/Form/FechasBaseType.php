<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class FechasBaseType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreFecha', 'text', array(
                'label' => 'Date:',
                'constraints' => array(
                    new NotBlank(array('message' => "El nombre de la fecha no puede ser nulo")),
                    new Length(array(
                        'min' => 1, 'max' => 25,
                        'minMessage' => "El nombre tiene que tener mas de 1 caracteres",
                        'maxMessage' => "El nombre tiene que tener menos de 25 caracteres"))
                )));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\FechasBase'
        ));
    }
}
