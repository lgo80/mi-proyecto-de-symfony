<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DatosTorneoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('puntosPartGanados', IntegerType::class, array(
                'label' => 'Partido ganado:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 100,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 1!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 100!"
                        ))
                )
            ))
            ->add('puntosPartEmpatados', IntegerType::class, array(
                'label' => 'Partido empatado:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 0,
                            'max' => 100,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 0!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 100!"
                        ))
                )
            ))
            ->add('puntosPartPerdidos', IntegerType::class, array(
                'label' => 'Partido perdido:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 0,
                            'max' => 100,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 0!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 100!"
                        ))
                )
            ))
            ->add('cantEquipos', IntegerType::class, array(
                'label' => 'Amount of equipment:',
                'constraints' => array(
                    new NotBlank(
                        array('message' => "�Debe ingresar la cantidad de equipos!")
                    ),
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 1000,
                            'minMessage' => "�La cantidad de equipos tiene que ser un numero entero mayor a 1!",
                            'maxMessage' => "�La cantidad de equipos tiene que ser un numero entero menor a 1000!"
                        ))
                )
            ))
            ->add('partidoDesempate', ChoiceType::class, array(
                'choices' => array(
                    'Yes' => true,
                    'No' => false,
                ),
                'multiple' => false,
                'expanded' => true,
                'label' => 'Tie breaker:',
                'choices_as_values' => true,
                'data' => false
            ))
            ->add('cantGrupos', IntegerType::class, array(
                'label' => 'Amount of groups:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 150,
                            'minMessage' => "�La cantidad de equipos tiene que ser un numero entero mayor a 1!",
                            'maxMessage' => "�La cantidad de equipos tiene que ser un numero entero menor a 150!"
                        ))
                )
            ))
            ->add('equipGrupos', IntegerType::class, array(
                'label' => 'Teams by group:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 150,
                            'minMessage' => "�La cantidad de equipos tiene que ser un numero entero mayor a 1!",
                            'maxMessage' => "�La cantidad de equipos tiene que ser un numero entero menor a 150!"
                        ))
                )
            ))
            ->add('formaGrupo', ChoiceType::class, [
                'choices' => array(
                    'soloida' => "One way",
                    'idayvuelta' => "Round trip",),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "League form:"
            ])
            ->add('vueltaGrupo', ChoiceType::class, [
                'choices' => array(
                    'igualIda' => "Same as the one going to invest the localias",
                    'InversaIda' => "Reverse one-way investment",
                    'pordefinir' => "Defining manually",),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Return form:"
            ])
            ->add('tipoClasificacion', ChoiceType::class, [
                'choices' => array(
                    'porGrupo' => "Per group",
                    'porPuntos' => "For global points",
                    'pordefinir' => "Defining manually",),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Classification form:"
            ])
            ->add('clasPorGrupo', IntegerType::class, array(
                'label' => 'Classified by groups:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 150,
                            'minMessage' => "�La cantidad de equipos tiene que ser un numero entero mayor a 1!",
                            'maxMessage' => "�La cantidad de equipos tiene que ser un numero entero menor a 150!"
                        ))
                )
            ))
            ->add('clasAdic', IntegerType::class, array(
                'label' => 'Additional classifieds:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 150,
                            'minMessage' => "�La cantidad de equipos tiene que ser un numero entero mayor a 1!",
                            'maxMessage' => "�La cantidad de equipos tiene que ser un numero entero menor a 150!"
                        ))
                )
            ))
            ->add('fechasAdic', IntegerType::class, array(
                'label' => 'Additional Dates:',
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 150,
                            'minMessage' => "�La cantidad de equipos tiene que ser un numero entero mayor a 1!",
                            'maxMessage' => "�La cantidad de equipos tiene que ser un numero entero menor a 150!"
                        ))
                )
            ))
            ->add('comienzoElim', ChoiceType::class, [
                'choices' => array(
                    '_128avos' => "128avos de final",
                    '_64avos' => "64avos de final",
                    '_32avos' => "32avos de final",
                    '_16avos' => "16avos de final",
                    '_8avos' => "Octavos de final",
                    '_4avos' => "Cuartos de final",
                    '_2avos' => "Semifinal",
                    '_1avos' => "Final",),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Beginning of elimination:"
            ])
            ->add('formaElim', ChoiceType::class, [
                'choices' => array(
                    'soloida' => "One way",
                    'idayvuelta' => "Round trip"),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Form of elimination:"
            ])
            ->add('partTercerPuesto', ChoiceType::class, array(
                'choices' => array(
                    'Yes' => true,
                    'No' => false,
                ),
                'multiple' => false,
                'expanded' => true,
                'label' => 'Third place match:',
                'choices_as_values' => true,
                'data' => false
            ))
            ->add('golVisitante', ChoiceType::class, [
                'choices' => array(
                    'no' => 'There is not',
                    'si' => "There are in all rounds",
                    'finalno' => "There are in all rounds except in the final"),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Visitors goal:"
            ])
            ->add('formaFinal', ChoiceType::class, [
                'choices' => array(
                    'soloida' => "One way",
                    'idayvuelta' => "Round trip"),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Form in the final:"
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\DatosTorneo'
        ));
    }


}
