<?php

namespace futfunBundle\Form;

use futfunBundle\Entity\ApuestaExtraUsuario;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class ApuestaExtraPartEspecial2Type
{

    /**
     * Esta funcion se especifica en armar el formulario para la vista de completar los extra del torneo de
     * apuesta particular de cada usuario
     * @param $datos Es un array con datos necesarios para crear el formulario
     * @param $em
     * @param $controler
     * @param $user Es la entidad User quien completo los extras
     * @return mixed El formulario formateado con los datos segun los parametros ingresados
     */
    public function crearGrupo($datos, $controler)
    {

        $arrayExtrasUsuario = $datos["apuestaExtraUsuarios"];
        $arrayExtras = $datos["extrasApuesta"];

        $defaultData = $this->devolverDatosDelForm(
            $arrayExtras, $arrayExtrasUsuario);
        $builder = $controler->createFormBuilder($defaultData);

        for ($i = 1; $i <= count($arrayExtras); $i++) {

            if ($arrayExtras[$i - 1]->getIsComplete()) {
                $builder
                    ->add('valorDetalle' . $i, "text", array(
                        'constraints' => array(
                            new NotBlank(array('message' => "El nombre del extra real no puede ser nulo")),
                            new Length(array(
                                'min' => 4, 'max' => 100,
                                'minMessage' => "El nombre del extra tiene que tener mas de 4 caracteres",
                                'maxMessage' => "El nombre del extra tiene que tener menos de 100 caracteres"))
                        )
                    ));
            } else {

                $builder
                    ->add('valorDetalle' . $i, "text");

            }


            for ($j = 1; $j <= count($arrayExtrasUsuario); $j++) {

                $builder
                    ->add('extraUsu' . $i . $j, CheckboxType::class, array(
                        'required' => false
                    ));

            }

        }
        $builder
            ->add('save', "submit", array(
                'label' => 'Save'))
            ->add('saveAndConfirm', "submit", array(
                'label' => 'Save and confirm'));

        return $builder;

    }

    /**
     * Esta funcion se especifica en procesar los datos de los extras para llenar el builder del formulario
     * @param $extrasApuesta Es un array con los extras elegidos por el usuario
     * @param $user La entidad user que eligio los extras
     * @param $em
     * @return mixed devuelve un array con los datos necesarios
     */
    public function devolverDatosDelForm(
        $arrayExtras, $arrayExtrasUsuario)
    {

        for ($i = 1; $i <= count($arrayExtras); $i++) {

            $respuesta['valorDetalle' . $i] =
                $arrayExtras[$i - 1]->getValorDetalle();

            for ($j = 1; $j <= count($arrayExtrasUsuario); $j++) {

                $respuesta['extraUsu' . $i . $j] =
                    (isset($arrayExtrasUsuario[$j - 1][$i - 1]))
                        ? $arrayExtrasUsuario[$j - 1][$i - 1]->getIsCorrect()
                        : false;

            }

        }

        return $respuesta;

    }

}
