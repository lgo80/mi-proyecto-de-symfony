<?php

namespace futfunBundle\Form;

use futfunBundle\Form\ApuestaBaseType;
use futfunBundle\Form\ApuestasExtrasType;

class TorneoApuestaEspecialType
{

    /**
     * Esta funcion se especifica en formatear el builder para el formulario de la vista de crear
     * una apuesta nueva
     * @param $controler
     * @return mixed devuelve un builder con el formato del formulario para la vista de la apuesta nueva
     */
    public function crearGrupo($controler)
    {

        $builder = $controler->createFormBuilder();
        $builder
            ->add('apuesta', new ApuestaBaseType());

        for ($i = 1; $i <= 10; $i++) {

            $builder
                ->add('extra' . $i, new ApuestasExtrasType())
                ->add('isComplete' . $i, "text");

        }

        return $builder;

    }

}
