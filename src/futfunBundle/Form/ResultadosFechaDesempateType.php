<?php

namespace futfunBundle\Form;

use futfunBundle\Entity\Clubes;
use futfunBundle\Entity\TorneoBase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Range;

class ResultadosFechaDesempateType
{

    /**
     * Esta funcion se especifica en formatear el builder para el formulario de la vista de los
     * resultados de la fecha
     * @param $datos Son los datos que se obtuvieron del torneo
     * @param $controler
     * @return mixed devuelve un builder con el formato del formulario para la vista de los
     * resultados de la fecha
     */
    public function crearGrupo($datos, Controller $controler)
    {

        $fechaDesempate = $datos["fechaDesempate"];

        $defaultData = $this->devolverDatosDelForm($fechaDesempate);
        $builder = $controler->createFormBuilder($defaultData);

        foreach ($fechaDesempate->getDesempatePartidos() as &$partido) {

            $builder
                ->add('valorLocal' . $partido->getNumeroPartido(), "integer", array(
                    'constraints' => array(
                        new Range(
                            array(
                                'min' => 0,
                                'max' => 40,
                                'minMessage' => "�La cantidad de goles tiene que ser un numero entero mayor a 0!",
                                'maxMessage' => "�La cantidad de goles tiene que ser un numero entero menor a 40!"
                            ))
                    )))
                ->add('valorDefinicionLocal' . $partido->getNumeroPartido(), "integer", array(
                    'constraints' => array(
                        new Range(
                            array(
                                'min' => 0,
                                'max' => 40,
                                'minMessage' => "�La cantidad de goles tiene que ser un numero entero mayor a 0!",
                                'maxMessage' => "�La cantidad de goles tiene que ser un numero entero menor a 40!"
                            ))
                    )))
                ->add('valorVisita' . $partido->getNumeroPartido(), "integer", array(
                    'constraints' => array(
                        new Range(
                            array(
                                'min' => 0,
                                'max' => 40,
                                'minMessage' => "�La cantidad de goles tiene que ser un numero entero mayor a 0!",
                                'maxMessage' => "�La cantidad de goles tiene que ser un numero entero menor a 40!"
                            ))
                    )))
                ->add('valorDefinicionVisita' . $partido->getNumeroPartido(), "integer", array(
                    'constraints' => array(
                        new Range(
                            array(
                                'min' => 0,
                                'max' => 40,
                                'minMessage' => "�La cantidad de goles tiene que ser un numero entero mayor a 0!",
                                'maxMessage' => "�La cantidad de goles tiene que ser un numero entero menor a 40!"
                            ))
                    )));

        }

        return $builder;

    }

    /**
     * Esta funcion se especifica en procesar los datos de la fecha para llenar el builder del formulario
     * @param $fechaBase Es la fechaBase correspondiente para mostrar los resultados que ya estan completos
     * @return mixed devuelve un array con los datos necesarios
     */
    public function devolverDatosDelForm($fechaDesempate)
    {

        foreach ($fechaDesempate->getDesempatePartidos() as &$partido) {

            $respuesta['valorLocal' . $partido->getNumeroPartido()]
                = $partido->getValorLocal();
            $respuesta['valorDefinicionLocal' . $partido->getNumeroPartido()]
                = $partido->getValorDefinicionLocal();
            $respuesta['valorVisita' . $partido->getNumeroPartido()]
                = $partido->getValorVisita();
            $respuesta['valorDefinicionVisita' . $partido->getNumeroPartido()]
                = $partido->getValorDefinicionVisita();

        }

        return $respuesta;

    }

}
