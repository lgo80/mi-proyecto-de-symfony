<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class ApuestaExtraUsuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('detalle', 'text', array(
                'constraints' => array(
                    new Length(array(
                        'min' => 2, 'max' => 100,
                        'minMessage' => "El nombre de equipo tiene que tener mas de 2 caracteres",
                        'maxMessage' => "El nombre de equipo tiene que tener menos de 100 caracteres"))
                )));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\ApuestaExtraUsuario'
        ));
    }
}
