<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ApuestaBaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                'label' => 'Name:'
            ))
            ->add('puntosPartExactos', IntegerType::class, array(
                'label' => 'Exact Result:',
                'constraints' => array(
                    new NotBlank(
                        array('message' => "�Debe ingresar los puntos por acertar resultados exactos!")
                    ),
                    new Range(
                        array(
                            'min' => 1,
                            'max' => 100,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 1!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 100!"
                        ))
                )))
            ->add('puntosPartResultados', IntegerType::class, array(
                'label' => 'Result only:',
                'constraints' => array(
                    new NotBlank(
                        array('message' => "�Debe ingresar los puntos por acertar resultados!")
                    ),
                    new Range(
                        array(
                            'min' => 0,
                            'max' => 100,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 0!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 100!"
                        ))
                )))
            ->add('puntosPartErrados', IntegerType::class, array(
                'label' => 'Failed result:',
                'constraints' => array(
                    new NotBlank(
                        array('message' => "�Debe ingresar los puntos por partido fallados!")
                    ),
                    new Range(
                        array(
                            'min' => 0,
                            'max' => 100,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 0!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 100!"
                        ))
                )))
            ->add('apuestaSimbolica', MoneyType::class, array(
                'label' => 'Symbolic bet:',
                'currency' => 'ARS',
                'scale' => 2,
                'constraints' => array(
                    new Range(
                        array(
                            'min' => 0,
                            'max' => 1000000000,
                            'minMessage' => "�Los puntos tienen que ser un numero entero mayor a 0!",
                            'maxMessage' => "�Los puntos tienen que ser un numero entero menor a 1000000000!"
                        ))
                )))
            ->add('torneo', 'entity', array(
                'class' => 'futfunBundle:TorneoBase',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.estado = :estado')
                        ->setParameter('estado', utf8_encode('En progreso'))
                        ->orderBy('t.nombre', 'ASC');
                },
                'choice_label' => 'nombre',
                'label' => 'Tournament of reference:',
                'placeholder' => "Seleccione..."
            ))
            ->add('fechaLimiteExtraAt', "text", array(
                'label' => 'Extra deadline:'
            ))
            ->add('fechaLimite',   ChoiceType::class, [
                'choices' => array(
                    'partido' => "Por el comienzo de partido",
                    'fecha' => "Por el comienzo de fecha",),
                'multiple' => false,
                'expanded' => false,
                'label' => "Limite para la apuesta:"
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\ApuestaBase'
        ));
    }
}
