<?php

namespace futfunBundle\Form;

use futfunBundle\Entity\FechasBase;
use futfunBundle\Entity\GrupoFecha;
use futfunBundle\Entity\PartidoFecha;
use futfunBundle\Entity\Clubes;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class FechaEspecialEliminacionType
{

    /**
     * Esta funcion se especifica en formatear el builder para el formulario de la vista de la fecha
     * de eliminacion
     * @param $datos on los datos que se obtuvieron del torneo
     * @param $fecha Es un integer con la fecha o la entidad fechaBase
     * @param $em
     * @param $controler
     * @return mixed devuelve un builder con el formato del formulario para la vista de la fecha de
     * eliminacion
     */
    public function crearGrupo($datos, $fecha, $em, $controler)
    {

        $defaultData = $this->devolverDatosDelForm($datos, $fecha, $em);
        $builder = $controler->createFormBuilder($defaultData);
        $builder
            ->add('fecha', new FechasBaseType())
            ->add('fechaAt', "text", array(
                'label' => 'Start date:'
            ))
            ->add('timeAt', "text", array(
                'label' => 'Start time:'
            ));

        for ($i = 1; $i <= $datos["cantGrupos"]; $i++) {

            for ($j = 1; $j <= $datos["cantPartidos"]; $j++) {

                $builder
                    ->add('localPartido' . $i . $j, "text", array(
                        'constraints' => array(
                            new NotBlank(array('message' => "El nombre del equipo no puede ser nulo")),
                            new Length(array(
                                'min' => 4, 'max' => 100,
                                'minMessage' => "El nombre de equipo tiene que tener mas de 4 caracteres",
                                'maxMessage' => "El nombre de equipo tiene que tener menos de 100 caracteres"))
                        )
                    ))
                    ->add('visitaPartido' . $i . $j, "text", array(
                        'constraints' => array(
                            new NotBlank(array('message' => "El nombre del equipo no puede ser nulo")),
                            new Length(array(
                                'min' => 4, 'max' => 100,
                                'minMessage' => "El nombre de equipo tiene que tener mas de 4 caracteres",
                                'maxMessage' => "El nombre de equipo tiene que tener menos de 100 caracteres"))
                        )
                    ))
                    ->add('fechaPartidoAt' . $i . $j, "text", array(
                        'label' => 'Match date'
                    ))
                    ->add('horaPartidoAt' . $i . $j, "text", array(
                        'label' => 'Split date'
                    ));

            }

        }

        return $builder;

    }

    /**
     * Esta funcion se especifica en procesar los datos de la fecha para llenar el builder del formulario
     * @param $datos
     * @param $fecha
     * @param $em
     * @return array devuelve un array con los datos necesarios
     */
    public function devolverDatosDelForm($datos, $fecha, $em)
    {

        if (!is_int($fecha)) {

            $fechaAt = "";
            $horaAt = "";
            if (!empty($fecha->getFechaInicioAt())) {

                $fechaAt = $fecha->getFechaInicioAt()->format('D, M d, Y');
                $horaAt = $fecha->getFechaInicioAt()->format('H:i:s');

            }


            $respuesta = array('fecha' => $fecha,
                'fechaAt' => $fechaAt, 'timeAt' => $horaAt);

            for ($i = 1; $i <= $datos['cantGrupos']; $i++) {

                $grupoFecha = $fecha->getGrupoFechas()[$i - 1];

                for ($j = 1; $j <= $datos['cantPartidos']; $j++) {

                    $part = $grupoFecha->getPartidoFechas()[$j - 1];

                    $datosEliminacion = $em
                        ->getRepository('futfunBundle:DatosEliminacion')
                        ->devolverDatosXTorneoXFechaYPartido(
                            $datos['torneo'], $fecha->getNumeroFecha(), $j);

                    $respuesta['localPartido' . $i . $j] =
                        ($part->getLocal() == null)
                            ? $datosEliminacion->getTipoLocal()
                            : $part->getLocal()->getNombre();
                    $respuesta['visitaPartido' . $i . $j] =
                        ($part->getVisita() == null)
                            ? $datosEliminacion->getTipoVisita()
                            : $part->getVisita()->getNombre();

                    if (!empty($part->getFechaPartidoAt())) {

                        $respuesta['fechaPartidoAt' . $i . $j]
                            = $part->getFechaPartidoAt()->format('D, M d, Y');
                        $respuesta['horaPartidoAt' . $i . $j] = $part
                            ->getFechaPartidoAt()->format('H:i:s');

                    }


                }

            }

        } else {

            $fechaNueva = new FechasBase();
            $fechaNueva->setNumeroFecha($fecha);

            $nombreFecha = $em
                ->getRepository('futfunBundle:FechasBase')
                ->devolverNombreFecha($datos, true, $fecha);
            $fechaNueva->setNombreFecha($nombreFecha);

            $respuesta = [];

            for ($i = 1; $i <= $datos['cantGrupos']; $i++) {

                $grupoFechaNuevo = new GrupoFecha();

                for ($j = 1; $j <= $datos['cantPartidos']; $j++) {

                    $partido = new PartidoFecha();

                    $partido->setNumeroPartido($j);

                    $partido->setLocal(new Clubes(""));
                    $partido->setVisita(new Clubes(""));

                    $partido->setGrupoFecha($grupoFechaNuevo);

                    $grupoFechaNuevo->addPartidoFecha($partido);

                }

                $grupoFechaNuevo->setFechaBase($fechaNueva);

                $fechaNueva->addGrupoFecha($grupoFechaNuevo);

            }

            $respuesta['fecha'] = $fechaNueva;

        }

        return $respuesta;

    }

}
