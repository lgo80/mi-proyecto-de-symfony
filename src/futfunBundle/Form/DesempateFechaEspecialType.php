<?php

namespace futfunBundle\Form;

use futfunBundle\Entity\DesempateFecha;
use futfunBundle\Entity\DesempatePartido;
use futfunBundle\Entity\Clubes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class DesempateFechaEspecialType
{

    /**
     * Esta funcion se especifica en formatear el builder para el formulario de la vista de la fecha
     * @param $datos Son los datos que se obtuvieron del torneo
     * @param $fecha Es un integer con la fecha o la entidad fechaBase
     * @param $em
     * @param $controler
     * @return mixed devuelve un builder con el formato del formulario para la vista de la fecha
     */
    public function crearGrupo($datos, Controller $controler)
    {

        $fecha = $datos["fecha"];
        $em = $controler->getDoctrine()->getEntityManager();
        $defaultData = $this->devolverDatosDelForm($datos, $fecha, $em);
        $builder = $controler->createFormBuilder($defaultData);
        $builder
            ->add('fecha', new DesempateFechaType())
            ->add('fechaAt', "text", array(
                'label' => 'Start date:'
            ))
            ->add('timeAt', "text", array(
                'label' => 'Start time:'
            ))
            ->add('equipoLibre', 'text', array(
                'label' => 'Free team:'
            ));

        for ($j = 1; $j <= $datos["cantPartidos"]; $j++) {

            $builder
                ->add('localPartido' . $j, 'text', array(
                    'constraints' => array(
                        new NotBlank(array('message' => "El nombre del equipo no puede ser nulo")),
                        new Length(array(
                            'min' => 4, 'max' => 100,
                            'minMessage' => "El nombre de equipo tiene que tener mas de 4 caracteres",
                            'maxMessage' => "El nombre de equipo tiene que tener menos de 100 caracteres"))
                    )))
                ->add('visitaPartido' . $j, 'text', array(
                    'constraints' => array(
                        new NotBlank(array('message' => "El nombre del equipo no puede ser nulo")),
                        new Length(array(
                            'min' => 4, 'max' => 100,
                            'minMessage' => "El nombre de equipo tiene que tener mas de 4 caracteres",
                            'maxMessage' => "El nombre de equipo tiene que tener menos de 100 caracteres"))
                    )))
                ->add('fechaPartidoAt' . $j, "text", array(
                    'label' => 'Split date'
                ));

        }


        return $builder;

    }

    /**
     * Esta funcion se especifica en procesar los datos de la fecha para llenar el builder del formulario
     * @param $datos Son los datos que se obtuvieron del torneo
     * @param $fecha Es un integer con la fecha o la entidad fechaBase
     * @param $em
     * @return array devuelve un array con los datos necesarios
     */
    public function devolverDatosDelForm($datos, $fecha, $em)
    {

        if (!is_int($fecha) && $fecha != null) {

            $fechaAt = "";
            $horaAt = "";

            if (!empty($fecha->getFechaInicioAt())) {

                $fechaAt = $fecha->getFechaInicioAt()->format('D, M d, Y');
                $horaAt = $fecha->getFechaInicioAt()->format('H:i:s');

            }

            $equipoLibre = ($fecha->getEquipoLibre() != null)
                ? $fecha->getEquipoLibre()->getNombre()
                : null;

            $respuesta = array('fecha' => $fecha, 'fechaAt' => $fechaAt,
                'timeAt' => $horaAt, "equipoLibre" => $equipoLibre);

            for ($j = 1; $j <= $datos['cantPartidos']; $j++) {

                $part = $fecha->getDesempatePartidos()[$j - 1];

                $datosEliminacion = $em->getRepository('futfunBundle:DesempateDatosElimin')
                    ->devolverDatosXTorneoXFechaYPartido(
                        $datos['desempateDatos']->getTorneo(),
                        $fecha->getNumeroFecha(), $j);

                $respuesta['localPartido' . $j] = ($part->getLocal() != null)
                    ? $part->getLocal()->getNombre()
                    : $datosEliminacion->getTipoLocal();
                $respuesta['visitaPartido' . $j] = ($part->getVisita() != null)
                    ? $part->getVisita()->getNombre()
                    : $datosEliminacion->getTipoVisita();

                if (!empty($part->getFechaPartidoAt())) {

                    $respuesta['fechaPartidoAt' . $j] =
                        $part->getFechaPartidoAt()->format('D, M d, Y');

                }

            }

        } else {

            $fechaNueva = new DesempateFecha();
            $numeroFec = ($fecha == null) ? 1 : $fecha;

            $fechaNueva->setNumeroFecha($numeroFec);
            $fechaNueva->setNombreFecha($numeroFec);
            $fechaNueva->setTorneo($datos['desempateDatos']->getTorneo());

            if ($datos['isHabLibre']) {
                $fechaNueva->setEquipoLibre(new Clubes(""));
            }

            $respuesta = [];


            for ($j = 1; $j <= $datos['cantPartidos']; $j++) {

                $partido = new DesempatePartido();

                $partido->setNumeroPartido($j);

                $partido->setLocal(new Clubes(""));
                $partido->setVisita(new Clubes(""));

                $partido->setDesempateFecha($fechaNueva);

                $fechaNueva->addDesempatePartido($partido);

            }

            $respuesta['fecha'] = $fechaNueva;

        }

        return $respuesta;

    }

}
