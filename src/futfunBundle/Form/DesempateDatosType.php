<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class DesempateDatosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('golVisitante', ChoiceType::class, [
                'choices' => array(
                    '0' => 'No',
                    '1' => "Yes"),
                'multiple' => false,
                'expanded' => true,
                'placeholder' => "Select...",
                'label' => "Visitors goal:"
            ])
            ->add('forma', ChoiceType::class, [
                'choices' => array(
                    'soloida' => "One way",
                    'idayvuelta' => "Round trip"),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Form in the tiebreaker:"
            ])
            ->add('isConfirm', CheckboxType::class, [
                'label' => 'Do you want to confirm the jump-off data:',
                'data' => false
            ])
            ->add('tipoTorneo', 'entity', array(
                // query choices from this entity
                'class' => 'futfunBundle:TipoTorneo',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('tt')
                        ->where('tt.nombre <> :nombre')
                        ->setParameter('nombre', utf8_encode('Liga y Eliminación'))
                        ->orderBy('tt.id', 'ASC');
                },
                // use the User.username property as the visible option string
                'choice_label' => 'nombre',
                'label' => 'Tie-break type:',
                'placeholder' => "Seleccione..."
            ))
            ->add('vueltaGrupo', ChoiceType::class, [
                'choices' => array(
                    'igualIda' => "Same as the one going to invest the localias",
                    'InversaIda' => "Reverse one-way investment",
                    'pordefinir' => "Defining manually",),
                'multiple' => false,
                'expanded' => false,
                'placeholder' => "Select...",
                'label' => "Return form:"
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\DesempateDatos'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'futfunBundle_desempatedatos';
    }


}
