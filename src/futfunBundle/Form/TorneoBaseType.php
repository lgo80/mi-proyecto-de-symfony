<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use futfunBundle\Form\DatosTorneoType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TorneoBaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class,array(
                'label'=> 'Tournament name:'
            ))
            ->add('dato', new DatosTorneoType())
            ->add('tipoTorneo', 'entity', array(
                // query choices from this entity
                'class' => 'futfunBundle:TipoTorneo',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.id', 'ASC');
                },
                // use the User.username property as the visible option string
                'choice_label' => 'nombre',
                'label' => 'Tournament type:',
                'placeholder' => "Seleccione..."

                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ))
            ->add('save', "submit",array(
                'label' => 'Create Tournament'
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\TorneoBase'
        ));
    }
}
