<?php

namespace futfunBundle\Form;


class ApuestaFechaEspecialType
{

    /**
     * Esta funcion se especifica en formatear el builder para el formulario de la vista de
     * la fecha de la apuesta
     * @param $datos on los datos que se obtuvieron del torneo
     * @param $em
     * @param $controler
     * @param $user Es la entidad User que completo los resultados de la apuesta
     * @return mixed devuelve un builder con el formato del formulario para la vista de la fecha de la apuesta
     */
    public function crearGrupo($datos, $em, $controler, $user)
    {

        $fechaApuesta = $datos["apuestaFecha"];

        $defaultData = $this->devolverDatosDelForm(
            $fechaApuesta);
        $builder = $controler->createFormBuilder($defaultData);

        $i = 0;
        foreach ($fechaApuesta->getApuestasPartidos() as &$partido) {

            $i++;
            $builder->add('apuestaValor' . $i,
                new ApuestasPartidosType());


        }

        return $builder;

    }

    /**
     * Esta funcion se especifica en procesar los datos de la fecha para llenar el builder del formulario
     * @param $apuestasFecha Es la fechaBase que representa la fecha de la apuesta
     * @param $em
     * @param $user
     * @return mixed devuelve un array con los datos necesarios
     */
    public function devolverDatosDelForm($fechaApuesta)
    {

        $i = 0;
        foreach ($fechaApuesta->getApuestasPartidos() as &$partido) {

            $i++;
            $respuesta['apuestaValor' . $i]
                = $partido;

        }

        return $respuesta;

    }

}
