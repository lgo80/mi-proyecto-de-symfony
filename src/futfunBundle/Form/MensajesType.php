<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MensajesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('descripcion', TextareaType::class, array(
                'constraints' => array(
                    new NotBlank(array(
                        'message' => utf8_encode("La descripci�n no puede estar vacio")
                    )))
            ))
            ->add('para', "text", array(
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => utf8_encode("�Debe ingresar el usuario que le quiere enviar el mensaje!")
                    )))
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'futfunBundle\Entity\Mensajes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'futfunBundle_mensajes';
    }


}
