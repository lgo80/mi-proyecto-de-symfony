<?php

namespace futfunBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class UserClaveEspecialType
{

    /**
     * Esta funcion se especifica en formatear el builder para el formulario de la vista de la fecha
     * @param $datos Son los datos que se obtuvieron del torneo
     * @param $fecha Es un integer con la fecha o la entidad fechaBase
     * @param $em
     * @param $controler
     * @return mixed devuelve un builder con el formato del formulario para la vista de la fecha
     */
    public function crearGrupo($userLogueado, $controler)
    {

        $defaultData = null;
        $builder = $controler->createFormBuilder($defaultData);
        if ($userLogueado != null) {
            $builder->add('passwordActual', PasswordType::class, array(
                'required' => true,
                'label' => 'Clave'
            ));
        }
        $builder
            ->
            add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'La claves no coincide',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array(
                    'label' => 'Clave',
                    'constraints' => array(
                        new NotBlank(
                            array('message' => utf8_encode("�Este campo no puede estar vacio!"))
                        ),
                        new Length(
                            array(
                                'min' => 4, 'max' => 100,
                                'minMessage' => "El nombre del extra tiene que tener mas de 4 caracteres",
                                'maxMessage' => "El nombre del extra tiene que tener menos de 100 caracteres")
                        ))),
                'second_options' => array('label' => 'Repetir clave')

            ));


        return $builder;

    }

}
