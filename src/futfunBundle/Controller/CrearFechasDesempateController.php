<?php

namespace futfunBundle\Controller;

use futfunBundle\Form\DesempateDatosType;
use futfunBundle\Form\DesempateFechaEspecialType;
use futfunBundle\Servicios\ServCrearFechasDesempate;
use futfunBundle\Validador\ValidarCrearDesempate;
use futfunBundle\Validador\ValidarCrearFechasDesempate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class CrearFechasDesempateController extends Controller
{
    public function crearFechasDesempateAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');
        $numeroFecha = $request->query->get('numeroFecha');
        $direccion = $request->query->get('direccion');
        $em = $this->getDoctrine()->getEntityManager();

        $servicio = new ServCrearFechasDesempate();
        $userLogueado = $this->getUser();

        $datos = $servicio->devolverDatos($idTorneo, $em, $userLogueado,
            $numeroFecha, $direccion, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            if ($datos["respuesta"]["lugar"]
                == "trabajo_dbbundle_torneos_crear_desempate" ||
                $datos["respuesta"]["lugar"]
                == "trabajo_dbbundle_torneos_ver_datos_desempate"
            ) {
                return $this->redirectToRoute($datos["respuesta"]["lugar"], array("idTorneo" => $idTorneo));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }

        }

        $fechaEspecial = new DesempateFechaEspecialType();

        $form = $fechaEspecial->crearGrupo($datos, $this)->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $request->isMethod('POST')) {

            $validar = new ValidarCrearFechasDesempate();

            $valor = $validar->validarFechaVacioYPermitido($form, $datos,
                $this);

            if ($valor["isValid"]) {

                $errorList = $validar->validarFechaRepetido($form, $datos, true);

                if (count($errorList) == 0) {

                    $fecha = $em->getRepository('futfunBundle:DesempateFecha')
                        ->armarFechas($form, $datos);

                    $em->getRepository('futfunBundle:DesempateFecha')
                        ->grabarFechaIdaYVuelta($fecha, $datos);

                    $this->get('session')->getFlashBag()->add(
                        "mensaje", "La fecha se grabo correctamente"
                    );

                    return $this->redirectToRoute('trabajo_dbbundle_torneos_crear_fechas_desempate',
                        array("idTorneo" => $datos["desempateDatos"]->getTorneo()->getId(),
                            "numeroFecha" => $fecha->getNumeroFecha()));

                } else {

                    foreach ($errorList as &$valor) {
                        if (isset($valor)) {
                            $errorMessage1 = new FormError($valor);
                            $form->get('fechaAt')->addError($errorMessage1);
                        }
                    }

                }

            } else {

                $errorList = $valor["errorList"];

                foreach ($errorList as &$valor) {
                    if (isset($valor[0])) {
                        $errorMessage1 = new FormError($valor[0]->getMessage());
                        $form->get('equipoLibre')->addError($errorMessage1);
                    }
                }

            }

        }

        return $this->render(
            'futfunBundle:CrearFechasDesempate:crearfechasdesempate.html.twig',
            array("datos" => $datos, "form" => $form->createView()));

    }

}
