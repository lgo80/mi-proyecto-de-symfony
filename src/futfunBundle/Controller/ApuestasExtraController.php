<?php

namespace futfunBundle\Controller;

use futfunBundle\Form\ApuestaExtraPartEspecial2Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use futfunBundle\Form\ApuestaExtraPartEspecialType;
use futfunBundle\Servicios\ServCompletarExtras;
use futfunBundle\Validador\ValidarCompletarExtras;

class ApuestasExtraController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir a la vista
     * para que los usuarios completen los extras de las apuestas
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function completarExtraAction(Request $request)
    {

        $idApuesta = $request->query->get('idApuesta');
        $idUsuario = $request->query->get('idUsuario');

        $servicio = new ServCompletarExtras();
        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarDatos(
            $idApuesta, $em, $userLogueado, $this, $idUsuario);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"]
                == "trabajo_dbbundle_apuestas_ver_tabla"
            ) {
                return $this->redirectToRoute(
                    $datos["respuesta"]["lugar"],
                    array("idApuesta" => $idApuesta));
            } else {
                return $this->redirectToRoute(
                    $datos["respuesta"]["lugar"]);
            }


        }

        $apuestaExtraEspecial = new ApuestaExtraPartEspecialType();
        $form = $apuestaExtraEspecial->crearGrupo(
            $datos, $em, $this, $userLogueado)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $validar = new ValidarCompletarExtras();

            if ($validar->validarAlgunoCompletado(
                $datos["extrasApuesta"], $form)
            ) {

                if (!$validar->validarFechaNoCumplida(
                    $datos["apuesta"], $em)
                ) {

                    $em
                        ->getRepository('futfunBundle:ApuestaExtraUsuario')
                        ->grabarExtras($form, $datos["extrasApuesta"]);

                } else {

                    $this->get('session')
                        ->getFlashBag()->add(
                            "mensaje-danger",
                            "No se puede grabar ya se cumplio la fecha limite"
                        );

                }

            } else {

                $this->get('session')->getFlashBag()->add(
                    "mensaje-danger",
                    "Debe haber por lo menos 1 extra completado para guardar"
                );

            }

        }

        return $this->render('futfunBundle:ApuestasExtra:completarApuestaExtras.html.twig',
            array("datos" => $datos, "form" => $form->createView()));

    }

    public function confirmarResultadosExtrasAction(Request $request)
    {

        $idApuesta = $request->query->get('idApuesta');

        $servicio = new ServCompletarExtras();
        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarDatosConfirmar(
            $idApuesta, $em, $userLogueado, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"] ==
                "trabajo_dbbundle_apuestas_ver_tabla"
            ) {
                return $this->redirectToRoute(
                    $datos["respuesta"]["lugar"],
                    array("idApuesta" => $idApuesta));
            } else {
                return $this->redirectToRoute(
                    $datos["respuesta"]["lugar"]);
            }


        }

        $apuestaExtraEspecial = new ApuestaExtraPartEspecial2Type();
        $form = $apuestaExtraEspecial->crearGrupo($datos, $this)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em->getRepository('futfunBundle:ApuestaExtraUsuario')
                ->modificarExtras($form, $datos["extrasApuesta"],
                    $datos["apuestaExtraUsuarios"]);

            $apuestasDelTorneo = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->devolverApuestasPorTorneo(
                    $datos["apuesta"]->getTorneo());
            $em->getRepository('futfunBundle:ApuestasParticipantes')
                ->actualizarTabla($apuestasDelTorneo);

            $nextAction = $form->get('saveAndConfirm')
                ->isClicked();

            if ($nextAction) {

                $apuesta = $datos["apuesta"];
                $apuesta->setEstado("Finalizado");
                $em->getRepository('futfunBundle:ApuestaBase')
                    ->alta($apuesta);

                $apuestasBase = $em
                    ->getRepository('futfunBundle:ApuestaBase')
                    ->devolverApuestasPorTorneo($apuesta->getTorneo());

                $em->getRepository('futfunBundle:ApuestasParticipantes')
                    ->actualizarCampeon($apuestasBase);

                $this->get('session')->getFlashBag()->add(
                    "mensaje-sucess", "La apuesta ha sido guardada
                    y finalizada correctamente");

                return $this->redirectToRoute(
                    'trabajo_dbbundle_apuestas_ver_tabla',
                    array("idApuesta" => $idApuesta));

            }

            $this->get('session')->getFlashBag()->add(
                "mensaje-sucess", "La apuesta ha
                sido guardada correctamente");

            return $this->redirectToRoute(
                'trabajo_dbbundle_apuesta_confirmarextras',
                array("idApuesta" => $idApuesta));

        }

        return $this->render(
            'futfunBundle:ApuestasExtra:confirmarApuestaExtras.html.twig',
            array("datos" => $datos, "form" => $form->createView()));

    }

}
