<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\ApuestasFechas;
use futfunBundle\Entity\ApuestasPartidos;
use futfunBundle\Servicios\ServInicio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdministradorController extends Controller
{
    public function indexAction(Request $request)
    {

        $servicio = new ServInicio();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->validarDatos(
            $this->getUser(), $em, $this);

        return $this->render('futfunBundle:Administrador:index.html.twig',
            compact("datos"));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getApuestasElegidasAction(Request $request)
    {

        $valor = $_POST["opt"];

        $servicio = new ServInicio();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->getDatosApuestasElegida(
            $this->getUser(), $em, $valor);

        return new Response(json_encode(
            array(
                'mensaje' => $datos)));

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getApuestasElegidas2Action(Request $request)
    {

        $idApuesta = $_POST["idApu"];
        $opt = $_POST["opt"];

        $servicio = new ServInicio();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->getArrayAccesos(
            $idApuesta, $opt, $em, $this->getUser());

        return new Response(json_encode(
            array(
                'mensaje' => $datos)));

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getTorneosElegidos2Action(Request $request)
    {

        $idApuesta = $_POST["idApu"];
        $opt = $_POST["opt"];

        $servicio = new ServInicio();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->getArrayAccesosT(
            $idApuesta, $opt, $em, $this->getUser());

        return new Response(json_encode(
            array(
                'mensaje' => $datos)));

    }

    public function pruebaAction(Request $request)
    {


        /*$em = $this->getDoctrine()->getManager();

        $torneo = $em->getRepository('futfunBundle:TorneoBase')->find(1);

        $fecha = $em->getRepository('futfunBundle:FechasBase')
            ->devolverFechaDelTorneo($torneo, 1);

        $grupos = $fecha[0]->getGrupos();

        print_r("Cantidad: " . Count($grupos[0]->getPartidoFechas()));

        return new Response("<br />estoy bien ");*/

        /*$em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('futfunBundle:User')->find(2);

        //exit("EL usuario es: " . $user->getUsername());

        $factory = $this->get('security.encoder_factory');

        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword(
            "boca2018", $user->getSalt());
        $user->setPassword($password);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();*/

        /*  $em = $this->getDoctrine()->getManager();
          $apuestasFechaMal = $em
              ->getRepository('futfunBundle:ApuestasFechasMal')
              ->findAll();

          $arrayPartidos = [];

          foreach ($apuestasFechaMal as &$fechaApuMal) {

              /*  var_dump("partido: "
                    . $fechaApuMal->getPartidoFecha()->getLocal()->getNombre()
                    . " vs " . $fechaApuMal->getPartidoFecha()->getVisita()->getNombre()
                    . " real: " . $fechaApuMal->getPartidoFecha()->getValorLocal()
                    . " - " . $fechaApuMal->getPartidoFecha()->getValorVisita()
                    . " apuesta: " . $fechaApuMal->getResultadoLocal()
                    . " - " . $fechaApuMal->getResultadoVisita()
                    . " usuario: " . $fechaApuMal->getUser()->getUsername()
                    . " puntos: " . $fechaApuMal->getPuntos()

                );
                var_dump("<br />");*/

        /*   $usuarioApuesta = $fechaApuMal->getUser();
           $apuestaBase = $fechaApuMal->getApuestaBase();
           $fechaBase = $fechaApuMal->getPartidoFecha()
               ->getGrupoFecha()->getFechaBase();

           $apuestasFechaBien = $em
               ->getRepository('futfunBundle:ApuestasFechas')
               ->devolverXApuestaXUserXFecha(
                   $apuestaBase, $usuarioApuesta, $fechaBase);

           if ($apuestasFechaBien == null) {

               $apuestasFechaBien = new ApuestasFechas();
               $apuestasFechaBien->setUser($usuarioApuesta);
               $apuestasFechaBien->setFechaModificacionAt(
                   $fechaApuMal->getFechaModificacionAt());
               $apuestasFechaBien->setFechaDesempate(null);
               $apuestasFechaBien->setApuestaBase($apuestaBase);
               $apuestasFechaBien->setFechaBase($fechaBase);

           }

           $apuestasFechaBien = $em
               ->getRepository('futfunBundle:ApuestasFechas')
               ->grabarNuevo($apuestasFechaBien);

           $apuestaPartido = new ApuestasPartidos();

           $apuestaPartido->setDesempatePartido(null);
           $apuestaPartido->setPartidoFecha($fechaApuMal->getPartidoFecha());
           $apuestaPartido->setPuntos($fechaApuMal->getPuntos());
           $apuestaPartido->setResultadoLocal($fechaApuMal->getResultadoLocal());
           $apuestaPartido->setResultadoVisita($fechaApuMal->getResultadoVisita());

           $nombreDeGrupo = ($fechaApuMal->getPartidoFecha()->getGrupoFecha()->getGrupo() != null)
               ? $fechaApuMal->getPartidoFecha()->getGrupoFecha()->getGrupo()->getNombre()
               : "A";

           $arrayPartidos[$apuestaBase->getId()][$usuarioApuesta->getId()]
           [$fechaBase->getNumeroFecha()]
           [$nombreDeGrupo]
           [$fechaApuMal->getPartidoFecha()->getNumeroPartido()] = $apuestaPartido;

       }

       $apuestasFechaBien = $em
           ->getRepository('futfunBundle:ApuestasFechas')
           ->findAll();

       foreach ($apuestasFechaBien as &$apuestaFechaBien) {

           if ($apuestaFechaBien->getApuestaBase()->getId() == 1) {

               if ($apuestaFechaBien->getFechaBase()->getNumeroFecha() >= 1 &&
                   $apuestaFechaBien->getFechaBase()->getNumeroFecha() <= 3
               ) {
                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["B"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["B"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["C"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["C"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["D"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["D"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["E"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["E"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["F"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["F"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["G"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["G"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["H"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["H"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

               } elseif ($apuestaFechaBien->getFechaBase()->getNumeroFecha() >= 6 &&
                   $apuestaFechaBien->getFechaBase()->getNumeroFecha() <= 7
               ) {

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

               } elseif ($apuestaFechaBien->getFechaBase()->getNumeroFecha() == 4
               ) {

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [3];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [4];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [5];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [6];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [7];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [8];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

               } elseif ($apuestaFechaBien->getFechaBase()->getNumeroFecha() == 5
               ) {

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [1];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [2];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [3];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["A"]
                   [4];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

               }

           } else {

               for ($i = 1; $i < 14; $i++) {

                   $partidoApuesta = $arrayPartidos
                   [$apuestaFechaBien->getApuestaBase()->getId()]
                   [$apuestaFechaBien->getUser()->getId()]
                   [$apuestaFechaBien->getFechaBase()->getNumeroFecha()]
                   ["Unico"]
                   [$i];

                   $partidoApuesta->setApuestasFecha($apuestaFechaBien);
                   $apuestaFechaBien->addApuestasPartido($partidoApuesta);

               }

           }

           $em
               ->getRepository('futfunBundle:ApuestasFechas')
               ->grabarNuevo($apuestaFechaBien);

       }

       /*$apuestasFechaBien = $em
           ->getRepository('futfunBundle:ApuestasFechas')
           ->findAll();

       for ($i = 28; $i < 33; $i++) {

           var_dump("Fecha: " . $apuestasFechaBien[$i]->getFechaBase()->getNumeroFecha());
           var_dump("<br />");
           var_dump("Usuario: " . $apuestasFechaBien[$i]->getUser()->getUsername());
           var_dump("<br />");
           var_dump("<br />");

           foreach ($apuestasFechaBien[$i]->getApuestasPartidos() as &$partidoApu) {

               var_dump("partido: "
                   . $partidoApu->getPartidoFecha()->getLocal()->getNombre()
                   . " vs " . $partidoApu->getPartidoFecha()->getVisita()->getNombre()
                   . " real: " . $partidoApu->getPartidoFecha()->getValorLocal()
                   . " - " . $partidoApu->getPartidoFecha()->getValorVisita()
                   . " apuesta: " . $partidoApu->getResultadoLocal()
                   . " - " . $partidoApu->getResultadoVisita()
                   . " puntos: " . $partidoApu->getPuntos()

               );

               var_dump("<br />");
               var_dump("-------------------------------------------");
               var_dump("<br />");
               var_dump("<br />");

           }

       }


       /*foreach ($apuestasFechaBien[$i]->getApuestasPartidos() as &$partidoApu) {

           var_dump("partido: "
               . $partidoApu->getPartidoFecha()->getLocal()->getNombre()
               . " vs " . $partidoApu->getPartidoFecha()->getVisita()->getNombre()
               . " real: " . $partidoApu->getPartidoFecha()->getValorLocal()
               . " - " . $partidoApu->getPartidoFecha()->getValorVisita()
               . " apuesta: " . $partidoApu->getResultadoLocal()
               . " - " . $partidoApu->getResultadoVisita()
               . " puntos: " . $partidoApu->getPuntos()

           );

           var_dump("<br />");

       }
       /*    $apuestaBase = $em
               ->getRepository('futfunBundle:ApuestaBase')
               ->find(1);

           $usuario = $em
               ->getRepository('futfunBundle:User')
               ->find(4);

           $fechaBase = $em
               ->getRepository('futfunBundle:FechasBase')
               ->find(1);

           $apuestasFechaMal = $em
               ->getRepository('futfunBundle:ApuestasFechas')
               ->devolverXApuestaXUserXFecha(
                   $apuestaBase, $usuario, $fechaBase);



           foreach ($apuestasFechaMal->getApuestasPartidos() as &$partidoApu) {

               var_dump("partido: "
                   . $partidoApu->getPartidoFecha()->getLocal()->getNombre()
                   . " vs " . $partidoApu->getPartidoFecha()->getVisita()->getNombre()
                   . " real: " . $partidoApu->getPartidoFecha()->getValorLocal()
                   . " - " . $partidoApu->getPartidoFecha()->getValorVisita()
                   . " apuesta: " . $partidoApu->getResultadoLocal()
                   . " - " . $partidoApu->getResultadoVisita()
                   . " puntos: " . $partidoApu->getPuntos()

               );

               var_dump("<br />");

           }
   */

        //   exit("Supuestamente funciono");

    }

}
