<?php

namespace futfunBundle\Controller;

use futfunBundle\Servicios\ServFuncionesUtiles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use futfunBundle\Form\ResultadosFechaTorneoType;
use futfunBundle\Servicios\ServTorneoResultados;
use futfunBundle\Validador\ValidarTorneoResultados;
use Symfony\Component\HttpFoundation\Response;

class TorneoCompletarResultadosController extends Controller
{

    /**
     * Esta funcion se especifica en preparar los datos para la vista de completar los resultados reales
     * de los partidos y grabarlos en el sistema
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function completarResultadosAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');
        $numeroFecha = $request->query->get('numeroFecha');
        $direccion = $request->query->get('direccion');

        $servicio = new ServTorneoResultados();
        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarDatos(
            $idTorneo, $em, $userLogueado,
            $numeroFecha, $direccion, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"] ==
                "trabajo_dbbundle_torneos_comprobarFecha"
            ) {
                return $this->redirectToRoute($datos["respuesta"]["lugar"],
                    array("idTorneo" => $idTorneo));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }


        }

        $apuestaFechaEspecial = new ResultadosFechaTorneoType();
        $form = $apuestaFechaEspecial->crearGrupo($datos, $this)->getForm();
        $form->handleRequest($request);

        $ganador = $this->devolverStringGanador($datos["definicion"], $form,
            $datos["fechaBase"], $datos["fechaAnterior"], $datos["isGolVisitante"]);

        if ($form->isValid()) {

            $validar = new ValidarTorneoResultados();

            $valor = $validar->validarVacioDeAmbosResult(
                $datos["fechaBase"], $form);

            if ($valor) {

                $fechaBase = $servicio
                    ->armarFechaConResultados(
                        $datos["fechaBase"], $form);

                $em->getRepository('futfunBundle:FechasBase')
                    ->grabarNuevo($fechaBase);

                if ($servicio->verSiEsLiga($datos["torneo"], $numeroFecha, $em)) {

                    $em->getRepository('futfunBundle:PosicionesLiga')
                        ->actualizarPosiciones($datos["torneo"]);

                }

                if ($datos["torneo"]->getTipoTorneo()->getNombre() != "Liga") {

                    $em->getRepository('futfunBundle:FechasBase')
                        ->actualizarFechaSiguiente($datos);


                }
                $isDesempate = $em
                    ->getRepository('futfunBundle:PosicionesLiga')
                    ->actualizarCampeon($datos["torneo"]);

                if ($isDesempate) {

                    $isExistsDesempate = $em
                        ->getRepository('futfunBundle:DesempateDatos')
                        ->alta($datos["torneo"]);

                    if ($isExistsDesempate) {

                        return null;

                    }

                    return $this
                        ->redirectToRoute("trabajo_dbbundle_torneos_crear_desempate",
                            array("idTorneo" => $datos["torneo"]->getId()));

                }

                $em->getRepository('futfunBundle:TorneoBase')
                    ->verificarFinDelTorneo($datos["torneo"]);

                $apuestasBase = $servicio
                    ->verSiHayApuestaCreada($datos["torneo"], $em);

                if (!empty($apuestasBase)) {

                    $em->getRepository('futfunBundle:ApuestasFechas')
                        ->actualizarPuntosFechaApuestas($fechaBase);

                    $em->getRepository('futfunBundle:ApuestasFechas')
                        ->procesarWinDate($apuestasBase, $fechaBase, $em);

                    $em->getRepository('futfunBundle:ApuestasParticipantes')
                        ->actualizarTabla($apuestasBase);

                    $em->getRepository('futfunBundle:ApuestasParticipantes')
                        ->actualizarCampeon($apuestasBase);

                }

                $this->get('session')->getFlashBag()->add(
                    "mensaje-sucess", $this
                    ->get('translator')
                    ->trans("The data was saved correctly")
                );


            } else {

                $this->get('session')->getFlashBag()->add(
                    "mensaje-danger", $this
                    ->get('translator')
                    ->trans("There has to be at least 1 result completed and with the valuesof the local and visitor"));

            }

        }

        $modificarFormAjax = $this->createCustomForm(
            'POST', 'trabajo_dbbundle_torneos_modificar_fecha', "_IDFECHA", "_LUGAR");

        return $this->render(
            'futfunBundle:TorneoCompletarResultados:completarResultadosFecha.html.twig',
            array(
                "datos" => $datos,
                "form" => $form->createView(),
                "ganador" => $ganador,
                "modificar_form_ajax" => $modificarFormAjax->createView()));
    }

    private function devolverStringGanador($isDefinicion, $form, $fechaBase,
                                           $fechaBaseAnterior, $isGolVisitante)
    {

        $respuesta = [];


        $i = 0;

        foreach ($fechaBase->getGrupoFechas() as &$valor) {

            $i++;

            foreach ($valor->getPartidoFechas() as &$valorPart) {

                if (!$isDefinicion) {
                    $valorLocal = $form
                        ->get('valorLocal' . $i . $valorPart->getNumeroPartido())
                        ->getData();
                    $valorVisita = $form
                        ->get('valorVisita' . $i . $valorPart->getNumeroPartido())
                        ->getData();
                    $valorDefLocal = $form
                        ->get('valorDefinicionLocal' . $i . $valorPart->getNumeroPartido())
                        ->getData();
                    $valorDefVisita = $form
                        ->get('valorDefinicionVisita' . $i . $valorPart->getNumeroPartido())
                        ->getData();

                    if (is_numeric($valorVisita)
                        && is_numeric($valorLocal)
                    ) {

                        if (is_numeric($valorDefLocal)
                            && is_numeric($valorDefVisita)
                        ) {

                            $respuesta[$valorPart->getNumeroPartido()] =
                                ($valorDefLocal > $valorDefVisita)
                                    ? "local" : "visita";

                        } else {

                            if ($fechaBaseAnterior != null) {

                                $partidoAnterior = $fechaBaseAnterior
                                    ->getGrupoFechas()[$i - 1]
                                    ->getPartidoFechas()[$valorPart->getNumeroPartido() - 1];

                                $valorLocalTotal = intval($valorLocal)
                                    + intval($partidoAnterior->getValorVisita());
                                $valorVisitaTotal = intval($valorVisita)
                                    + intval($partidoAnterior->getValorLocal());

                                if ($valorLocalTotal != $valorVisitaTotal) {

                                    $respuesta[$valorPart->getNumeroPartido()]
                                        = ($valorLocalTotal > $valorVisitaTotal)
                                        ? "local" : "visita";

                                } else {

                                    $valorLocalTotal +=
                                        intval($partidoAnterior->getValorVisita());
                                    $valorVisitaTotal +=
                                        intval($valorVisita);

                                    $respuesta[$valorPart->getNumeroPartido()]
                                        = ($valorLocalTotal > $valorVisitaTotal)
                                        ? "local" : "visita";

                                }


                            } else {

                                $respuesta[$valorPart->getNumeroPartido()] =
                                    ($valorLocal > $valorVisita)
                                        ? "local" : "visita";

                            }

                        }

                    } else {

                        $respuesta[$valorPart->getNumeroPartido()] = "";

                    }
                } else {

                    $respuesta[$valorPart->getNumeroPartido()] = "";

                }
            }

        }


        return $respuesta;

    }

    public function modificarLaFechaAction(Request $request)
    {

        $fechaPost = $request->request->get('txtFechaInicio');
        $horaPost = $request->request->get('txtHoraInicio');
        $nombreFecha = $request->request->get('txtNombreFecha');
        $lugar = $request->query->get('lugar');

        if (($lugar != 'lblT2' && $fechaPost && $horaPost) ||
            ($lugar == 'lblT2' && $nombreFecha && strlen($nombreFecha) <= 25)
        ) {

            $idFecha = $request->query->get('idFecha');

            $em = $this->getDoctrine()->getManager();
            $servicio = new ServFuncionesUtiles();

            $fecha = $em->getRepository('futfunBundle:FechasBase')
                ->find($idFecha);

            switch ($lugar) {
                case 'lblT2':

                    $fecha->setNombreFecha($nombreFecha);
                    break;
                case 'lblT4':

                    $dateFecha = $servicio->armarFechaYHora(
                        $fechaPost, $horaPost);
                    $fecha->setFechaInicioAt($dateFecha);
                    break;

                default:

                    $i = 0;

                    foreach ($fecha->getGrupoFechas() as &$grupo) {

                        $i++;

                        foreach ($grupo->getPartidoFechas() as &$partido) {

                            $lugarCreado = 'lblT' . $i . $partido->getNumeroPartido();

                            if ($lugarCreado == $lugar) {

                                $dateFecha = $servicio->armarFechaYHora(
                                    $fechaPost, $horaPost);
                                $partido->setFechaPartidoAt($dateFecha);

                            }
                        }
                    }
                    break;
            }

            $em->getRepository('futfunBundle:FechasBase')
                ->grabarNuevo($fecha);

            $error = false;

            $mensaje = ($lugar != 'lblT2')
                ? $servicio
                    ->formatearFechaEspanolUnica($dateFecha)
                : $nombreFecha;
            $elemento = $lugar;


        } else {

            $error = true;
            $mensaje = ($lugar == "dia")
                ? utf8_encode("�La fecha y hora son incorrectos!")
                : utf8_encode('�El nombre de la fecha es incorrecta!');
            $elemento = "";
        }

        return new Response(
            json_encode(array(
                'error' => $error,
                'mensaje' => $mensaje,
                'elemento' => $elemento)),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    private function createCustomForm($method, $route, $idFecha, $lugar)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($route,
                array('idFecha' => $idFecha, 'lugar' => $lugar)))
            ->setMethod($method)
            ->getForm();

        return $form;

    }
}
