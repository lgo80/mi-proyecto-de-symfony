<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use futfunBundle\Form\FechaEspecialType;
use futfunBundle\Form\FechaEspecialEliminacionType;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Component\Form\FormError;
use futfunBundle\Servicios\ServArmarFechas;

class ArmarFechasTorneoController extends Controller
{

    public function pruebaAction(Request $request)
    {
    }

    /**
     * Esta funcion se especifica en obtener los datos especificos para las vistas y redireccionar a otra
     * funcion que corresponda segun los datos ingresados por GET
     * @param Request $request
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function comprobarFechaAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');
        $numeroFecha = $request->query->get('numeroFecha');
        $direccion = $request->query->get('direccion');

        $em = $this->getDoctrine()->getManager();
        $servicio = new ServArmarFechas();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarPrimera($idTorneo, $em, $userLogueado,
            $numeroFecha, $direccion);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["lugar"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"] == "trabajo_dbbundle_torneos_ver_tabla") {
                return $this->redirectToRoute($datos["respuesta"]["lugar"], array("idTorneo" => $idTorneo));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }
        }

        switch ($datos["page"]) {

            case "primera":

                return $this->forward('futfunBundle:ArmarFechasTorneo:crearPrimeraFecha', array(
                    'torneo' => $datos["torneo"], 'isHabilitarGrupo' => $datos["isHabilitarGrupo"],
                    "valorGrupo" => $datos["valorGrupo"]
                ));
                break;

            case "liga":

                return $this->forward('futfunBundle:ArmarFechasTorneo:crearFechasLiga', array(
                    'torneo' => $datos["torneo"], 'numFecha' => $datos['fechas']
                ));
                break;

            case "eliminacion":

                return $this->forward('futfunBundle:ArmarFechasTorneo:crearFechasEliminacion', array(
                    'torneo' => $datos["torneo"], 'numFecha' => $datos['fechas']
                ));
                break;

        }

        return "";

    }

    /**
     * Esta funcion se especifica en obtener los datos faltantes y renderizar la vista de la
     * primera fecha
     * @param Request $request
     * @param $torneo Es el torneo que el usuario eligio
     * @param $isHabilitarGrupo Es un booleano que habilita o no el modificar el nombre de grupo
     * @param $valorGrupo Es un string que va en el nombre de grupo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function crearPrimeraFechaAction(
        Request $request, $torneo, $isHabilitarGrupo, $valorGrupo)
    {

        $servicio = new ServArmarFechas();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->obtenerDatosPrimera($torneo, 1, $em, $this);

        $fechaEspecial = new FechaEspecialType();
        $form = $fechaEspecial->crearGrupo($datos, null, $em, $this)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $validar = new ValidarFechas();

            $valor = $validar->validarFechaVacioYPermitido($form, $datos, $this);

            if ($valor["isValid"]) {

                $errorList = $validar->validarFechaRepetido($form, $datos, true);

                if (count($errorList) == 0) {

                    $fecha = $em->getRepository('futfunBundle:FechasBase')
                        ->armarFechas($form, $torneo, "primera");

                    $fecha->setTorneo($torneo);

                    $em->getRepository('futfunBundle:FechasBase')
                        ->grabarFechaIdaYVuelta($fecha, $datos);

                    $this->get('session')->getFlashBag()->add(
                        "mensaje", "La fecha se grabo correctamente"
                    );

                    return $this->redirectToRoute('trabajo_dbbundle_torneos_comprobarFecha',
                        array("idTorneo" => $torneo->getId(), "numeroFecha" => 2));

                } else {

                    foreach ($errorList as &$valor) {
                        if (isset($valor)) {
                            $errorMessage1 = new FormError($valor);
                            $form->get('fecha')->addError($errorMessage1);
                        }
                    }

                }
            } else {

                $errorList = $valor["errorList"];

                foreach ($errorList as &$valor) {
                    if (isset($valor[0])) {
                        $errorMessage1 = new FormError($valor[0]->getMessage());
                        $form->get('fecha')->addError($errorMessage1);
                    }
                }

            }


        }


        return $this->render('futfunBundle:ArmarFechasTorneo:primeraFecha.html.twig', array(
            "form" => $form->createView(), "datos" => $datos, "isHabilitarGrupo" => $isHabilitarGrupo,
            "valorGrupo" => $valorGrupo
        ));

    }

    /**
     * Esta funcion se especifica en obtener los datos faltantes y renderizar la vista de las
     * fechas de la liga
     * @param Request $request
     * @param $torneo Es el torneo que el usuario eligio
     * @param $numFecha Puede ser un integer con el numero de fecha o la entidad FechaBase
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function crearFechasLigaAction(Request $request,
                                          $torneo, $numFecha)
    {

        $servicio = new ServArmarFechas();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->obtenerDatosPrimera($torneo, $numFecha, $em, $this);
        $posiciones = $em->getRepository('futfunBundle:PosicionesLiga')
            ->devolverPosiciones($datos['torneo']);

        $fechaEspecial = new FechaEspecialType();
        $form = $fechaEspecial->crearGrupo($datos, $numFecha, $em, $this)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $validar = new ValidarFechas();

            $valor = $validar->validarFechaVacioYPermitido($form, $datos, $this);

            if ($valor["isValid"]) {

                $errorList = $validar->validarFechaRepetido($form, $datos, true);

                if (count($errorList) == 0) {

                    $nFecha = (is_int($numFecha)) ? $numFecha : $numFecha->getNumeroFecha();

                    $fecha = $em->getRepository('futfunBundle:FechasBase')
                        ->armarFechas($form, $torneo, "crear");

                    $fecha->setTorneo($torneo);

                    $em->getRepository('futfunBundle:FechasBase')
                        ->grabarFechaIdaYVuelta($fecha, $datos);

                    $this->get('session')->getFlashBag()->add(
                        "mensaje", "La fecha " . $fecha->getNombreFecha() . " se grabo correctamente"
                    );

                    return $this->redirectToRoute('trabajo_dbbundle_torneos_comprobarFecha',
                        array("idTorneo" => $torneo->getId(), "numeroFecha" => $nFecha));

                } else {

                    foreach ($errorList as &$valor) {
                        if (isset($valor)) {
                            $errorMessage1 = new FormError($valor);
                            $form->get('fecha')->addError($errorMessage1);
                        }
                    }

                }

            } else {

                $errorList = $valor["errorList"];

                foreach ($errorList as &$valor) {
                    if (isset($valor[0])) {
                        $errorMessage1 = new FormError($valor[0]->getMessage());
                        $form->get('fecha')->addError($errorMessage1);
                    }
                }

            }

        }

        $cambiarUsuario = $this->createCustomForm(
            'POST', 'trabajo_dbbundle_torneos_cambiar_nombre_equipo',
            $torneo->getId(), (is_int($numFecha))
            ? $numFecha : $numFecha->getNumeroFecha(), "FECHA-ANT",
            "FECHA-NUEVA");

        return $this->render(
            'futfunBundle:ArmarFechasTorneo:fechasLiga.html.twig',
            array("fecha" => (is_int($numFecha))
                ? $numFecha : $numFecha->getNumeroFecha(),
                "datos" => $datos, "form" => $form->createView(),
                "grupos" => $posiciones,
                "cambiarUsuario" => $cambiarUsuario->createView()));

    }

    /**
     * Esta funcion se especifica en el formulario que va ir en el menu de apuestas para cuando
     * se presiona en la apuesta deseada te rediriga automaticamente
     * @param $id Es el id de apuesta que se eligio en el menu
     * @param $method Es el metodo que puede ser POST o GET
     * @param $route Es la ruta al que tiene que redirigir la vista
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createCustomForm($method, $route, $idTorneo,
                                      $numFecha, $nombreAnterior,
                                      $nombreNuevo)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($route,
                array('idTorneo' => $idTorneo,
                    'numeroFecha' => $numFecha,
                    'nombreAnterior' => $nombreAnterior,
                    'nombreNuevo' => $nombreNuevo)))
            ->setMethod($method)
            ->getForm();

        return $form;

    }

    /**
     * Esta funcion se especifica en obtener los datos faltantes y renderizar la vista de las
     * fechas de eliminacion
     * @param Request $request
     * @param $torneo Es el torneo que el usuario eligio
     * @param $numFecha Puede ser un integer con el numero de fecha o la entidad FechaBase
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function crearFechasEliminacionAction(Request $request, $torneo, $numFecha)
    {

        $servicio = new ServArmarFechas();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->obtenerDatosEliminacion(
            $torneo, $numFecha, $em, $this);

        $fechaEspecial = new FechaEspecialEliminacionType();

        $form = $fechaEspecial->crearGrupo($datos, $numFecha, $em, $this)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $validar = new ValidarFechas();

            $valor = $validar->validarFechaRepetido($form, $datos, false);

            if (count($valor) == 0) {

                $fecha = $em->getRepository('futfunBundle:FechasBase')
                    ->armarFechas($form, $torneo, "eliminacion");

                $fecha->setTorneo($torneo);

                $em->getRepository('futfunBundle:FechasBase')
                    ->grabarFechaIdaYVuelta($fecha, $datos);

                $this->get('session')->getFlashBag()->add(
                    "mensaje", "La fecha n� " . $fecha->getNombreFecha() . " se grabo correctamente"
                );

                return $this->redirectToRoute('trabajo_dbbundle_torneos_comprobarFecha',
                    array("idTorneo" => $torneo->getId(),
                        "numeroFecha" => $fecha->getNumeroFecha()));

            } else {

                $errorList = $valor;

                foreach ($errorList as &$valor) {
                    if (isset($valor)) {
                        $errorMessage1 = new FormError($valor);
                        $form->get('fecha')->addError($errorMessage1);
                    }
                }

            }

        }

        $cambiarUsuario = $this->createCustomForm(
            'POST', 'trabajo_dbbundle_torneos_cambiar_nombre_equipo',
            $torneo->getId(), (is_int($numFecha))
            ? $numFecha : $numFecha->getNumeroFecha(), "FECHA-ANT",
            "FECHA-NUEVA");

        return $this->render(
            'futfunBundle:ArmarFechasTorneo:fechasEliminatoria.html.twig',
            array("datos" => $datos, "form" => $form->createView(),
                "cambiarUsuario" => $cambiarUsuario->createView()));

    }

}