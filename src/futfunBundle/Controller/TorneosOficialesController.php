<?php

namespace futfunBundle\Controller;

use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TorneosOficialesController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los array para el menu de la zolapa torneos
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function devolverTorneosAction()
    {

        $lugar = $this->obtenerLugarDeAcceso();

        $repository = $this->getDoctrine()
            ->getRepository('futfunBundle:TorneoBase');
        $user = $this->getUser();

        $oficialProgreso = $repository->findByEstadoAndClase("En progreso", "oficial");
        $oficialFinalizado = $repository->findByEstadoAndClase("Finalizado", "oficial");
        $privadoProgreso = $repository->findByEstadoAndClase("En progreso", "privada");
        $privadoFinalizado = $repository->findByEstadoAndClase("Finalizado", "privada");

        $habActFecha = $this
            ->devolverArrayHabilitar(
                $oficialProgreso, $privadoProgreso, $user);

        return $this->render(
            'futfunBundle:ArmarTorneoBase:menu/torneosOficiales.html.twig',
            compact('oficialProgreso', 'oficialFinalizado',
                'privadoProgreso', 'privadoFinalizado', 'habActFecha',
                'lugar')
        );

    }

    /**
     * Esta funcion se especifica en obtener los array para el menu de la zolapa torneos
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cambiarNombreEquipoAction(Request $request,
                                              $idTorneo, $numeroFecha,
                                              $nombreAnterior, $nombreNuevo)
    {

        $em = $this->getDoctrine()->getManager();

        $torneo = $em->getRepository("futfunBundle:TorneoBase")
            ->findById($idTorneo);
        $clubNuevo = $em->getRepository("futfunBundle:Clubes")
            ->modificarYDevolverClub($nombreNuevo);
        $clubViejo = $em->getRepository("futfunBundle:Clubes")
            ->devolverClubXNombre($nombreAnterior);

        $tipoTorneo = $em->getRepository("futfunBundle:TipoTorneo")
            ->devolverTipoTorneo($torneo->getTipoTorneo());

        if ($tipoTorneo != "e") {

            $em->getRepository("futfunBundle:PosicionesLiga")
                ->actualizarEquipoModificado($torneo,
                    $clubViejo, $clubNuevo);

        }


        $em->getRepository("futfunBundle:PartidoFecha")
            ->actualizarEquipoModificado(
                $torneo, $clubViejo, $clubNuevo);

        $em->getRepository("futfunBundle:TorneoBase")
            ->actualizarEquipoModificado(
                $torneo, $clubViejo, $clubNuevo);

        $this->get('session')->getFlashBag()->add(
            "mensaje", "�Se ha modificado el equipo con exito!"
        );

        return $this->redirectToRoute(
            "trabajo_dbbundle_torneos_comprobarFecha", array(
            "idTorneo" => $idTorneo, "numeroFecha" => $numeroFecha));

    }

    private function devolverArrayHabilitar(
        $oficialProgreso, $privadoProgreso, $user)
    {

        $respuesta = [];
        $validar = new ValidarFechas();

        foreach ($oficialProgreso as &$torneoOficial) {

            $respuesta["oficial"][$torneoOficial->getId()] = $validar
                ->validarAdministrador($user, $torneoOficial);

        }

        foreach ($privadoProgreso as &$torneoPrivado) {

            $respuesta["privado"][$torneoPrivado->getId()] = $validar
                ->validarAdministrador($user, $torneoPrivado);

        }

        return $respuesta;
    }

    private function obtenerLugarDeAcceso()
    {

        $url = $_SERVER["REQUEST_URI"];
        $parte = strrchr($url, "/");
        $lugar = substr($parte, 1, strpos($parte, "?") - 1);

        $parte = substr($parte, strpos($parte, "idTorneo") + 9);

        $pos = strpos($parte, "&");
        if ($pos === false) {
            $idTorneo = substr($parte, 0);
        } else {
            $idTorneo = substr($parte, 0, strpos($parte, "&"));
        }

        return compact("idTorneo", "lugar");

    }


}
