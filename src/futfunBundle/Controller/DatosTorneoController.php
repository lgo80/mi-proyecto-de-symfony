<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use futfunBundle\Servicios\ServDatosTorneo;

class DatosTorneoController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para poder llamar
     * a la vista de los datos del torneoo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function datosTorneoAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');

        $em = $this->getDoctrine()->getManager();
        $servicio = new ServDatosTorneo();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarPrimera($idTorneo, $em, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["lugar"],
                $datos["respuesta"]["mensaje"]
            );

            return $this->redirectToRoute($datos["respuesta"]["lugar"]);
        }


        return $this->render('futfunBundle:DatosTorneo:datosTorneo.html.twig',
            array('datos' => $datos));
    }


}
