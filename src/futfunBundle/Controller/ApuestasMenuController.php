<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use futfunBundle\Validador\ValidarCompletarExtras;

class ApuestasMenuController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir al menu
     * de la zolapa apuesta
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function devolverApuestasAction()
    {

        $lugar = $this->obtenerLugarDeAcceso();

        $repository = $this->getDoctrine()
            ->getRepository('futfunBundle:ApuestaBase');

        $apuestaEnEspera = $repository
            ->devolverApuestasPorEstado(true, "En espera");
        $apuestaComenzado = $repository
            ->devolverApuestasPorEstado(true, "Comenzado");
        $apuestaFinalizado = $repository
            ->devolverApuestasPorEstado(true, "Finalizado");

        $deleteFormAjax = $this->createCustomForm(
            ':USER_ID', 'POST', 'trabajo_dbbundle_apuestas_agregar_usuario');

        $userLogueado = $this->getUser();
        $arrayParaLoguearse = $this->devolverArrayParaLoguearse(
            $apuestaEnEspera, $apuestaComenzado, $userLogueado);

        $arrayIsFechaCumplida = $this->devolverArrayIsFechaCumplida(
            $apuestaEnEspera, $apuestaComenzado);

        $this->verificarSiComenzoLaApuesta($apuestaEnEspera);

        return $this
            ->render('futfunBundle:ApuestasTorneo:menu/apuestasMenu.html.twig',
                array(
                    "apuestaEnEspera" => $apuestaEnEspera,
                    "apuestaComenzado" => $apuestaComenzado,
                    "apuestaFinalizado" => $apuestaFinalizado,
                    "delete_form_ajax" => $deleteFormAjax->createView(),
                    "arrayParaLoguearse" => $arrayParaLoguearse,
                    "arrayIsFechaCumplida" => $arrayIsFechaCumplida,
                    "lugar" => $lugar)
            );

    }

    /**
     * Esta funcion se especifica en el formulario que va ir en el menu de apuestas para cuando
     * se presiona en la apuesta deseada te rediriga automaticamente
     * @param $id Es el id de apuesta que se eligio en el menu
     * @param $method Es el metodo que puede ser POST o GET
     * @param $route Es la ruta al que tiene que redirigir la vista
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createCustomForm($id, $method, $route)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('idApuesta' => $id)))
            ->setMethod($method)
            ->getForm();

        return $form;

    }

    private function devolverArrayParaLoguearse(
        $apuestaEnEspera, $apuestaComenzado, $userLogueado)
    {

        $arrayRespuesta = [];

        $repository = $this->getDoctrine()
            ->getRepository('futfunBundle:ApuestasParticipantes');

        foreach ($apuestaEnEspera as &$apuesta) {

            $participante = $repository
                ->getByParticipant($apuesta, $userLogueado);

            $arrayRespuesta[$apuesta->getId()] = ($participante == null);

        }

        foreach ($apuestaComenzado as &$apuesta) {

            $participante = $repository
                ->getByParticipant($apuesta, $userLogueado);

            $arrayRespuesta[$apuesta->getId()] = ($participante == null);

        }

        return $arrayRespuesta;

    }

    private function verificarSiComenzoLaApuesta(
        $apuestaEnEspera)
    {

        $em = $this->getDoctrine()->getManager();

        $isModify = false;

        foreach ($apuestaEnEspera as &$apuesta) {

            $torneo = $apuesta->getTorneo();

            $fechaBase = $em->getRepository("futfunBundle:FechasBase")
                ->devolverFechaDelTorneo($torneo, 1);

            if ($fechaBase != null) {

                if ($apuesta->getFechaLimite() == "fecha" &&
                    $fechaBase->getFechaInicioAt() != null
                ) {

                    if (!$this->validarFechaLimite($fechaBase->getFechaInicioAt())) {

                        $isModify = true;

                    }

                } else {

                    foreach ($fechaBase->getGrupoFechas() as &$grupoFecha) {

                        foreach ($grupoFecha->getPartidoFechas() as &$partido) {

                            if ($partido->getValorLocal() != null &&
                                $partido->getValorVisita() != null
                            ) {

                                $isModify = true;
                                break;

                            }

                            if (!$this->validarFechaLimite(
                                $partido->getFechaPartidoAt())
                            ) {

                                $isModify = true;
                                break;

                            }

                        }

                    }

                }

                if ($isModify) {

                    $em
                        ->getRepository("futfunBundle:ApuestaBase")
                        ->cambiarEstado($apuesta, "Comenzado");

                }

            }

        }

    }

    private function validarFechaLimite($fechaEntrante)
    {

        if (!empty($fechaEntrante)) {

            $fechaEntrante->add(new \DateInterval('PT5H'));

            $ahora = new \DateTime();

            return ($fechaEntrante > $ahora);
        }

        return true;

    }

    private function devolverArrayIsFechaCumplida(
        $apuestaEnEspera, $apuestaComenzado)
    {

        $arrayRespuesta = [];
        $validar = new ValidarCompletarExtras();

        foreach ($apuestaEnEspera as &$apuesta) {

            $arrayRespuesta[$apuesta->getId()] =
                $validar->validarFechaNoCumplida(
                    $apuesta, $this->getDoctrine()->getManager());

        }

        foreach ($apuestaComenzado as &$apuesta) {

            $arrayRespuesta[$apuesta->getId()] =
                $validar->validarFechaNoCumplida(
                    $apuesta, $this->getDoctrine()->getManager());

        }

        return $arrayRespuesta;

    }

    private function obtenerLugarDeAcceso()
    {

        $url = $_SERVER["REQUEST_URI"];
        $parte = strrchr($url, "/");
        $lugar = substr($parte, 1, strpos($parte, "?") - 1);

        $parte = substr($parte, strpos($parte, "idApuesta") + 10);

        $pos = strpos($parte, "&");
        if ($pos === false) {
            $idApuesta = substr($parte, 0);
        } else {
            $idApuesta = substr($parte, 0, strpos($parte, "&"));
        }

        return compact("idApuesta", "lugar");

    }

}
