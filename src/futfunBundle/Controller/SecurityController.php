<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\RecuperarUsuario;
use futfunBundle\Form\RecuperarUsuarioType;
use futfunBundle\Servicios\ServEnvioEmail;
use futfunBundle\Servicios\ServFuncionesUtiles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\FormError;

class SecurityController extends Controller
{

    /**
     * Esta funcion se especifica en gestionar los datos para loguearse en el sistema
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {

        if (true === $this->get('security.context')->isGranted('ROLE_USER') ||
            true === $this->get('security.context')->isGranted('ROLE_ADMIN')
        ) {
            return $this->redirectToRoute("home1");
        }

        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        $user = $this->getUser();

        if ($user) {

            return $this->render(
                'futfunBundle:Administrador:vermensajes.html.twig');

        } else {

            $datos = array(
                "titulo" => $this->get('translator')->trans("Log in"),
                "nombreTorneoLugar" => "");

            return $this->render(
                'futfunBundle:Security:login.html.twig',
                array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error, "datos" => $datos
                )
            );

        }


    }

    public function ingresarEmailRUAction(Request $request)
    {

        if ($request->isMethod('POST')) {

            $datos = array(
                "titulo" => $this->get('translator')
                    ->trans("Enter e-mail or user to recover"),
                "nombreTorneoLugar" => "", "botonEditar" => "",
                "botonGuardar" => $this->get('translator')->trans("Recover user"));

            $form = $this->createForm(new RecuperarUsuarioType(), null);

            $form->handleRequest($request);

            if ($form->isValid()) {

                $usuarioIngresado = $form->get('usuario')->getData();
                $em = $this->getDoctrine()->getManager();

                $user = $em->getRepository('futfunBundle:User')
                    ->loadByUsername($usuarioIngresado);

                if ($user != null) {

                    $token = $em->getRepository('futfunBundle:RecuperarUsuario')
                        ->alta($user);

                    $servicio = new ServEnvioEmail();

                    ($user->getIsActive())
                        ? $servicio->mandarMailParaClave($user, $token)
                        : $servicio->mandarMailParaConfirmar($user, $token);

                    $texto = ($user->getIsActive())
                        ? utf8_encode("Se le ha enviado un e-mail para cambiar la contrase�a del usuario")
                        : utf8_encode("Se le ha enviado un e-mail para confirmar el usuario");
                    return $this->redirectToRoute("texto_recuperar_usuario",
                        array("texto" => $texto));

                } else {

                    $errorMessage1 = new FormError(utf8_encode("�El usuario o e-mail ingresado no existe!"));
                    $form->get('usuario')->addError($errorMessage1);

                }

            }

            return $this->render(
                'futfunBundle:Security:emailrecupararusuario.html.twig',
                array(
                    "datos" => $datos, "form" => $form->createView()
                )
            );

        }

        return $this->render(
            'futfunBundle:Administrador:vermensajes.html.twig'
        );

    }

    public function textoEmailRUAction(Request $request)
    {

        $texto = $request->query->get('texto');

        $datos = array(
            "titulo" => $this->get('translator')->trans("Recover user"),
            "nombreTorneoLugar" => "");

        return $this->render(
            'futfunBundle:Security:textorecupararusuario.html.twig',
            array(
                "datos" => $datos, "texto" => $texto
            )
        );

    }

    public function confirmarRUAction(Request $request)
    {

        $idUser = $request->query->get('ui');
        $token = $request->query->get('tkn');
        $motivo = $request->query->get('mtv');

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('futfunBundle:User')
            ->find($idUser);
        $recupero = $em->getRepository('futfunBundle:RecuperarUsuario')
            ->validarRecupero($user, $token);

        if ($recupero != null) {

            if ($motivo == "ac") {

                $user->setIsActive(true);
                $em->getRepository('futfunBundle:User')
                    ->grabarNuevo($user);

                $em->getRepository('futfunBundle:RecuperarUsuario')
                    ->modificar($user);

                return $this->redirectToRoute("login");

            } else {

                $session = $request->getSession();

                $session->set('tkn', $token);

                return $this->redirectToRoute(
                    "trabajo_dbbundle_modificar_clave", array("ui" => $idUser));

            }

        }

        return $this->redirectToRoute("home1");

    }

}