<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\Mensajes;
use futfunBundle\Entity\PizarraMensaje;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PizarraMensajesController extends Controller
{
    public function mostrarMensajesAction(Request $request)
    {

        $isHabilitarMsgApuesta = $this->isMensajeApuestas();

        $delete_form_ajax = $this
            ->createCustomForm("POST", "mandar_mensaje");
        $delete_form_ajax_2 = $this
            ->createCustomForm("POST", "recargar_mensaje");

        $em = $this->getDoctrine()->getManager();
        $mensajes = $this
            ->devolverMensajesPizarra(
                "public", null, $em);

        $mensajesArmados = $this
            ->armarPizarraMensajes($mensajes, 15);

        return $this->render('futfunBundle:PizarraMensajes:index.html.twig', array(
            "btnPizarra" => "public",
            "delete_form_ajax" => $delete_form_ajax->createView(),
            "delete_form_ajax_2" => $delete_form_ajax_2->createView(),
            "isHabilitarMsgApuesta" => $isHabilitarMsgApuesta,
            "mensajesArmados" => $mensajesArmados,
            "isMostrarMas" => (count($mensajes) > 15)
        ));

    }

    private function createCustomForm($method, $route)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($route))
            ->setMethod($method)
            ->getForm();

        return $form;

    }

    public function mandarMsgAction(Request $request)
    {

        $bandeja = $request->query->get('bandeja');
        $idapuesta = $request->query->get('idapuesta');
        $maximoMsg = $request->query->get('maximoMsg');
        $em = $this->getDoctrine()->getManager();

        $texto = $_POST["txtMensajePizarra"];

        $apuesta = (empty($idapuesta) || $idapuesta == null)
            ? null
            : $em
                ->getRepository("futfunBundle:ApuestaBase")
                ->find($idapuesta);

        $mensaje = new PizarraMensaje();

        $mensaje->setEmisor($this->getUser());
        $mensaje->setMensaje($texto);
        $mensaje->setTipo($bandeja);
        $mensaje->setPizarraApuesta($apuesta);

        $em
            ->getRepository("futfunBundle:PizarraMensaje")
            ->grabarNuevo($mensaje);

        $mensajes = $this
            ->devolverMensajesPizarra(
                $bandeja, $apuesta, $em);

        $mensajesArmados = $this
            ->armarPizarraMensajes($mensajes, $maximoMsg);

        return new Response(
            json_encode(array(
                'message' => $mensajesArmados,
                'isMostrarMas' => (count($mensajes) > $maximoMsg))),
            200,
            array('Content-Type' => 'application/json')
        );

    }

    public function recargarMsgAction(Request $request)
    {

        $bandeja = $request->query->get('bandeja');
        $idapuesta = $request->query->get('idapuesta');
        $maximoMsg = $request->query->get('maximoMsg');
        $em = $this->getDoctrine()->getManager();

        $apuesta = (empty($idapuesta) || $idapuesta == null)
            ? null
            : $em
                ->getRepository("futfunBundle:ApuestaBase")
                ->find($idapuesta);

        $mensajes = $this
            ->devolverMensajesPizarra(
                $bandeja, $apuesta, $em);

        $mensajesArmados = $this
            ->armarPizarraMensajes($mensajes, $maximoMsg);

        return new Response(
            json_encode(array(
                'message' => $mensajesArmados,
                'isMostrarMas' => (count($mensajes) > $maximoMsg))),
            200,
            array('Content-Type' => 'application/json')
        );

    }

    private function isMensajeApuestas()
    {

        $url = $_SERVER["REQUEST_URI"];
        $parte = strrchr($url, "/");

        if (strpos($parte, "idApuesta") === false) {

            return array("isValid" => false, "id" => 0);

        }

        $parte = substr($parte, strpos($parte, "idApuesta") + 10);

        $pos = strpos($parte, "&");
        if ($pos === false) {
            $idApuesta = substr($parte, 0);
        } else {
            $idApuesta = substr($parte, 0, strpos($parte, "&"));
        }

        $isUsuarioValido = $this
            ->isParticipante($idApuesta);

        return array(
            "isValid" => $isUsuarioValido,
            "id" => ($isUsuarioValido) ? $idApuesta : null
        );

    }

    private function devolverMensajesPizarra(
        $bandeja, $apuesta, $em
    )
    {

        $mensajesPizarra = ($bandeja == "public")
            ? $em
                ->getRepository("futfunBundle:PizarraMensaje")
                ->getByTipo($bandeja)
            : $em
                ->getRepository("futfunBundle:PizarraMensaje")
                ->getByApuesta($apuesta);

        return $mensajesPizarra;

    }

    private function armarPizarraMensajes(
        $mensajes, $maximoMsg)
    {

        $respuesta = "";

        $maximoFor = (count($mensajes) > $maximoMsg)
            ? $maximoMsg : count($mensajes);

        for ($i = $maximoFor - 1; $i >= 0; $i--) {

            $mensaje = $mensajes[$i];

            $date = $mensaje->getFechaAt();
            $usuario = ($mensaje->getEmisor() != null)
                ? $mensaje->getEmisor()->getUsername()
                : $this->get('translator')
                    ->trans("anonymous");

            $respuesta .=
                "<div class='pnlMensajeInterno'>
                    <span class='spnTituloPM'>
                        [" . date_format($date, 'd-m-Y H:i:s')
                . " - " . $usuario . "]
                    </span>
                    <span class='spnMensajePM'>"
                . $mensaje->getMensaje() . "
                    </span>
                <div />";

            if ($i == $maximoMsg) {
                break;
            }

        }

        return (empty($mensajes) || $mensajes == null)
            ? $this->get('translator')
                ->trans("There are no posts to display")
            : $respuesta;

    }

    private function isParticipante($idApuesta)
    {

        $userLogueado = $this->getUser();

        if ($userLogueado != null) {
            $em = $this->getDoctrine()->getManager();

            $apuesta = $em
                ->getRepository("futfunBundle:ApuestaBase")
                ->find($idApuesta);
            $participante = $em
                ->getRepository("futfunBundle:ApuestasParticipantes")
                ->getByParticipant($apuesta, $userLogueado);

            return ($participante != null);

        }

        return false;

    }

}
