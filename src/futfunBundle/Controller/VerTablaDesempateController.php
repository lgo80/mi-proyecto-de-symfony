<?php

namespace futfunBundle\Controller;

use futfunBundle\Servicios\ServDesempateTabla;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VerTablaDesempateController extends Controller
{
    public function verTablaDesempateAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');

        $servicio = new ServDesempateTabla();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->validarDatos(
            $idTorneo, $em, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            return $this->redirectToRoute($datos["respuesta"]["lugar"]);

        }

        return $this->render('futfunBundle:VerTablaDesempate:vertabladesempate.html.twig',
            array("datos" => $datos));

    }

}
