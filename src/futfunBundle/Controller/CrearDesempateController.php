<?php

namespace futfunBundle\Controller;

use futfunBundle\Form\DesempateDatosType;
use futfunBundle\Servicios\ServCrearDesempate;
use futfunBundle\Validador\ValidarCrearDesempate;
use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CrearDesempateController extends Controller
{
    public function crearDesempateAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');
        $em = $this->getDoctrine()->getManager();

        $servicio = new ServCrearDesempate();
        $userLogueado = $this->getUser();

        $datos = $servicio->devolverDatos(
            $idTorneo, $em, $userLogueado, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            if ($datos["respuesta"]["lugar"]
                == "trabajo_dbbundle_torneos_crear_desempate" ||
                $datos["respuesta"]["lugar"]
                == "trabajo_dbbundle_torneos_ver_datos_desempate"
            ) {
                return $this->redirectToRoute($datos["respuesta"]["lugar"], array("idTorneo" => $idTorneo));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }

        }

        $datosDesempate = $datos["desempateDatos"];

        $form = $this->createForm(new DesempateDatosType(),
            $datosDesempate);

        $form->handleRequest($request);

        if ($form->isValid() && $request->isMethod('POST')) {

            $validar = new ValidarCrearDesempate();

            $errorList = $validar->validar($form);

            if (count($errorList) == 0) {

                $em->getRepository("futfunBundle:DesempateDatos")
                    ->grabarNuevo($datosDesempate);

                $em->getRepository("futfunBundle:PosicionesDesempate")
                    ->procesarPosicionesDesempate($datosDesempate);

                if ($datosDesempate->getIsConfirm()) {

                    return $this->redirectToRoute("trabajo_dbbundle_torneos_crear_fechas_desempate",
                        array("idTorneo" => $datosDesempate->getTorneo()->getId()));

                }

            } else {

                foreach ($errorList as &$valor) {
                    $errorMessage1 = new FormError($valor);
                    $form->get('vueltaGrupo')->addError($errorMessage1);
                }


            }

        }

        return $this->render('futfunBundle:CrearDesempate:creardesempate.html.twig',
            array("datos" => $datos, "form" => $form->createView()
            ));
    }

    public function menuAction(Request $request, $idTorneo)
    {

        $em = $this->getDoctrine()->getManager();
        $lugar = null;
        $isAdministrador = null;

        $torneo = $em->getRepository('futfunBundle:TorneoBase')
            ->findById($idTorneo);

        $desempateDatos = $em->getRepository('futfunBundle:DesempateDatos')
            ->devolverXTorneo($torneo);

        if ($desempateDatos != null) {

            $lugar = $this->obtenerLugarDeAcceso();

            $validar = new ValidarFechas();

            $isAdministrador = $validar
                ->validarAdministrador(
                    $this->getUser(), $desempateDatos->getTorneo());

        }

        return $this->render(
            'futfunBundle:CrearDesempate:menu/desempateMenu.html.twig',
            array("desempate" => $desempateDatos, "lugar" => $lugar,
                "isAdministrador" => $isAdministrador['isValid'])
        );

    }

    private function obtenerLugarDeAcceso()
    {

        $url = $_SERVER["REQUEST_URI"];
        $parte = strrchr($url, "/");

        return substr($parte, 1, strpos($parte, "?") - 1);

    }

}
