<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use futfunBundle\Servicios\ServTorneoTabla;

class TorneoTablaController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos para mostrar la vista con las tablas de posiciones
     * de los equipos del torneo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verTablaAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');

        $servicio = new ServTorneoTabla();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->validarDatos($idTorneo, $em, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            return $this->redirectToRoute($datos["respuesta"]["lugar"]);


        }

        return $this->render('futfunBundle:TorneoTabla:verTablaTorneo.html.twig', array("datos" => $datos));

    }

}
