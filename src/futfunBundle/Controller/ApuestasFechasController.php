<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use futfunBundle\Form\ApuestaFechaEspecialType;
use futfunBundle\Servicios\ServArmarResultados;
use futfunBundle\Validador\ValidarApuestas;

class ApuestasFechasController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir a la vista
     * para que los usuarios completen los resultados de las fechas del torneo
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verFechasAction(Request $request)
    {

        $idApuesta = $request->query->get('idApuesta');
        $numeroFecha = $request->query->get('numeroFecha');
        $direccion = $request->query->get('direccion');

        $servicio = new ServArmarResultados();
        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarDatos(
            $idApuesta, $em, $userLogueado,
            $numeroFecha, $direccion, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"] == "trabajo_dbbundle_apuestas_ver_tabla") {
                return $this->redirectToRoute($datos["respuesta"]["lugar"], array("idApuesta" => $idApuesta));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }


        }

        $apuestaFechaEspecial = new ApuestaFechaEspecialType();
        $form = $apuestaFechaEspecial->crearGrupo($datos, $em, $this,
            $userLogueado)->getForm();
        $form->handleRequest($request);

        if ($form->isValid() && $datos["isModificar"][0]) {

            $validar = new ValidarApuestas();
            $errores = $validar->devolverErroresForm(
                $datos["fechaBase"], $datos["isModificar"], $form, $this);

            if ($errores[0]["isGrabar"]) {

                $em->getRepository('futfunBundle:ApuestasFechas')
                    ->grabarResultadosFecha($datos, $errores);

            }

            if (!$errores[0]["isGrabar"]) {

                $this->get('session')->getFlashBag()->add(
                    "mensaje-danger", $errores[0]["mensaje"]
                );


            } elseif ($errores[0]["isError"]) {

                $this->get('session')->getFlashBag()->add(
                    "mensaje-warning", $errores[0]["mensaje"]
                );

                for ($i = 1; $i < count($errores); $i++) {

                    $errorList = $errores[$i];

                    if ($errorList["isError"]) {

                        $this->get('session')->getFlashBag()->add(
                            'apuestaValor' . $i, $errorList["mensaje"]
                        );
                    }

                }

            } else {

                $this->get('session')->getFlashBag()->add(
                    "mensaje-sucess", $errores[0]["mensaje"]
                );

            }

        }


        return $this->render('futfunBundle:ApuestasFechas:completarFechaApuesta.html.twig',
            array("datos" => $datos, "form" => $form->createView()));
    }

}
