<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use futfunBundle\Servicios\ServApuestaTabla;

class ApuestasTablaController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir a la vista
     * para mostrar la tabla de los participante del torneo de apuesta
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verTablaAction(Request $request)
    {

        $idApuesta = $request->query->get('idApuesta');
        $filtro = $request->query->get('filtro');
        $numFecha = $request->query->get('numFecha');

        $servicio = new ServApuestaTabla();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->validarDatos(
            $idApuesta, $em, $this, $filtro, $numFecha);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            return $this->redirectToRoute($datos["respuesta"]["lugar"]);


        }

        return $this->render('futfunBundle:ApuestasTabla:verTablaApuesta.html.twig', array("datos" => $datos));

    }

}
