<?php

namespace futfunBundle\Controller;

use futfunBundle\Servicios\ServDatosApuesta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DatosApuestaController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir a la vista
     * para ver los datos de una apuesta
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function datosApuestaAction(Request $request)
    {

        $idTorneo = $request->query->get('idApuesta');

        $em = $this->getDoctrine()->getManager();
        $servicio = new ServDatosApuesta();

        $datos = $servicio->validarPrimera(
            $idTorneo, $em, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["lugar"],
                $datos["respuesta"]["mensaje"]
            );

            return $this->redirectToRoute($datos["respuesta"]["lugar"]);
        }

        return $this->render('futfunBundle:DatosApuesta:datosApuesta.html.twig', array(
            "datos" => $datos
        ));

    }

}
