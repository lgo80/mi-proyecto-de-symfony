<?php

namespace futfunBundle\Controller;

use futfunBundle\Validador\ValidarFechas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use futfunBundle\Validador\ValidarCompletarExtras;

class AccesosController extends Controller
{
    public function accesosAction($id)
    {

        $isAnotarse = "";
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $respuesta = array("isValid" => false);
        $deleteFormAjax = null;
        $isFechaCumplida = null;
        $lugar = $this->obtenerLugarDeAcceso();
        $validar = new ValidarFechas();
        $desempate = null;
        $apuestasDelTorneo = null;

        if ($lugar == "verdatostorneo" || $lugar == "crearfecha" ||
            $lugar == "completarresultadostorneo" || $lugar == "vertablatorneo"
        ) {

            $torneo = $em
                ->getRepository('futfunBundle:TorneoBase')
                ->find($id);

            $respuesta = $validar
                ->validarAdministrador($user, $torneo);

            $vista = 'futfunBundle:ArmarTorneoBase:menu/accesosTorneo.html.twig';

            $desempate = $em
                ->getRepository('futfunBundle:DesempateDatos')
                ->devolverXTorneo($torneo);

            $apuestasDelTorneo = $this
                ->devolverArrayApuestas($torneo, $em, $user);

            $deleteFormAjax = $this->createCustomForm(
                ':USER_ID', 'POST', 'trabajo_dbbundle_apuestas_agregar_usuario');

        } elseif ($lugar == "creardesempate" || $lugar == "verdatosdesempate" ||
            $lugar == "crearfechasdesempate" || $lugar == "vertabladesempate" ||
            $lugar == "completarresultadosdesempate"
        ) {

            $torneo = $em
                ->getRepository('futfunBundle:DesempateDatos')
                ->findByTorneo($id);

            $torneo = $torneo[0];

            $respuesta = $validar
                ->validarAdministrador($user, $torneo->getTorneo());

            $vista = 'futfunBundle:CrearDesempate:menu/accesosDesempate.html.twig';

        } else {

            $validar = new ValidarCompletarExtras();
            $torneo = $em
                ->getRepository('futfunBundle:ApuestaBase')
                ->find($id);

            $vista = 'futfunBundle:ApuestasTorneo:menu/accesosApuestas.html.twig';

            $isAnotarse = $this
                ->verificarAnotarse($user, $torneo);

            $respuesta = $validar
                ->validarAdministrador($user, $torneo->getTorneo());

            $deleteFormAjax = $this->createCustomForm(
                ':USER_ID', 'POST', 'trabajo_dbbundle_apuestas_agregar_usuario');

            $isFechaCumplida = $validar->validarFechaNoCumplida(
                $torneo, $em);

            $desempate = $em
                ->getRepository('futfunBundle:DesempateDatos')
                ->devolverXTorneo($torneo->getTorneo());

        }

        return $this->render($vista, array(
                "torneo" => $torneo,
                "isAdministrador" => $respuesta["isValid"],
                "desempate" => $desempate, "isAnotarse" => $isAnotarse,
                "lugar" => $lugar,
                "delete_form_ajax" => ($deleteFormAjax != null)
                    ? $deleteFormAjax->createView() : null,
                "isFechaCumplida" => $isFechaCumplida,
                "apuestasDelTorneo" => $apuestasDelTorneo)
        );

    }

    private function verificarAnotarse(
        $userLogueado, $apuesta)
    {

        $repository = $this->getDoctrine()
            ->getRepository('futfunBundle:ApuestasParticipantes');

        $participante = $repository
            ->getByParticipant($apuesta, $userLogueado);

        return ($participante == null);

    }

    private function obtenerLugarDeAcceso()
    {

        $url = $_SERVER["REQUEST_URI"];
        $parte = strrchr($url, "/");
        return substr($parte, 1, strpos($parte, "?") - 1);

    }

    /**
     * Esta funcion se especifica en el formulario que va ir en el menu de apuestas para cuando
     * se presiona en la apuesta deseada te rediriga automaticamente
     * @param $id Es el id de apuesta que se eligio en el menu
     * @param $method Es el metodo que puede ser POST o GET
     * @param $route Es la ruta al que tiene que redirigir la vista
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createCustomForm($id, $method, $route)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('idApuesta' => $id)))
            ->setMethod($method)
            ->getForm();

        return $form;

    }

    private function devolverArrayApuestas(
        $torneo, $em, $user)
    {

        $apuestasDelTorneo = $em
            ->getRepository('futfunBundle:ApuestaBase')
            ->devolverApuestasPorTorneo($torneo);

        $validar = new ValidarCompletarExtras();
        $arrayRespuesta = [];

        foreach ($apuestasDelTorneo as &$apuesta) {

            $isAnotarse = $this
                ->verificarAnotarse($user, $apuesta);

            $isFechaCumplida = $validar
                ->validarFechaNoCumplida(
                    $apuesta, $em);

            $arrayRespuesta[] = array(
                "apuesta" => $apuesta,
                "isAnotarse" => $isAnotarse,
                "isFechaCumplida" => $isFechaCumplida
            );
        }

        return $arrayRespuesta;

    }

}
