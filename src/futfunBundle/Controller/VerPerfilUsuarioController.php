<?php

namespace futfunBundle\Controller;

use futfunBundle\Servicios\ServPerfilUsuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VerPerfilUsuarioController extends Controller
{
    public function verPerfilUsuarioAction(Request $request)
    {

        $servicio = new ServPerfilUsuario();
        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $datos = $servicio->obtenerDatos(
            $userLogueado, $em, $this);


        return $this->render('futfunBundle:VerPerfilUsuario:verperfilusu.html.twig',
            compact("datos"));
    }

}
