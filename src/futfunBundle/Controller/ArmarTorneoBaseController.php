<?php

namespace futfunBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use futfunBundle\Entity\AdministradoresTorneos;
use futfunBundle\Entity\DatosTorneo;
use futfunBundle\Form\DatosTorneoType;
use futfunBundle\Validador\Constraints as AcmeAssert;
use futfunBundle\Entity\TorneoBase;
use futfunBundle\Entity\User;
use futfunBundle\Form\TorneoBaseType;
use Symfony\Component\Form\FormError;
use futfunBundle\Validador\ValidarFechas;
use futfunBundle\Validador\ValidarTorneo;

class ArmarTorneoBaseController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos para generar la vista necesaria para mostrarsela al
     * usuario
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function crearTorneoAction(Request $request)
    {

        $torneo = new TorneoBase();

        $form = $this->createForm(new TorneoBaseType(), $torneo);

        $form->handleRequest($request);

        if ($form->isValid() && $request->isMethod('POST')) {

            $validar = new ValidarTorneo();

            $errorList = $validar->validar($form, $this);

            if (count($errorList) == 0) {

                $em = $this->getDoctrine()->getManager();

                $user = $this->getUser();

                $user1 = $this->getDoctrine()->getRepository("futfunBundle:User")
                    ->findOneByUsername($user->getUsername());

                $torneo->setClase(($user1->getRole()->getNombre()
                    == "ROLE_ADMIN") ? "oficial" : "privada");

                $administrador = $this->generarAdministrador($user1, $torneo);

                $torneo->addAdministradore($administrador);

                $em->persist($torneo);
                $em->flush();

                return $this->redirectToRoute(
                    "trabajo_dbbundle_torneos_comprobarFecha",
                    array("idTorneo" => $torneo->getId()));

            } else {

                foreach ($errorList as &$valor) {
                    $errorMessage1 = new FormError($valor);
                    $form->get('dato')->addError($errorMessage1);
                }


            }

        }

        $datos = array(
            "titulo" => $this->get('translator')->trans("Create new tournament"),
            "subtitulo" => $this->get('translator')->trans("Complete data"),
            "nombreTorneoLugar" => "", "botonEditar" => "",
            "botonGuardar" => $this->get('translator')->trans("Create Tournament"));

        return $this->render('futfunBundle:ArmarTorneoBase:crearTorneo.html.twig', array(
            "form" => $form->createView(), "datos" => $datos));
    }

    /**
     * Esta funcion se especifica en generar un administrador del torneo para despues devolverlo
     * @param User $user
     * @param TorneoBase $torneo
     * @return AdministradoresTorneos Devuelve la entidad AdministradoresTorneos
     * generada por los parametros ingresados
     */
    public function generarAdministrador(User $user, TorneoBase $torneo)
    {

        $administrador = new AdministradoresTorneos();

        $administrador->setTorneo($torneo);

        $administrador->setUser($user);

        $rol = $this->getDoctrine()->getRepository("futfunBundle:Roles")
            ->findOneByNombre("ROLE_ADMIN");

        $administrador->setRole($rol);

        return $administrador;
    }
}
