<?php

namespace futfunBundle\Controller;

use futfunBundle\Servicios\ServVerApuestas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApuestasVerFechasController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir a la vista
     * para ver las apuestas de los demas usuarios que se puedan mostrar
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verFechasAction(Request $request)
    {

        $idApuesta = $request->query->get('idApuesta');
        $numeroFecha = $request->query->get('numeroFecha');
        $direccion = $request->query->get('direccion');
        $usuarioEntrante = $request->query->get('usuario');
        $usuarioLogueado = $this->getUser();

        $servicio = new ServVerApuestas();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->validarDatos(
            $idApuesta, $em, $usuarioEntrante, $numeroFecha,
            $direccion, $usuarioLogueado, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"] == "trabajo_dbbundle_apuestas_ver_tabla") {
                return $this->redirectToRoute($datos["respuesta"]["lugar"], array("idApuesta" => $idApuesta));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }


        }

        return $this->render(
            'futfunBundle:ApuestasVerFechas:verFechaApuesta.html.twig',
            array("datos" => $datos));

    }

}
