<?php

namespace futfunBundle\Controller;

use futfunBundle\Form\DesempateDatosType;
use futfunBundle\Servicios\ServCrearDesempate;
use futfunBundle\Servicios\ServVerDatosDesempate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VerDatosDesempateController extends Controller
{
    public function verDatosDesempateAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');
        $em = $this->getDoctrine()->getEntityManager();

        $servicio = new ServVerDatosDesempate();

        $datos = $servicio->devolverDatos(
            $idTorneo, $em, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            if ($datos["respuesta"]["lugar"] == "trabajo_dbbundle_torneos_ver_tabla") {
                return $this->redirectToRoute($datos["respuesta"]["lugar"], array("idTorneo" => $idTorneo));
            } else {
                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }

        }

        return $this->render('futfunBundle:VerDatosDesempate:verdatosdesempate.html.twig',
            array("datos" => $datos));
    }

}
