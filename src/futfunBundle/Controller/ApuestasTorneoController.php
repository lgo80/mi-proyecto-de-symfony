<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\AdministradoresApuestas;
use futfunBundle\Entity\ApuestasExtras;
use futfunBundle\Servicios\ServFuncionesUtiles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use futfunBundle\Entity\ApuestasParticipantes;
use futfunBundle\Entity\User;
use futfunBundle\Entity\ApuestaBase;
use futfunBundle\Form\TorneoApuestaEspecialType;

class ApuestasTorneoController extends Controller
{

    /**
     * Esta funcion se especifica en obtener los datos necesarios para mandar y redirigir a la vista
     * para crear una apuesta nueva
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function crearApuestaAction(Request $request)
    {

        $apuestas = new TorneoApuestaEspecialType();

        $form = $apuestas->crearGrupo($this)->getForm();
        $form->handleRequest($request);

        if ($form->isValid() && $request->isMethod('POST')) {

            $apuesta = $form->get('apuesta')->getData();

            $apuesta->setFechaLimiteExtraAt(
                $this->obtenerFechaExtras(
                    $apuesta->getFechaLimiteExtraAt(), $apuesta->getTorneo()));

            $apuesta = $this->agregarExtras($form, $apuesta);

            $user = $this->getUser();

            $user1 = $this->getDoctrine()
                ->getRepository("futfunBundle:User")
                ->findOneByUsername(
                    $user->getUsername());

            $administrador = $this
                ->generarAdministrador($user1, $apuesta);

            $apuesta->addAdministradore($administrador);

            $em = $this->getDoctrine()->getManager();
            $em->persist($apuesta);
            $em->flush();

            return $this->redirectToRoute(
                "trabajo_dbbundle_apuestas_ver_tabla",
                array("idApuesta" => $apuesta->getId()));

        }

        $datos = array(
            "titulo" => $this->get('translator')->trans("Create bet New"),
            "nombreTorneoLugar" => "", "botonEditar" => "",
            "botonGuardar" => $this->get('translator')
                ->trans("Save bet"));

        return $this->render(
            'futfunBundle:ApuestasTorneo:crearapuesta.html.twig',
            array("form" => $form->createView(), "datos" => $datos));

    }

    /**
     * Esta funcion se especifica en agregar los extras que el usuario creador de la apuesta ha decidido
     * que se completen
     * @param $form Es el formulario que el usuario creador completo a su gusto
     * @param $apuesta Es la entidad apuesta
     * @return mixed Devuelve la entidad apuesta con los extras ingresados
     */
    private function agregarExtras($form, $apuesta)
    {

        for ($i = 1; $i <= 10; $i++) {

            $extra = $form->get('extra' . $i)
                ->getData();
            $isComplete = $form->get('isComplete' . $i)
                ->getData();

            if ($extra->getDetalle() != "") {

                if ($extra->getDetalle()
                    == "Clasificados del grupo"
                ) {

                    $apuesta = $this
                        ->generarExtrasPorClasificadosGruopos(
                            $apuesta, $extra->getDescripcion(),
                            $extra->getValor());

                } elseif ($extra->getDetalle()
                    == "Orden de clasificados del grupo"
                ) {

                    $apuesta = $this
                        ->generarExtrasPorOrdenClasificadosGruopos(
                            $apuesta, $extra->getDescripcion(),
                            $extra->getValor());

                } else {

                    $extra->setBase($apuesta);
                    $extra->setIsComplete(($isComplete == "SI"));
                    $apuesta->addExtra($extra);

                }

            } else {

                break;

            }

        }

        return $apuesta;

    }

    /**
     * Esta funcion se especifica en agregar un usuario a la apuesta creada y redirige la ruta a la
     * vista de la tabla de las apuesta
     * @param Request $request
     * @param $idApuesta Es la entidad apuesta que se quiere anotar el usuario nuevo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function agregarUsuarioAction(Request $request, $idApuesta)
    {

        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $apuestaBase = $em
            ->getRepository("futfunBundle:ApuestaBase")
            ->find($idApuesta);

        if (!$apuestaBase) {
            $messageException = $this->get('translator')->trans("User not found");
            throw $this->createNotFoundException($messageException);
        }

        $participanteLogueado = $em
            ->getRepository("futfunBundle:ApuestasParticipantes")
            ->getByParticipant($apuestaBase, $userLogueado);

        if ($participanteLogueado != null) {

            $this->addFlash('mensaje-danger', 'No se puede anotar el usuario ya esta ingresado!');
            return $this->redirectToRoute(
                "trabajo_dbbundle_apuestas_ver_tabla",
                array("idApuesta" => $idApuesta));
        }

        $participante = new ApuestasParticipantes();
        $participante->setApuestasBase($apuestaBase);
        $participante->setUser($userLogueado);


        $em->persist($participante);
        $em->flush();

        $this->addFlash('mensaje-sucess',
            'Te has anotado en el torneo exitosamente!');

        return $this->redirectToRoute("trabajo_dbbundle_apuestas_ver_tabla",
            array("idApuesta" => $idApuesta));


    }

    /**
     * Esta funcion se especifica en generar la entidad AdministradoresApuestas con el usuario
     * que creo la apuesta
     * @param User $user Es el usuario que creo la apuesta
     * @param ApuestaBase $apuesta Es la apuesta que se le quiere ingresar el administrador
     * @return AdministradoresApuestas Devuelve la entidad AdministradoresApuestas con el usuario ya agregado
     */
    private function generarAdministrador(User $user, ApuestaBase $apuesta)
    {

        $administrador = new AdministradoresApuestas();

        $administrador->setApuesta($apuesta);

        $administrador->setUser($user);

        $rol = $this->getDoctrine()
            ->getRepository("futfunBundle:Roles")
            ->findOneByNombre("ROLE_ADMIN");

        $administrador->setRole($rol);

        return $administrador;
    }

    private function generarExtrasPorClasificadosGruopos(
        ApuestaBase $apuesta, $descripcion, $valorExtra)
    {

        $datos = $apuesta->getTorneo()->getDato();

        $fechasBase = $this->getDoctrine()
            ->getRepository("futfunBundle:FechasBase")
            ->devolverFechaDelTorneo($apuesta->getTorneo(), 1);

        foreach ($fechasBase->getGrupoFechas() as &$valor) {

            for ($i = 1; $i <= $datos->getClasPorGrupo(); $i++) {

                $extra = new ApuestasExtras();

                $extra->setDetalle(utf8_encode($i . "� del grupo "
                    . $valor->getGrupo()->getNombre()));
                $extra->setDescripcion($descripcion);
                $extra->setValor($valorExtra);
                $extra->setBase($apuesta);
                $extra->setIsComplete(true);
                $apuesta->addExtra($extra);

            }

        }

        for ($i = 1; $i <= $datos->getClasAdic(); $i++) {

            $extra = new ApuestasExtras();

            $extra->setDetalle(utf8_encode(
                "Clasificado n� " . $i));
            $extra->setDescripcion($descripcion);
            $extra->setValor($valorExtra);
            $extra->setBase($apuesta);
            $extra->setIsComplete(true);
            $apuesta->addExtra($extra);

        }

        return $apuesta;


    }

    private function generarExtrasPorOrdenClasificadosGruopos(
        ApuestaBase $apuesta, $descripcion, $valorExtra)
    {

        $fechasBase = $this->getDoctrine()
            ->getRepository("futfunBundle:FechasBase")
            ->devolverFechaDelTorneo($apuesta->getTorneo(), 1);

        foreach ($fechasBase->getGrupoFechas() as &$valor) {

            $extra = new ApuestasExtras();

            $extra->setDetalle("Extra por orden de clasificado del grupo "
                . $valor->getGrupo()->getNombre());
            $extra->setDescripcion($descripcion);
            $extra->setValor($valorExtra);
            $extra->setBase($apuesta);
            $extra->setIsComplete(false);
            $apuesta->addExtra($extra);

        }

        return $apuesta;

    }

    private function obtenerFechaExtras($fecha, $torneo)
    {

        if (empty($fecha)) {

            $em = $this->getDoctrine()->getManager();

            $fechaBase = $em
                ->getRepository("futfunBundle:FechasBase")
                ->devolverFechaDelTorneo($torneo, 1);

            if ($fechaBase != null
                && $fechaBase->getFechaInicioAt() != null
            ) {

                return $fechaBase->getFechaInicioAt();

            }

        } else {

            $servicio = new ServFuncionesUtiles();

            return $servicio->armarFechaYHora(
                $fecha, null);

        }

        return null;

    }
}
