<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\RecuperarUsuario;
use futfunBundle\Form\UserClaveEspecialType;
use futfunBundle\Servicios\ServEnvioEmail;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use futfunBundle\Entity\User;
use futfunBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

class RegistrarseController extends Controller
{

    /**
     * Esta funcion se especifica en preparar los datos para el registro de usuarios nuevos
     * y los graba en el sistema
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registrarseAction(Request $request)
    {

        if (true === $this->get('security.context')->isGranted('ROLE_USER') ||
            true === $this->get('security.context')->isGranted('ROLE_ADMIN')
        ) {
            return $this->redirectToRoute("home1");
        }

        $u = new User();
        $form = $this->createForm(new UserType(), $u);

        if (isset($lala)) {

        }

        $form->handleRequest($request);

        if ($form->isValid()) {

            $repository = $this->getDoctrine()
                ->getRepository("futfunBundle:Roles");

            $rol = $repository->findOneByNombre("ROLE_USER");

            $u->setRole($rol);

            $factory = $this->get('security.encoder_factory');

            $encoder = $factory->getEncoder($u);
            $password = $encoder->encodePassword(
                $u->getPassword(), $u->getSalt());
            $u->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->getRepository('futfunBundle:User')
                ->grabarNuevo($u);

            $token = $em->getRepository('futfunBundle:RecuperarUsuario')
                ->alta($u);

            $servicio = new ServEnvioEmail();

            $servicio->mandarMailParaConfirmar($u, $token);

            $texto = utf8_encode(
                "Se le ha enviado un e-mail para confirmar el usuario");
            return $this->redirectToRoute("texto_recuperar_usuario",
                array("texto" => $texto));

        }

        $datos = array(
            "titulo" => $this->get('translator')->trans("Check in"),
            "nombreTorneoLugar" => "", "botonEditar" => "",
            "botonGuardar" => $this->get('translator')->trans("Check in"));

        return $this->render('futfunBundle:Registrarse:registrarse.html.twig',
            array("form" => $form->createView(), "datos" => $datos));

    }

    public function modificarClaveUAction(
        Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        $isValido = true;
        $userLogueado = $this->getUser();

        if ($userLogueado == null) {

            $idUser = $request->query->get('ui');
            $user = $em->getRepository('futfunBundle:User')
                ->find($idUser);

        }

        $fechaEspecial = new UserClaveEspecialType();
        $form = $fechaEspecial->crearGrupo(
            $userLogueado, $this)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($userLogueado != null) {

                $claveVieja = $form['passwordActual']->getData();

                $factory = $this->get('security.encoder_factory');

                $encoder = $factory->getEncoder($userLogueado);
                $passwordVieja = $encoder->encodePassword(
                    $claveVieja, $userLogueado->getSalt());

                if ($passwordVieja != $userLogueado->getPassword()) {

                    $errorMessage1 = new FormError(
                        "Debe ingresar la clave correcta actual");
                    $form->get('passwordActual')->addError($errorMessage1);

                    $isValido = false;

                } else {

                    $user = $userLogueado;

                }

            } else {

                $session = $request->getSession();
                $token = $session->get('tkn');

                $recupero = $em
                    ->getRepository("futfunBundle:RecuperarUsuario")
                    ->findByUser($user);

                if ($recupero[0]->getToken() != $token) {

                    $this->get('session')->getFlashBag()->add(
                        "login", "La clave no coincide con la enviada al e-mail"
                    );
                    return $this->redirectToRoute("home1");

                }

            }

            if ($isValido) {

                $factory = $this->get('security.encoder_factory');

                $claveNueva = $form['password']['first']->getData();

                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword(
                    $claveNueva, $user->getSalt());
                $user->setPassword($password);

                $em->getRepository("futfunBundle:User")
                    ->grabarNuevo($user);

                return ($userLogueado != null)
                    ? $this->redirectToRoute("home1")
                    : $this->redirectToRoute("login");

            }

        }

        $datos = array(
            "titulo" => $this->get('translator')->trans(utf8_decode('Change password')),
            "nombreTorneoLugar" => "", "botonEditar" => "",
            "botonGuardar" => $this->get('translator')->trans(utf8_decode('Change password')));

        return $this->render('futfunBundle:Registrarse:modificarclave.html.twig',
            array("form" => $form->createView(), "datos" => $datos));

    }
}
