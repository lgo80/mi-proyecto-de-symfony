<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\Roles;
use futfunBundle\Entity\TipoTorneo;
use futfunBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function arranqueAction()
    {

        $this->grabarRoles();

        $repository = $this->getDoctrine()
            ->getRepository("futfunBundle:Roles");

        $rolAdmin = $repository->findOneByNombre("ROLE_ADMIN");

        $this->grabarAdministrador($rolAdmin);

        $this->grabarTipoTorneos();

        return $this->redirectToRoute("home1");
    }

    private function grabarRoles()
    {

        $repository = $this->getDoctrine()
            ->getRepository("futfunBundle:Roles");

        $nuevoRol = new Roles();
        $nuevoRol->setNombre("ROLE_ADMIN");

        $repository->alta($nuevoRol);

        $nuevoRol2 = new Roles();
        $nuevoRol2->setNombre("ROLE_USER");

        $repository->alta($nuevoRol2);

    }

    private function grabarAdministrador($rol)
    {

        $nuevoAdmin = new User();

        $nuevoAdmin->setEmail("lgo80@yahoo.com.ar");
        $nuevoAdmin->setIsActive(true);
        $nuevoAdmin->setRole($rol);
        $nuevoAdmin->setUsername("lgo80");

        $factory = $this->get('security.encoder_factory');

        $encoder = $factory->getEncoder($nuevoAdmin);
        $password = $encoder->encodePassword(
            "boca2018", $nuevoAdmin->getSalt());
        $nuevoAdmin->setPassword($password);

        $em = $this->getDoctrine()->getManager();
        $em->persist($nuevoAdmin);
        $em->flush();


    }

    private function grabarTipoTorneos()
    {

        $nuevoTipo = new TipoTorneo();
        $nuevoTipo->setNombre("Liga");

        $em = $this->getDoctrine()->getManager();
        $em->persist($nuevoTipo);
        $em->flush();

        $nuevoTipo = new TipoTorneo();
        $nuevoTipo->setNombre(utf8_encode("Liga y Eliminación"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($nuevoTipo);
        $em->flush();

        $nuevoTipo = new TipoTorneo();
        $nuevoTipo->setNombre(utf8_encode("Eliminación"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($nuevoTipo);
        $em->flush();
    }

    public function redirectionAction()
    {

        $ahora = new \DateTime();
        exit("hora: " . $ahora->format('Y-m-d H:i:s'));
        return $this->render('futfunBundle:Default:formMail.html.twig');

    }

    public function sendMailAction(Request $request)
    {

        $message = (new \Swift_Message('Hello Emails'))
            ->setFrom('leolma1980@gmail.com')
            ->setTo('lgo80@yahoo.com.ar')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/registration.html.twig',
                    array('name' => "hplkl")
                ),
                'text/html'
            )/*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;

        $this->get('mailer')->send($message);
        return $this->render('futfunBundle:Default:formMail.html.twig');

    }

    public function pruebaajaxAction(
        Request $request
    )
    {
        $deleteFormAjax = $this->createCustomForm(':USER_ID', 'POST', 'prueba_ajax_2');

        return $this->render('futfunBundle:Default:index.html.twig', array(
            "delete_form_ajax" => $deleteFormAjax->createView()
        ));
    }

    public function pruebaajax2Action(
        Request $request
    )
    {


        if ($request->isXMLHttpRequest()) {

            return new Response(
                "");
        }

        return $this->render('futfunBundle:Default:index.html.twig');
    }

    private function createCustomForm($id, $method, $route)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route))
            ->setMethod($method)
            ->getForm();
    }
}
