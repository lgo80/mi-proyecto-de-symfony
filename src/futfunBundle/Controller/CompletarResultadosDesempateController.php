<?php

namespace futfunBundle\Controller;

use futfunBundle\Form\ResultadosFechaDesempateType;
use futfunBundle\Servicios\ServDesempateResultados;
use futfunBundle\Validador\ValidarDesempateResultados;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use futfunBundle\Form\ResultadosFechaTorneoType;
use futfunBundle\Servicios\ServTorneoResultados;
use futfunBundle\Validador\ValidarTorneoResultados;

class CompletarResultadosDesempateController extends Controller
{

    /**
     * Esta funcion se especifica en preparar los datos para la vista de completar los resultados reales
     * de los partidos y grabarlos en el sistema
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function completarResultadosAction(Request $request)
    {

        $idTorneo = $request->query->get('idTorneo');
        $numeroFecha = $request->query->get('numeroFecha');
        $direccion = $request->query->get('direccion');

        $servicio = new ServDesempateResultados();
        $em = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();

        $datos = $servicio->validarDatos($idTorneo, $em, $userLogueado,
            $numeroFecha, $direccion, $this);

        if ($datos["respuesta"]["isValid"] == 0) {

            $this->get('session')->getFlashBag()->add(
                $datos["respuesta"]["nombreError"],
                $datos["respuesta"]["mensaje"]
            );

            if ($datos["respuesta"]["lugar"] == "trabajo_dbbundle_torneos_crear_fechas_desempate") {

                return $this->redirectToRoute($datos["respuesta"]["lugar"],
                    array("idTorneo" => $idTorneo, "numeroFecha" =>
                        ($numeroFecha != "") ? $numeroFecha : 1));

            } else if ($datos["respuesta"]["lugar"] == "trabajo_dbbundle_torneos_ver_datos_desempate") {

                return $this->redirectToRoute($datos["respuesta"]["lugar"],
                    array("idTorneo" => $idTorneo));

            } else {

                return $this->redirectToRoute($datos["respuesta"]["lugar"]);
            }


        }

        $apuestaFechaEspecial = new ResultadosFechaDesempateType();
        $form = $apuestaFechaEspecial->crearGrupo($datos, $this)->getForm();
        $form->handleRequest($request);

        $ganador = $this->devolverStringGanador($datos["definicion"], $form,
            $datos["fechaDesempate"], $datos["fechaAnterior"]);

        if ($form->isValid()) {

            $validar = new ValidarDesempateResultados();

            $valor = $validar->validarVacioDeAmbosResult(
                $datos["fechaDesempate"], $form);

            if ($valor) {

                $fechaDesempate = $servicio->armarFechaConResultados(
                    $datos["fechaDesempate"], $form);

                $em->getRepository('futfunBundle:DesempateFecha')
                    ->grabarNuevo($fechaDesempate);

                $em->getRepository('futfunBundle:PosicionesDesempate')
                    ->actualizarPosiciones($datos["desempateDatos"]);

                $em->getRepository('futfunBundle:DesempateFecha')
                    ->actualizarFechaSiguiente($datos);


                $em->getRepository('futfunBundle:PosicionesDesempate')
                    ->actualizarCampeon($datos["desempateDatos"],
                        $datos["fechaDesempate"]);

                $apuestasBase = $servicio->verSiHayApuestaCreada(
                    $datos["desempateDatos"]->getTorneo(), $em);

                if (!empty($apuestasBase)) {

                    $em->getRepository('futfunBundle:ApuestasFechas')
                        ->actualizarPuntosFechaDesempateApuestas($fechaDesempate);

                    $em->getRepository('futfunBundle:ApuestasParticipantes')
                        ->actualizarTabla($apuestasBase);

                    $em->getRepository('futfunBundle:ApuestasParticipantes')
                        ->actualizarCampeon($apuestasBase);

                }

                $this->get('session')->getFlashBag()->add(
                    "mensaje-sucess", "La fecha se grabo correctamente"
                );

                return $this->redirectToRoute('trabajo_dbbundle_torneos_completar_resultados_desempate',
                    array("idTorneo" => $idTorneo,
                        "numeroFecha" =>
                            $fechaDesempate->getNumeroFecha()));

            } else {

                $this->get('session')->getFlashBag()->add(
                    "mensaje-danger", "Tiene que haber al menos 1 resultado completado
                     y con los valores del local y visitante");

            }

        }

        return $this->render('futfunBundle:CompletarResultadosDesempate:completarResultadosDesempate.html.twig',
            array("datos" => $datos, "form" => $form->createView(), "ganador" => $ganador));

    }


    private function devolverStringGanador($isDefinicion, $form, $fechaDesempate,
                                           $fechaBaseAnterior)
    {

        $respuesta = [];

        foreach ($fechaDesempate->getDesempatePartidos() as &$valorPart) {

            if ($isDefinicion) {
                $valorLocal = $form->get('valorLocal' . $valorPart->getNumeroPartido())->getData();
                $valorVisita = $form->get('valorVisita' . $valorPart->getNumeroPartido())->getData();
                $valorDefLocal = $form->get('valorDefinicionLocal' . $valorPart->getNumeroPartido())->getData();
                $valorDefVisita = $form->get('valorDefinicionVisita' . $valorPart->getNumeroPartido())->getData();

                if (is_numeric($valorVisita) && is_numeric($valorLocal)) {

                    if (is_numeric($valorDefLocal) && is_numeric($valorDefVisita)) {

                        $respuesta[$valorPart->getNumeroPartido()] = ($valorDefLocal > $valorDefVisita)
                            ? "local" : "visita";

                    } else {

                        if ($fechaBaseAnterior != null) {

                            $partidoAnterior = $fechaBaseAnterior
                                ->getDesempatePartidos()[$valorPart->getNumeroPartido() - 1];

                            $valorLocalTotal = intval($valorLocal) + intval($partidoAnterior->getValorVisita());
                            $valorVisitaTotal = intval($valorVisita) + intval($partidoAnterior->getValorLocal());

                            if ($valorLocalTotal != $valorVisitaTotal) {

                                $respuesta[$valorPart->getNumeroPartido()] = ($valorLocalTotal > $valorVisitaTotal)
                                    ? "local" : "visita";

                            } else {

                                $valorLocalTotal += intval($partidoAnterior->getValorVisita());
                                $valorVisitaTotal += intval($valorVisita);

                                $respuesta[$valorPart->getNumeroPartido()] = ($valorLocalTotal > $valorVisitaTotal)
                                    ? "local" : "visita";

                            }


                        } else {

                            $respuesta[$valorPart->getNumeroPartido()] = ($valorLocal > $valorVisita)
                                ? "local" : "visita";

                        }

                    }

                } else {

                    $respuesta[$valorPart->getNumeroPartido()] = "";

                }
            } else {

                $respuesta[$valorPart->getNumeroPartido()] = "";

            }
        }

        return $respuesta;

    }
}
