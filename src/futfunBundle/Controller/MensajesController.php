<?php

namespace futfunBundle\Controller;

use futfunBundle\Entity\Mensajes;
use futfunBundle\Form\MensajesType;
use futfunBundle\Servicios\ServMensajes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MensajesController extends Controller
{
    public function verAction(Request $request)
    {

        $bandeja = $request->query->get('bm');
        $idm = $request->query->get('idm');
        $page = $request->query->get('page');

        $user = $this->getUser();
        $servicio = new ServMensajes();
        $em = $this->getDoctrine()->getManager();

        $datos = $servicio->validarDatos(
            $user, $em, $this, $bandeja, $page);

        $mensaje = new Mensajes($user);

        $form = $this->createForm(new MensajesType(), $mensaje);

        $form->handleRequest($request);

        if ($form->isValid() && $request->isMethod('POST')) {

            $receptor = $em
                ->getRepository("futfunBundle:User")
                ->loadByUsername($form->get('para')->getData());

            if ($receptor != null) {

                if ($mensaje->getTitulo() == null
                    or empty($mensaje->getTitulo())
                ) {
                    $mensaje->setTitulo("Hola");
                }

                $mensaje->setReceptor($receptor);
                $em
                    ->getRepository("futfunBundle:Mensajes")
                    ->grabarNuevo($mensaje);

            } else {

                $errorMessage1 = new FormError(
                    utf8_encode("�El usuario no fue encontrado!"));
                $form->get('para')->addError($errorMessage1);

            }

        }

        $deleteFormAjax = $this->createCustomForm(
            'POST', 'borrar_mensajes', "_ID", "_BM");

        $deleteFormAjax2 = $this->createCustomForm(
            'POST', 'marcar_mensaje_leido', "_ID", "_BM");

        return $this->render('futfunBundle:Mensajes:vermensajes.html.twig', array(
                "datos" => $datos,
                "form" => $form->createView(),
                "delete_form_ajax" => $deleteFormAjax->createView(),
                "delete_form_ajax_2" => $deleteFormAjax2->createView(),
                "idm" => $idm)
        );

    }

    private function createCustomForm($method, $route, $id, $bm)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($route,
                array('idMensaje' => $id, "bm" => $bm)))
            ->setMethod($method)
            ->getForm();

        return $form;

    }

    public function borrarMensajeAction(Request $request)
    {

        $idMensaje = $request->query->get('idMensaje');
        $bandeja = $request->query->get('bm');

        $em = $this->getDoctrine()->getManager();

        $mensaje = $em
            ->getRepository("futfunBundle:Mensajes")
            ->find($idMensaje);

        $mensaje->setIsActive(false);
        $em
            ->getRepository("futfunBundle:Mensajes")
            ->grabarNuevo($mensaje);

        return $this->redirectToRoute("mensajes", array("bm" => $bandeja));

    }

    public function marcarMensajeAction(Request $request)
    {

        $idMensaje = $request->query->get('idMensaje');
        $bandeja = $request->query->get('bm');
        $em = $this->getDoctrine()->getManager();

        $mensaje = $em
            ->getRepository("futfunBundle:Mensajes")
            ->find($idMensaje);

        $mensaje->setIsLeido(true);
        $em
            ->getRepository("futfunBundle:Mensajes")
            ->grabarNuevo($mensaje);

        return $this->redirectToRoute("mensajes",
            array("bm" => $bandeja, "idm" => $idMensaje));

    }

    public function mostrarMensajesNuevosAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $cantidadNuevos = $em
            ->getRepository("futfunBundle:Mensajes")
            ->devolverCantidadMensajesNuevos($this->getUser());
        return new Response($cantidadNuevos);

    }
}
