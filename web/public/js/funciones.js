function esEntero(numero) {
    if (isNaN(numero)) {
        return false;
    }
    else {
        if (numero % 1 == 0) {
            return true;
        } else {
            return false;
        }
    }
}

$(document).ready(function () {
    $('#aOpcionesPizarra1').click(function (e) {

        if ($('#liOpcionesPizarra1').hasClass("active") == false) {

            e.preventDefault();

            $("#liOpcionesPizarra2").removeClass("active");
            $("#liOpcionesPizarra2").removeClass("disabled");
            $("#liOpcionesPizarra1").addClass("active");
            $("#liOpcionesPizarra1").addClass("disabled");
            $("#aOpcionesPizarra2").addClass("grayColor");

            var elment1 = document.getElementById("mensajesPizarra");
            procesarAjax('#form-recargarmsg', true, elment1.scrollHeight);

        }

    });

    $('#aOpcionesPizarra2').click(function (e) {

        if ($('#liOpcionesPizarra2').hasClass("active") == false
            && $('#liOpcionesPizarra2').hasClass("disabled") == false) {

            e.preventDefault();

            $("#liOpcionesPizarra1").removeClass("active");
            $("#liOpcionesPizarra1").removeClass("disabled");
            $("#liOpcionesPizarra2").addClass("active");
            $("#liOpcionesPizarra2").addClass("disabled");
            $("#aOpcionesPizarra1").addClass("grayColor");

            var elment1 = document.getElementById("mensajesPizarra");
            procesarAjax('#form-recargarmsg', true, elment1.scrollHeight);
        }

    });

    $('#btnEnviarMsg').click(function (e) {

        e.preventDefault();

        var valor = $("#txtMensajePizarra").val();
        if (valor != "") {
            var elment1 = document.getElementById("mensajesPizarra");
            procesarAjax('#form-enviarmsg', true, elment1.scrollHeight);
        }

    });

    $('#mensajesPizarra').show(function () {

        var texto = $(this).data('msg');
        cargarHtml(texto);

        var elment1 = document.getElementById("mensajesPizarra");
        elment1.scrollTop = elment1.scrollHeight;

    });

    $('#spnMensajePM').click(function () {

        var maximoMsg = parseInt($("#pnlPizarraMsg").data('mostrarmas')) + 8;
        $('#pnlPizarraMsg').data('mostrarmas', maximoMsg);

        procesarAjax("#form-recargarmsg", false, 0);

    });

    $('#spnRefrescarMsg').click(function (e) {

        e.preventDefault();
        var elment1 = document.getElementById("mensajesPizarra");
        procesarAjax("#form-recargarmsg", false, elment1.scrollHeight);

    });

});

function cargarHtml(texto) {

    var elment = document.getElementById("pnlmensajePizarraDnto");
    elment.innerHTML = texto;

}

function procesarAjax(formEntrada, isTexto, valorScroll) {

    var form = $(formEntrada);
    var url = form.attr('action');

    url = devolverUrl(url);

    var data = form.serialize();

    $.post(url, data, function (result) {

        var elment = document.getElementById("pnlmensajePizarraDnto");
        elment.innerHTML = result.message;
        if (isTexto == true) {
            document.getElementById("txtMensajePizarra").value = "";
        }

        var elment1 = document.getElementById("mensajesPizarra");
        elment1.scrollTop = valorScroll;

        if (result.isMostrarMas == true) {

            $("#spnMensajePM").removeClass("ocultar");

        } else {

            $("#spnMensajePM").addClass("ocultar");

        }

    }).fail(function () {
        console.log('ERROR');

    });

}

function devolverUrl(url) {

    var maximoMsg = $("#pnlPizarraMsg").data('mostrarmas');

    if ($('#liOpcionesPizarra1').hasClass("active") == true) {

        url = url + "?bandeja=public&maximoMsg=" + maximoMsg;

    } else {

        var id = $("#liOpcionesPizarra2").data('ida');

        url = url + "?bandeja=private&idapuesta=" + id + "&maximoMsg=" + maximoMsg;

    }

    return url;

}