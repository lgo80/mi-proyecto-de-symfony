$(document).ready(function () {
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });

    $('#cmbEditarArriba').click(function (e) {
        habilitarCamposResultados();
    });
    $('#cmbEditarAbajo').click(function (e) {
        habilitarCamposResultados();
    });
});


function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar los resultados de la fecha?",
        message: "Los resultados se pueden cambiar hasta la fecha de cierre!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmResultadosApuestas.submit();

            }
        }
    });

}

function habilitarCamposResultados() {

    document.getElementById("cmbGuardarArriba").disabled = false;
    document.getElementById("cmbGuardarAbajo").disabled = false;
    var cantGrupos = parseInt($("#pnlBodyCrear1").data("grupos"));

    var i;
    var k = 0;
    for (i = 1; i <= cantGrupos; i++) {

        var cantPartidos = parseInt($("#pnlPartidos" + i).data("partidos"));

        var j;
        for (j = 1; j <= cantPartidos; j++) {

            k = k + 1;
            var id = $("#pnlHabilitar" + i + j).data("habilitar");

            console.log("entro y habilitar es: " + id);

            if (id == true) {

                $("#txtResultadoLocal" + k).removeAttr("readonly");
                $("#txtResultadoVisita" + k).removeAttr("readonly");

            }

        }
    }


}