$(document).ready(function () {
    $('#cmbEditarArriba').click(function (e) {
        confirmarCrearTorneo("guardar");
    });
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo("confirmar");
    });
	$('#cmbEditarAbajo').click(function (e) {
        confirmarCrearTorneo("guardar");
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo("confirmar");
    });
});

function confirmarCrearTorneo(lugar) {

    var titulo;
    var mensaje;
    if (lugar == "guardar") {

        titulo = "Desea guardar los datos de los extras?";
        mensaje = "Solo se guardan los datos sin finalizar el torneo de apuestas";

    } else {

        titulo = "Desea confirmar los datos de los extras?";
        mensaje = "Se guardan los datos y se finaliza el torneo de apuestas";

    }
    bootbox.confirm({
        title: titulo,
        message: mensaje,
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
			
            if (result == true) {

                if (lugar == "guardar") {
                    document.frmConfirmarExtras.btnGuardar.click();
                } else {
                    document.frmConfirmarExtras.btnGuardarYConfirmar.click();
                }

            }
        }
    });

}