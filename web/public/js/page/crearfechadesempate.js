$(document).ready(function () {
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
});

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar la fecha?",
        message: "Los nombres de equipo van a poder cambiarse si es que lo desea!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmCrearFecha.submit();

            }
        }
    });

}

function cambiarBorde(e) {

    $(".bordeClass").removeClass("bordeAzul");
    $("#" + e.id).addClass("bordeAzul");
}

function ingresarEquipo(nombreEquipo, partidos, isLibre) {

    var lugar = devolverInputAIngresar(partidos, isLibre);

    if ((lugar != "no")) {
        document.getElementById(lugar).value = nombreEquipo;
        $(".bordeClass").removeClass("bordeAzul");
    }

}

function devolverInputAIngresar(partidos, isLibre) {

    var nombreBorderColor = "";
    var nombreVacio = "";


    for (var k = 1; k <= partidos; k++) {

        var nombreInputLocal = ("txtLocal" + k);
        var nombreInputVisita = ("txtVisita" + k);

        if (nombreBorderColor == "" && $("#" + nombreInputLocal).hasClass("bordeAzul")) { //document.getElementById(nombreInputLocal).style.borderColor == "blue"
            nombreBorderColor = nombreInputLocal;
        }

        if (nombreBorderColor == "" && $("#" + nombreInputVisita).hasClass("bordeAzul")) {
            nombreBorderColor = nombreInputVisita;
        }

        if (nombreVacio == "" && $("#" + nombreInputLocal).val() == "") {

            nombreVacio = nombreInputLocal;
        }

        if (nombreVacio == "" && $("#" + nombreInputVisita).val() == "") {

            nombreVacio = nombreInputVisita;
        }


    }

    var nombreInputLibre = ("txtEquipoLibre");

    if (isLibre != 1 &&nombreBorderColor == "" && $("#" + nombreInputLibre).hasClass("bordeAzul")) {
        nombreBorderColor = nombreInputLibre;
    }

    if (isLibre != 1 && nombreVacio == "" && $("#" + nombreInputLibre).val() == "") {

        nombreVacio = nombreInputLibre;
    }

    if (nombreBorderColor != "") {
        return nombreBorderColor;
    } else if (nombreVacio != "") {
        return nombreVacio;
    } else {
        return "no";
    }

}