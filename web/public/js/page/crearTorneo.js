$(document).ready(function () {
    $('#cmbFormaLiga').show(function (e) {
        habilitarCollage();
    });

    $('#cmbTipoTorneo').change(function (e) {
        habilitarCollage();
    });

    $('#cmbFormaLiga').change(function (e) {
        habilitarVueltaLiga();
    });

    $('#txtCantEquipos').change(function (e) {
        verSiModificoComienzo();
    });

    $('#txtClasPorGrupo').change(function (e) {
        cambiarComienzoEliminacion();
    });

    $('#txtClasAdic').change(function (e) {
        cambiarComienzoEliminacion();
    });

    $('#cmbFormaElim').change(function (e) {
        habilitarGolVisitante();
    });

    $('#cmbFormaFinal').change(function (e) {
        habilitarGolVisitante();
    });

    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
});

function habilitarCollage() {

    limpiarObjetos();
    var valor = $('#cmbTipoTorneo').val();

    $("#cmbFormaLiga").children().prop('disabled', !(valor == 1 || valor == 2));
    $("#cmbVueltaGrupo").children().prop('disabled', true);
    $("#chcpartDesem").children().prop('disabled', !(valor == 1 || valor == 2));
    document.getElementById("txtPxG").disabled = !(valor == 1 || valor == 2);
    document.getElementById("txtPxE").disabled = !(valor == 1 || valor == 2);
    document.getElementById("txtPxP").disabled = !(valor == 1 || valor == 2);
    document.getElementById("cmbFechasAdic").disabled = (valor != 1);
    document.getElementById("txtCantGrupos").disabled = (valor != 2);
    document.getElementById("txtEquipGrupos").disabled = (valor != 2);
    document.getElementById("txtClasPorGrupo").disabled = (valor != 2);
    document.getElementById("txtClasAdic").disabled = (valor != 2);
    $("#cmbTipoClasif").children().prop('disabled', !(valor == 2));
    $("#cmbComienzo").children().prop('disabled', !(valor == 3 || valor == 2));
    $("#cmbFormaElim").children().prop('disabled', !(valor == 3 || valor == 2));
    $("#chcTercerPuesto").children().prop('disabled', !(valor == 3 || valor == 2));
    $("#cmbFormaFinal").children().prop('disabled', !(valor == 3 || valor == 2));
    $("#chcGolVisita").children().prop('disabled', true);

    var cantEquipos = parseInt($('#txtCantEquipos').val());

    if (valor == 3) {

        modificarComienzo(parseInt(cantEquipos));

    }

}

function habilitarVueltaLiga() {

    var valor = $('#cmbFormaLiga').val();

    $("#cmbVueltaGrupo").children().prop('disabled', (valor != "idayvuelta" ));

    $("select#cmbVueltaGrupo").val('').attr('selected', 'selected');

}

function habilitarGolVisitante() {

    var valor = $('#cmbFormaElim').val();
    var valorFinal = $('#cmbFormaFinal').val();

    $("#chcGolVisita").children().prop('disabled', (valor != "idayvuelta" && valorFinal != "idayvuelta"));

    $("select#chcGolVisita").val('').attr('selected', 'selected');

}

function verSiModificoComienzo() {

    var cantEquipos = parseInt($('#txtCantEquipos').val());
    var tipoTorneo = $('#cmbTipoTorneo').val();

    if (tipoTorneo == 3) {

        modificarComienzo(parseInt(cantEquipos));

    }

}

function limpiarObjetos() {

    $("select#cmbFormaLiga").val('').attr('selected', 'selected');
    $("select#cmbVueltaGrupo").val('').attr('selected', 'selected');
    document.getElementById("torneo_base_dato_partidoDesempate_1").checked = true;
    document.getElementById("txtPxG").value = 3;
    document.getElementById("txtPxE").value = 1;
    document.getElementById("txtPxP").value = 0;
    document.getElementById("cmbFechasAdic").value = "";
    document.getElementById("txtCantGrupos").value = "";
    document.getElementById("txtEquipGrupos").value = "";
    document.getElementById("txtClasPorGrupo").value = "";
    document.getElementById("txtClasAdic").value = "";
    $("select#cmbComienzo").val('').attr('selected', 'selected');
    $("select#cmbFormaElim").val('').attr('selected', 'selected');
    document.getElementById("torneo_base_dato_partTercerPuesto_1").checked = true;
    $("select#cmbFormaFinal").val('').attr('selected', 'selected');

}

function cambiarComienzoEliminacion() {

    var clasXGrupo = parseInt($('#txtClasPorGrupo').val());
    var clasAdic = parseInt($('#txtClasAdic').val());
    var cantGrupos = parseInt($('#txtCantGrupos').val());
    var suma = 0;


    if (esEntero(clasXGrupo) && esEntero(cantGrupos)) {

        suma = suma + (cantGrupos * clasXGrupo);

    }

    if (esEntero(clasAdic)) {

        suma = suma + clasAdic;

    }

    modificarComienzo(suma);

}

function modificarComienzo(suma) {

    switch (suma) {
        case 2:
            $("select#cmbComienzo").val('_1avos').attr('selected', 'selected');
            break;
        case 4:
            $("select#cmbComienzo").val('_2avos').attr('selected', 'selected');
            break;
        case 8:
            $("select#cmbComienzo").val('_4avos').attr('selected', 'selected');
            break;
        case 16:
            $("select#cmbComienzo").val('_8avos').attr('selected', 'selected');
            break;
        case 32:
            $("select#cmbComienzo").val('_16avos').attr('selected', 'selected');
            break;
        case 64:
            $("select#cmbComienzo").val('_32avos').attr('selected', 'selected');
            break;
        case 128:
            $("select#cmbComienzo").val('_64avos').attr('selected', 'selected');
            break;
        case 256:
            $("select#cmbComienzo").val('_124avos').attr('selected', 'selected');
            break;
        default :
            $("select#cmbComienzo").val('').attr('selected', 'selected');
            break;
    }

}

function esEntero(numero) {
    if (isNaN(numero)) {
        return false;
    }
    else {
        if (numero % 1 == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar el torneo?",
        message: "Si confirma el torneo lo uncio que no puede cambiar es el tipo de torneo!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.getElementById('btnGuardar').click();

            }
        }
    });

}
