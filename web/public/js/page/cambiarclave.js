$(document).ready(function () {
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
});


function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea registrarse con estos datos?",
        message: "Acepte para poder utilizar las herramientas de la pagina!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmcambiarclave.submit();

            }
        }
    });

}