$(document).ready(function () {
    $('#cmbAROpciones').change(function (e) {

        e.preventDefault();
        var row = $(this);

        var urlCtrl = row.data("url");
        var cod = document.getElementById("cmbAROpciones").value;

        var parametro = {
            "opt": cod
        };

        ajax(urlCtrl, parametro, "combo1", null);

    });
    $('#cmbARApuestasOpciones').change(function (e) {

        e.preventDefault();
        var row = $(this);

        var urlCtrl = row.data("url");
        var idApu = document.getElementById("cmbARApuestasOpciones").value;
        var cod = document.getElementById("cmbAROpciones").value;

        if (idApu > 0) {

            var parametro = {
                "idApu": idApu,
                "opt": cod
            };
            ajax(urlCtrl, parametro, "combo2", idApu);

        } else {
            modificarDataAccessos(false, false
                , false, false, false, false);
            isOcultar("pnlARSpan", false);
        }

    });
    $('#cmbARApuestasOpciones').show(function (e) {

        var idApu = document.getElementById("cmbARApuestasOpciones").value;
        if (idApu > 0) {

            var row = $("#cmbARApuestasOpciones");

            var urlCtrl = row.data("url");
            var cod = document.getElementById("cmbAROpciones").value;

            var parametro = {
                "idApu": idApu,
                "opt": cod
            };
            ajax(urlCtrl, parametro, "combo2", idApu);


        } else {
            modificarDataAccessos(false, false
                , false, false, false, false);
            isOcultar("pnlARSpan", false);
        }

    });
    $('.accessos').click(function (e) {

        var row = $(this);

        var urlCtrl = row.data("url");
        var idApu = document.getElementById("cmbARApuestasOpciones").value;
        urlCtrl = urlCtrl.replace('parameter', idApu).trim();

        location.href = urlCtrl;


    });


    $('#cmbTOOpciones').change(function (e) {

        e.preventDefault();
        var row = $(this);

        var urlCtrl = row.data("url");
        var cod = document.getElementById("cmbTOOpciones").value;
        //urlCtrl = urlCtrl.replace('opt', cod).trim();

        var parametro = {
            "opt": cod
        };

        ajax(urlCtrl, parametro, "combo3", null);

    });
    $('#cmbTOTorneosOpciones').change(function (e) {

        e.preventDefault();
        var row = $(this);

        var urlCtrl = row.data("url");
        var idApu = document.getElementById("cmbTOTorneosOpciones").value;
        var cod = document.getElementById("cmbTOOpciones").value;

        if (idApu > 0) {

            var parametro = {
                "idApu": idApu,
                "opt": cod
            };
            ajax(urlCtrl, parametro, "combo4", idApu);

        } else {
            modificarDataAccessosT(false, false
                , false, false, false);
            isOcultar("pnlTOSpan", false);
        }

    });
    $('#cmbTOTorneosOpciones').show(function (e) {

        var idApu = document.getElementById("cmbTOTorneosOpciones").value;
        if (idApu > 0) {

            var row = $("#cmbTOTorneosOpciones");

            var urlCtrl = row.data("url");
            var cod = document.getElementById("cmbTOOpciones").value;

            var parametro = {
                "idApu": idApu,
                "opt": cod
            };
            ajax(urlCtrl, parametro, "combo4", idApu);


        } else {
            modificarDataAccessosT(false, false
                , false, false, false);
            isOcultar("pnlTOSpan", false);
        }

    });
    $('.accessosT').click(function (e) {

        var row = $(this);

        var urlCtrl = row.data("url");
        var idApu = document.getElementById("cmbTOTorneosOpciones").value;
        urlCtrl = urlCtrl.replace('parameter', idApu).trim();

        location.href = urlCtrl;


    });
    $('.accessos').click(function (e) {

        var row = $(this);

        var urlCtrl = row.data("url");
        var idApu = document.getElementById("cmbARApuestasOpciones").value;
        urlCtrl = urlCtrl.replace('parameter', idApu).trim();

        location.href = urlCtrl;


    });
});

function ajax(urlCtrl, parametro, lugar, idApu) {
    jQuery.ajax({
        url: urlCtrl,
        type: 'POST',
        dataType: 'json',
        data: parametro,
        beforeSend: function () {
            $(".loadingla").removeClass("ocultar");
        }
    })
        .done(function (respuesta) {

            if (lugar == "combo1") {
                combo1(respuesta);
            } else if (lugar == "combo2") {
                combo2(respuesta, idApu);
            } else if (lugar == "combo3") {
                combo3(respuesta);
            } else {
                combo4(respuesta, idApu);
            }

        })
        .fail(function (resp) {
            // console.log("mal" + resp.responseText);
        })
        .always(function (respuesta) {
        });
}

function combo1(respuesta) {

    var combo = document.getElementById('cmbARApuestasOpciones');
    combo.length = 0;
    combo.options[0] = new Option("Seleccione...", "", "0");

    if (Array.isArray(respuesta.mensaje) == true) {

        for (var i = 0; i < respuesta.mensaje.length; i++) {
            var apuesta = respuesta.mensaje[0];
            var numero = i + 1;
            combo.options[numero] = new Option(respuesta.mensaje[i].nombre, respuesta.mensaje[i].id, numero.toString());
        }

    } else {

        combo.options[1] = new Option(respuesta.mensaje, "nada", "0");

    }

    modificarDataAccessos(false, false
        , false, false, false, false);
    isOcultar("pnlARSpan", false);

}

function combo2(respuesta, idApu) {

    var objeto = respuesta.mensaje;
    $("#aAnotarse").data("id", idApu);
    modificarDataAccessos(
        objeto.anotarse, objeto.datos
        , objeto.extras, objeto.completarResul
        , objeto.posiciones, objeto.resultados);
    isHabilitar();
    clasePosicion();
    isOcultar("pnlARSpan", true);

}

function combo3(respuesta) {

    var combo = document.getElementById('cmbTOTorneosOpciones');
    combo.length = 0;
    combo.options[0] = new Option("Seleccione...", "", "0");

    if (Array.isArray(respuesta.mensaje) == true) {

        for (var i = 0; i < respuesta.mensaje.length; i++) {
            var apuesta = respuesta.mensaje[0];
            var numero = i + 1;
            combo.options[numero] = new Option(respuesta.mensaje[i].nombre, respuesta.mensaje[i].id, numero.toString());
        }

    } else {

        combo.options[1] = new Option(respuesta.mensaje, "nada", "0");

    }

    modificarDataAccessosT(false, false
        , false, false, false);
    isOcultar("pnlTOSpan", false);

}

function combo4(respuesta, idApu) {

    var objeto = respuesta.mensaje;
    $("#aAnotarse").data("id", idApu);
    modificarDataAccessosT(
        objeto.datos, objeto.crear
        , objeto.posiciones, objeto.completarResul
        , objeto.desempate);
    isHabilitarT();
    clasePosicionT();
    isOcultar("pnlTOSpan", true);

}

function modificarDataAccessos(anotarse
    , datos, extras, completar, tabla, ver) {

    $("#aAnotarse").data("hab", anotarse);
    $("#aDatos").data("hab", datos);
    $("#aExtras").data("hab", extras);
    $("#aCompletar").data("hab", completar);
    $("#aTabla").data("hab", tabla);
    $("#aVer").data("hab", ver);

}

function modificarDataAccessosT(datos, crear
    , tabla, completar, desempate) {

    $("#aDatosT").data("hab", datos);
    $("#aCrearT").data("hab", crear);
    $("#aTablaT").data("hab", tabla);
    $("#aCompletarT").data("hab", completar);
    $("#aDesempateT").data("hab", desempate);

}

function isOcultar(id, valor) {

    if (valor) {

        $("#" + id).removeClass("ocultar");
    } else {
        $("#" + id).addClass("ocultar");
    }
}

function isHabilitar() {

    isOcultar("aAnotarse", ($("#aAnotarse").data("hab")));
    isOcultar("aDatos", ($("#aDatos").data("hab")));
    isOcultar("aExtras", ($("#aExtras").data("hab")));
    isOcultar("aCompletar", ($("#aCompletar").data("hab")));
    isOcultar("aTabla", ($("#aTabla").data("hab")));
    isOcultar("aVer", ($("#aVer").data("hab")));
}

function isHabilitarT() {

    isOcultar("aDatosT", ($("#aDatosT").data("hab")));
    isOcultar("aCrearT", ($("#aCrearT").data("hab")));
    isOcultar("aTablaT", ($("#aTablaT").data("hab")));
    isOcultar("aCompletarT", ($("#aCompletarT").data("hab")));
    isOcultar("aDesempateT", ($("#aDesempateT").data("hab")));
}

function clasePosicion() {

    var suma = 0;
    if (!$("#aAnotarse").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aDatos").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aExtras").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aCompletar").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aTabla").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aVer").hasClass('ocultar')) {
        suma = suma + 1;
    }

    removePadding("pnlARSpan");
    agregarPadding("pnlARSpan", suma);

}

function clasePosicionT() {
    var suma = 0;
    if (!$("#aDatosT").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aCrearT").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aTablaT").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aCompletarT").hasClass('ocultar')) {
        suma = suma + 1;
    }
    if (!$("#aDesempateT").hasClass('ocultar')) {
        suma = suma + 1;
    }

    removePadding("pnlTOSpan");
    agregarPadding("pnlTOSpan", suma);

}

function agregarPadding(id, suma) {

    switch (suma) {
        case 2:
            $("#" + id).addClass('padding2');
            break;
        case 3:
            $("#" + id).addClass('padding3');
            break;
        case 4:
            $("#" + id).addClass('padding4');
            break;
        case 5:
            $("#" + id).addClass('padding5');
            break;
    }

}

function removePadding(id) {

    $("#" + id).removeClass("padding2");
    $("#" + id).removeClass("padding3");
    $("#" + id).removeClass("padding4");
    $("#" + id).removeClass("padding5");
}
