$(document).ready(function () {
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
    $('.btnModificarEquipo').click(function (e) {

        e.preventDefault();
        var row = $(this).parents('div');

        var id = row.data("idclub");

        var form = $('#form-cambiar');

        var url = form.attr('action').replace('FECHA-ANT', id);

        var mensaje = "Se quiere modificar el equipo: " + id;

        bootbox.prompt(mensaje, function (result) {

            if (result != null && result != "" && result != id) {

                document.forms['form-agregar'].action
                    = url.replace('FECHA-NUEVA', result);
                document.forms['form-agregar'].submit();

            }

        });


    });
});

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar la fecha?",
        message: "Los nombres de equipo van a poder cambiarse si es que lo desea!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmCrearFecha.submit();

            }
        }
    });

}

function ingresarEquipo(nombreEquipo, grupo, partidos, isLibre) {

    var lugar = devolverInputAIngresar(grupo, partidos, isLibre);

    if ((lugar != "no")) {
        document.getElementById(lugar).value = nombreEquipo;
        $(".bordeClass").removeClass("bordeAzul");
    }

}

function devolverInputAIngresar(grupo, partidos, isLibre) {

    var nombreBorderColor = "";
    var nombreVacio = "";


    for (var k = 1; k <= partidos; k++) {

        var nombreInputLocal = ("txtLocal" + grupo + k);
        var nombreInputVisita = ("txtVisita" + grupo + k);

        if (nombreBorderColor == "" && $("#" + nombreInputLocal).hasClass("bordeAzul")) { //document.getElementById(nombreInputLocal).style.borderColor == "blue"
            nombreBorderColor = nombreInputLocal;
        }

        if (nombreBorderColor == "" && $("#" + nombreInputVisita).hasClass("bordeAzul")) {
            nombreBorderColor = nombreInputVisita;
        }

        if (nombreVacio == "" && $("#" + nombreInputLocal).val() == "") {

            nombreVacio = nombreInputLocal;
        }

        if (nombreVacio == "" && $("#" + nombreInputVisita).val() == "") {

            nombreVacio = nombreInputVisita;
        }


    }

    var nombreInputLibre = ("txtEquipoLibre" + grupo);

    if (isLibre != 1 && nombreBorderColor == "" && $("#" + nombreInputLibre).hasClass("bordeAzul")) {
        nombreBorderColor = nombreInputLibre;
    }

    if (isLibre != 1 && nombreVacio == "" && $("#" + nombreInputLibre).val() == "") {

        nombreVacio = nombreInputLibre;
    }

    if (nombreBorderColor != "") {
        return nombreBorderColor;
    } else if (nombreVacio != "") {
        return nombreVacio;
    } else {
        return "no";
    }

}

function prueba(e) {

    $(".bordeClass").removeClass("bordeAzul");
    $("#" + e.id).addClass("bordeAzul");

}
