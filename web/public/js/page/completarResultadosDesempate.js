$(document).ready(function () {

    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });

    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });

    $('#cmbEditarArriba').click(function (e) {
        habilitarCamposResultados(false);
    });

    $('#cmbEditarAbajo').click(function (e) {
        habilitarCamposResultados(false);
    });

});


function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar los resultados de la fecha?",
        message: "Los resultados se pueden cambiar hasta la fecha de cierre!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmResultadosDesempate.submit();
                habilitarCamposResultados(true);

            }
        }
    });

}

function habilitarCamposResultados(isHabilitar) {

    $(".txtLocalRes").removeAttr("readonly");
    $(".txtVisitaRes").removeAttr("readonly");
    document.getElementById("cmbGuardarAbajo").disabled = false;
    document.getElementById("cmbGuardarArriba").disabled = false;

}

function habilitarDefinicionPenales(isPenalty, golVisitante, valorLocalIda,
                                    valorVisitaIda, partido, isHabilitar) {

    var isGanoAlguien = false;

    if (isPenalty) {

        var valorLocalVuelta = $('#txtResultadoLocal' + partido).val();
        var valorVisitaVuelta = $('#txtResultadoVisita' + partido).val();

        var isValid = devolverResultadosValidos(valorLocalVuelta, valorVisitaVuelta);

        if (isValid) {
            var valorEnTotalLocal = parseInt(valorLocalVuelta) + parseInt(valorVisitaIda);
            var valorEnTotalVisita = parseInt(valorVisitaVuelta) + parseInt(valorLocalIda);

            if (valorEnTotalLocal == valorEnTotalVisita) {

                if (golVisitante) {

                    valorEnTotalLocal = parseInt(valorEnTotalLocal) + parseInt(valorVisitaIda);
                    valorEnTotalVisita = parseInt(valorEnTotalVisita) + parseInt(valorVisitaVuelta);

                }

                if (isHabilitar == "true") {

                    deshabilitarPenales(partido, (valorEnTotalLocal != valorEnTotalVisita), false);

                }


                isGanoAlguien = (valorEnTotalLocal != valorEnTotalVisita);

            } else {

                isGanoAlguien = true;

            }

            eliminarClase(partido);
            if (isGanoAlguien) {

                var equipoGanador = devolverGanador(isGanoAlguien, partido,
                    valorEnTotalLocal, valorEnTotalVisita);

                $("#" + equipoGanador).addClass("negrita");
                deshabilitarPenales(partido, true, true);

            }
        } else {

            eliminarClase(partido);
            deshabilitarPenales(partido, true, true);

        }

        actualizarGlobal(valorLocalIda, valorVisitaIda, valorLocalVuelta,
            valorVisitaVuelta, isValid, partido);

    }

}

function actualizarGlobal(valorLocalIda, valorVisitaIda, valorLocalVuelta,
                          valorVisitaVuelta, isValid, partido) {

    var valorGlobal = (isValid)
        ? "Global: " + (parseInt(valorVisitaIda) + parseInt(valorLocalVuelta))
    + " - " + (parseInt(valorLocalIda) + parseInt(valorVisitaVuelta))
        : "Global: " + valorVisitaIda + " - " + valorLocalIda;

    document.getElementById("lblGlobal" + partido).innerHTML = valorGlobal;

}

function eliminarClase(partido) {

    $("#lblLocal" + partido).removeClass("negrita");
    $("#lblVisita" + partido).removeClass("negrita");
}

function deshabilitarPenales(partido, isHabilitar, isPonerBlanco) {

    document.getElementById('txtValorDefinicionLocal' + partido).readOnly
        = isHabilitar;
    document.getElementById('txtValorDefinicionVisita' + partido).readOnly
        = isHabilitar;

    if (isPonerBlanco) {
        document.getElementById("txtValorDefinicionLocal" + partido).value = "";
        document.getElementById("txtValorDefinicionVisita" + partido).value = "";
    }

}

function devolverResultadosValidos(valorLocalVuelta, valorVisitaVuelta) {

    var isValid = false;

    if (esEntero(valorLocalVuelta) && esEntero(valorVisitaVuelta)) {

        if (parseInt(valorLocalVuelta) >= 0 && parseInt(valorVisitaVuelta) >= 0) {

            isValid = true;

        }
    }

    return isValid;
}

function devolverGanador(isGanoAlguien, partido, valorLocal, valorVisita) {

    if (isGanoAlguien) {

        return (valorLocal > valorVisita)
            ? "lblLocal" + partido : "lblVisita" + partido;

    } else {

        var valorLocalPenales = $('#txtValorDefinicionLocal' + partido).val();
        var valorVisitaPenales = $('#txtValorDefinicionVisita' + partido).val();

        if (devolverResultadosValidos(valorLocalPenales, valorVisitaPenales)) {

            return (valorLocalPenales > valorVisitaPenales)
                ? "lblLocal" + partido : "lblVisita" + partido;

        }


    }
}

function procesarGanador(isGanoAlguien, partido) {

    eliminarClase(partido);

    var equipoGanador = devolverGanador(isGanoAlguien, partido,
        0, 0);

    $("#" + equipoGanador).addClass("negrita");
}

