$(document).ready(function () {
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
});

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Recuperar usuario?",
        message: "Se le mandara un e-mail para poder ingresar contraseņa nueva!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmRecuperarXMail.submit();

            }
        }
    });

}