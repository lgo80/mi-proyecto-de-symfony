function habilitarVueltaLiga() {
	
    var valor = $('#form_forma').val();
	
    $("#pnlTipoVueltaChoice").children().prop('disabled', (valor != "idayvuelta" ));

    $("select#form_tipoVuelta").val('').attr('selected', 'selected');

}

function habilitarVueltaLigaYElim() {

    var valor = $('#form_FormaGrupos').val();

    $("#pnlTipoVueltaGrupoChoice").children().prop('disabled', (valor != "idayvuelta" ));

    $("select#form_tipoVueltaGrupo").val('').attr('selected', 'selected');

}

function cambiarComienzoEliminacion() {

    var clasXGrupo = parseInt($('#form_cantClasPorGrupos').val());
    var clasAdic = parseInt($('#form_cantClasifAdicionales').val());
    var cantGrupos = parseInt($('#form_cantGrupos').val());
    var suma = 0;


    if (esEntero(clasXGrupo) && esEntero(cantGrupos)) {

        suma = suma + (cantGrupos * clasXGrupo);

    }

    if (esEntero(clasAdic)) {

        suma = suma + clasAdic;

    }

    switch (suma) {
        case 2:
            $("select#form_primeraEliminacion").val('_1avos').attr('selected', 'selected');
            break;
        case 4:
            $("select#form_primeraEliminacion").val('_2avos').attr('selected', 'selected');
            break;
        case 8:
            $("select#form_primeraEliminacion").val('_4avos').attr('selected', 'selected');
            break;
        case 16:
            $("select#form_primeraEliminacion").val('_8avos').attr('selected', 'selected');
            break;
        case 32:
            $("select#form_primeraEliminacion").val('_16avos').attr('selected', 'selected');
            break;
        case 64:
            $("select#form_primeraEliminacion").val('_32avos').attr('selected', 'selected');
            break;
        case 128:
            $("select#form_primeraEliminacion").val('_64avos').attr('selected', 'selected');
            break;
        case 256:
            $("select#form_primeraEliminacion").val('_124avos').attr('selected', 'selected');
            break;
        default :
            $("select#form_primeraEliminacion").val('').attr('selected', 'selected');
            break;
    }

}

function esEntero(numero) {
    if (isNaN(numero)) {
        return false;
    }
    else {
        if (numero % 1 == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}