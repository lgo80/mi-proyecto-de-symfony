$(document).ready(function () {
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
    $('.btnModificarEquipo').click(function (e) {

        e.preventDefault();
        var row = $(this).parents('div');

        var id = row.data("idclub");

        if (validarQueSeaEquipo(id)) {

            id = id.replace('(G)', '').trim();
            id = id.replace('(P)', '').trim();

            var form = $('#form-cambiar');

            var url = form.attr('action').replace('FECHA-ANT', id);

            var mensaje = "Se quiere modificar el equipo: " + id;

            bootbox.prompt(mensaje, function (result) {

                if (result != null && result != "" && result != id) {

                    document.forms['form-agregar'].action
                        = url.replace('FECHA-NUEVA', result);
                    document.forms['form-agregar'].submit();

                }

            });

        }

    });
});

function validarQueSeaEquipo(nombreEquipo) {

    if (isHere(nombreEquipo, "Ganador") || isHere(nombreEquipo, "Perdedor")
        || isHere(nombreEquipo, "mejor") || isHere(nombreEquipo, "grupo")) {

        return false;

    }

    return true;

}

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar la fecha?",
        message: "Los nombres de equipo van a poder cambiarse si es que lo desea!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmCrearFechaEliminacion.submit();

            }
        }
    });

}

function ingresarEquipo(nombreEquipo, grupo, partidos, equipos) {


    var lugar = devolverInputAIngresar(grupo, partidos, equipos, nombreEquipo);

    if ((lugar != "no")) {
        document.getElementById(lugar).value = nombreEquipo;
        $(".bordeClass").removeClass("bordeAzul");
    }

}

function devolverInputAIngresar(grupo, partidos, equipos, nombreEquipo) {

    var nombreBorderColor = "";
    var nombreVacio = "";

    var isTercero = isThirdPlace(grupo, partidos, equipos);

    for (var k = 1; k <= partidos; k++) {

        if (k == 1 && isTercero && (isHere(nombreEquipo, "Ganador") || isHere(nombreEquipo, "(G)") )) {

            continue;

        }
        var nombreInputLocal = ("txtLocal" + grupo + k);
        var nombreInputVisita = ("txtVisita" + grupo + k);

        if (nombreBorderColor == "" && $("#" + nombreInputLocal).hasClass("bordeAzul")) { //document.getElementById(nombreInputLocal).style.borderColor == "blue"
            nombreBorderColor = nombreInputLocal;
        }

        if (nombreBorderColor == "" && $("#" + nombreInputVisita).hasClass("bordeAzul")) {
            nombreBorderColor = nombreInputVisita;
        }

        if (nombreVacio == "" && $("#" + nombreInputLocal).val() == "") {

            nombreVacio = nombreInputLocal;
        }

        if (nombreVacio == "" && $("#" + nombreInputVisita).val() == "") {

            nombreVacio = nombreInputVisita;
        }

        if (k == 1 && isTercero && (isHere(nombreEquipo, "Perdedor") || isHere(nombreEquipo, "(P)"))) {

            break;

        }

    }

    if (nombreBorderColor != "") {
        return nombreBorderColor;
    } else if (nombreVacio != "") {
        return nombreVacio;
    } else {
        return "no";
    }

}

function prueba(e) {
    $(".bordeClass").removeClass("bordeAzul");
    $("#" + e.id).addClass("bordeAzul");
}

function isThirdPlace(grupo, partidos, equipos) {

    if (partidos != 2) {

        return false;

    }

    for (var k = 1; k <= equipos; k++) {

        var nombreEquipo = $("#btnEquipoGral" + k).val();

        if (isHere(nombreEquipo, "Perdedor") || isHere(nombreEquipo, "(P)")) {

            return true;

        }

    }

    return false;
}

function isHere(cadena, palabra) {

    var patt = new RegExp(palabra);
    return patt.test(cadena);

}

function mandarComentario() {
        aplicarAjaxEnvioMail();
}

function aplicarAjaxEnvioMail() {
    var nombre = "";
    var mail = "";
    var telefono = "";
    var asunto = "";
    var comentario = "";
    $.ajax({
        type: "POST",
        url: "mandar3.php",
        data: "nombre=" + nombre + "&mail=" + mail + "&telefono=" + telefono + "&asunto=" + asunto + "&comentario=" + comentario,
        success: function(datos) {
            var obj = JSON.parse(datos);
            if (!obj.result) {
                document.getElementById("problemas").innerHTML = "¡Hubo problemas de conexion a internet!<br />vuelva a interntar mas tarde por favor";
            } 
            cerrarMostrarErrores('#apDiv1');
        }
    });
}
