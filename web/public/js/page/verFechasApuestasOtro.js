$(document).ready(function () {
    $('#btnGuardar').click(function (e) {

    });
});


function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar los resultados de la fecha?",
        message: "Los resultados se pueden cambiar hasta la fecha de cierre!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmResultadosApuestas.submit();

            }
        }
    });

}