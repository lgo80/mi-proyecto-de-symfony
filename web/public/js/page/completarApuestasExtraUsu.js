$(document).ready(function () {
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
});

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar los extras del torneo de apuestas?",
        message: "Los valores se van a poder cambiar si es que lo desea hasta que comienze el torneo base!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmCompletarExtras.submit();

            }
        }
    });

}