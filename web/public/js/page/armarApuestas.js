$(document).ready(function () {
    $('#sltExtra').change(function (e) {

        var valor = $('#sltExtra').val();

        limpiarElementos(false);

        document.getElementById("txtDetalleExtra").disabled = (valor != "otros");
        document.getElementById("chkIsComplete").disabled = (valor != "otros");
        document.getElementById("areaDescripcion").disabled = (valor != "otros");

        document.getElementById("chkIsComplete").checked
            = devolverValorIsComplete(valor);

        document.getElementById("areaDescripcion").value
            = devolverMensajeDescripcion(valor);

        elemento = (valor == "otros") ? "txtDetalleExtra" : "txtValor";
        document.getElementById(elemento).focus();

    });

    $('#btnAgregar').click(function (e) {

        var extra = $('#sltExtra').val();
        var detalle = $('#txtDetalleExtra').val();
        var puntos = $('#txtValor').val();
        var isComplete = document.getElementById("chkIsComplete").checked;
        var descripcion = $('#areaDescripcion').val();
        var selected = "";

        if (extra != 'otros') {

            var combo = document.getElementById("sltExtra");
            selected = combo.options[combo.selectedIndex].text;

        } else {

            selected = $('#txtDetalleExtra').val();

        }


        if ((extra != "") && (puntos != "" && esEntero(puntos))
            && ((extra != "otros") || ((extra == "otros") && (detalle != ""))) && (validarExtra(selected))
            && (descripcion != "")) {

            var i = devolverIndice();

            if (extra == "desc") {

                selected = actualizarDescenso(selected);

            }

            document.getElementById("txtExtra" + i).value = selected;
            document.getElementById("txtExtraValor" + i).value = puntos;
            document.getElementById("txtIsComplete" + i).value = (isComplete == 1) ? "SI" : "NO";
            document.getElementById("txtDescripcion" + i).value = descripcion;
            limpiarElementos(true);

        } else {

            bootbox.alert({
                message: "Alg&uacute;n campo no fue completado o es inv&aacute;lido",
                className: 'bb-alternate-modal'
            });

        }
        //

    });

    $('#sltFechaInicioSelect').change(function (e) {
        habilitarFechaLimiteExtra();
    });

    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearApuesta();
    });
});

function devolverValorIsComplete(valor) {

    if (valor == "gol" || valor == "camp"
        || valor == "desc" || valor == "clasf") {

        return 1;

    }

    return 0;


}

function habilitarFechaLimiteExtra() {

    var detalle = $('#sltFechaInicioSelect').val();

    document.getElementById("cmbFechaInicio").disabled = (detalle == "nada");

    if(detalle == "nada"){
        document.getElementById("cmbFechaInicio").value = "";
    }
}

function devolverMensajeDescripcion(valor) {

    if (valor == "gol") {

        return "Puntos por el jugador que mas goles hace entre todos los elegidos";

    } else if (valor == "golExt") {

        return "Puntos extra por si el jugador elegido es el goleador del torneo";

    } else if (valor == "camp") {

        return "Puntos por el equipo que mas puntos hace entre todos los elegidos";

    } else if (valor == "camExt") {

        return "Puntos extra por si el equipo elegido es el campeon del torneo";

    } else if (valor == "desc") {

        return "Puntos por si el equipo elegido es uno de los descendidos";

    } else if(valor == "clasf") {

        return "Puntos por elegir bien equipo que casifica a la ronda de eliminacion";

    } else if(valor == "orden") {

        return "Puntos extra por elegir bien los equipos exactos que casifican a la ronda de eliminacion";

    }

    return "";


}


function limpiarElementos(isDetalle) {

    if (isDetalle) {

        $("select#sltExtra").val('').attr('selected', 'selected');

    }

    document.getElementById("txtDetalleExtra").value = "";
    document.getElementById("txtValor").value = "";
    document.getElementById("chkIsComplete").checked = 0;
    document.getElementById("areaDescripcion").value = "";


}

function validarExtra(extra) {

    for (var i = 1; i <= 10; i++) {

        var detalle = $('#txtExtra' + i).val();

        if (detalle == extra) {

            return false;

        }

    }

    return true;

}

function devolverIndice() {

    for (var i = 1; i <= 10; i++) {

        var detalle = $('#txtExtra' + i).val();

        if (detalle == "") {

            return i;

        }

    }

    return 10;

}

function actualizarDescenso(selected) {

    var j = 1;
    entro = true;

    while (entro == true) {

        selected1 = selected + " " + j;
        entro = false;
        for (var i = 1; i <= 10; i++) {

            var detalle = $('#txtExtra' + i).val();

            if (detalle == selected1) {

                entro = true;
                j++;
                break;

            }

        }

    }

    return selected1;

}

function confirmarCrearApuesta() {
    bootbox.confirm({
        title: "Desea crear dicha apuesta?",
        message: "Se va a crear la apuesta sobre el torneo de referencia que se eligio!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmcrearapuesta.submit();

            }
        }
    });

}