$(document).ready(function () {
    $('#btnGuardar').click(function (e) {
        confirmarCrearTorneo();
    });
    $('form').show(function (e) {
        procesarTipoDesempate();
    });
    $('#cmbTipoTorneo').change(function (e) {
        habilitarCollage();
    });
    $('#cmbFormaDesempate').change(function (e) {
        procesarFormaDesempate();
    });
});

function confirmarCrearTorneo() {

    var isComplete = document.getElementById("chcIsConfirm").checked;


    var mensajeTitulo = (isComplete)
        ? "�El desempate esta confirmado!"
        : "�El desempate no esta confirmado!";
    var mensajeCuerpo = (isComplete)
        ? "�Seguro desea guardar los datos?\nNota: No podra modificarlos si acepta"
        : "�Seguro desea guardar los datos?\nNota: No se va a activar la creacion " +
    "de los partidos hasta que confirme el desempate";
    bootbox.confirm({
        title: mensajeTitulo,
        message: mensajeCuerpo,
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                $("#cmbTipoTorneo").children().prop('disabled', false);
                document.frmCrearDesempate.submit();

            }
        }
    });

}

function procesarTipoDesempate() {

    limpiarObjetos();
    var equipos = document.getElementById("txtCantEquipos").innerHTML;

    if (equipos == "2") {

        $("select#cmbTipoTorneo").val(3).attr('selected', 'selected');
        $("#cmbTipoTorneo").children().prop('disabled', true);


    } else if (equipos == "4" || equipos == "8" || equipos == "16"
        || equipos == "32" || equipos == "64") {

        $("select#cmbTipoTorneo").val('').attr('selected', 'selected');
        $("#cmbTipoTorneo").children().prop('disabled', false);

    } else {

        $("select#cmbTipoTorneo").val(1).attr('selected', 'selected');
        $("#cmbTipoTorneo").children().prop('disabled', true);

    }

    habilitarCollage();
    habilitarCollageResto();

}

function habilitarCollage() {

    limpiarObjetos(true);
    var valor = document.getElementById("cmbTipoTorneo").value;
    $("#cmbFormaDesempate").children().prop('disabled', (valor == ""));

}

function limpiarObjetos(isFormaDesempate) {

    if (isFormaDesempate) {
        $("select#cmbFormaDesempate").val('').attr('selected', 'selected');
    }

    $("select#cmbFormaVuelta").val('').attr('selected', 'selected');
    document.getElementById("futfunBundle_desempatedatos_golVisitante_0").checked = true;


}

function procesarFormaDesempate() {

    limpiarObjetos(false);
    habilitarCollageResto();

}

function habilitarCollageResto() {

    var valorTipo = document.getElementById("cmbTipoTorneo").value;
    var valorForma = document.getElementById("cmbFormaDesempate").value;

    $("#cmbFormaVuelta").children().prop('disabled',
        !((valorForma == "idayvuelta") && (valorTipo == "1")));
    $("#chcGolVisita").children().prop('disabled',
        !((valorForma == "idayvuelta") && (valorTipo == "3")));

}