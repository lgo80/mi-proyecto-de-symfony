$(document).ready(function () {
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo();
    });
    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo();
    });
});

function confirmarCrearTorneo() {

    bootbox.confirm({
        title: "Desea confirmar la primera fecha?",
        message: "Los nombres de equipo van a poder cambiarse si es que lo desea!",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmPrimeraFecha.submit();

            }
        }
    });

}


function validarEquipos(cantPartidos, grupos) {

    var entro = false;
    var cantVacia = 0;
    var errorCarac = 0;
    var errorRepetido = 0;
    var cantGrupos = retornarValor(grupos, "texto");

    for (var i = 1; i <= cantGrupos; i++) {

        for (var j = 1; j <= cantPartidos; j++) {

            var letra = retornarValor(i, "numero");
            var local = $('#txtLocal' + letra + j).val();
            var visita = $('#txtVisita' + letra + j).val();

            if (local == "") {

                entro = true;
                cantVacia++;
                document.getElementById("txtLocal" + letra + j).style.border = "1px solid red";

            } else if (local.length <= 3 || local.length >= 100) {

                entro = true;
                errorCarac++;
                document.getElementById("txtLocal" + letra + j).style.border = "1px solid red";

            } else if (validarRepetidos(local, visita, cantPartidos, cantGrupos, j, i)) {

                entro = true;
                errorRepetido++;
                document.getElementById("txtLocal" + letra + j).style.border = "1px solid blue";

            } else {

                if (document.getElementById("txtLocal" + letra + j).style.borderColor != "blue") {

                    document.getElementById("txtLocal" + letra + j).style.border = "1px solid green";

                }

            }

            if (visita == "") {

                entro = true;
                cantVacia++;
                document.getElementById("txtVisita" + letra + j).style.border = "1px solid red";

            } else if (visita.length <= 3 || visita.length >= 100) {

                entro = true;
                errorCarac++;
                document.getElementById("txtVisita" + letra + j).style.border = "1px solid red";

            } else if (validarRepetidos(visita, "", cantPartidos, cantGrupos, j, i)) {

                entro = true;
                errorRepetido++;
                document.getElementById("txtVisita" + letra + j).style.border = "1px solid blue";

            } else {

                if (document.getElementById("txtVisita" + letra + j).style.borderColor != "blue") {

                    document.getElementById("txtVisita" + letra + j).style.border = "1px solid green";

                }

            }

        }

    }

    if (entro) {

        document.getElementById("pnlErrores").innerHTML = "Tiene los siguientes problemas:<br />";
        if (cantVacia > 0) {
            document.getElementById("pnlErrores").innerHTML += "- Hay " + cantVacia + " equipos que no estan completados<br />";
        }
        if (errorCarac > 0) {
            document.getElementById("pnlErrores").innerHTML += "- Hay " + errorCarac + " equipos que no estan dentro de 3 a 100 caracteres<br />";
        }
        if (errorRepetido > 0) {
            document.getElementById("pnlErrores").innerHTML += "- Hay equipos que estan repetidos<br />";
        }
    } else {

        /*var submitBtn = document.getElementById('txtGuardar');

         submitBtn.click();*/
        document.getElementById('txtGuardar').click();
        //document.location.reload();

    }

}

function retornarValor(valor, tipo) {

    var abecedario = ["nada", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        "N", "�", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

    if (tipo == "texto") {

        for (var k = 1; k <= 27; k++) {

            if (abecedario[k] == valor) {

                return k;

            }

        }


    } else {

        return abecedario[valor];

    }
}


function validarRepetidos(local, visita, cantPartidos, cantGrupos, partido, grupo) {

    var isRepit = false;
    if (local == visita) {

        isRepit = true;
    }

    for (var l = grupo; l <= cantGrupos; l++) {

        if (l > grupo) {
            p = 1;
        } else {
            p = partido + 1;
        }

        for (var m = p; m <= cantPartidos; m++) {

            var letra = retornarValor(l, "numero");
            var local1 = $('#txtLocal' + letra + m).val();
            var visita1 = $('#txtVisita' + letra + m).val();
            if (local == local1) {

                document.getElementById("txtLocal" + letra + m).style.border = "1px solid blue";
                isRepit = true;

            } else if (local == visita1) {

                document.getElementById("txtVisita" + letra + m).style.border = "1px solid blue";
                isRepit = true;

            }

        }

    }

    var libre = $('#txtEquipoLibre').val();

    if (local == libre) {

        isRepit = true;


    }

    return isRepit;


}

function prueba(valor) {


    alert("El valor: " + valor);
}