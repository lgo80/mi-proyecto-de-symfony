$(document).ready(function () {
    $('#btnSend').click(function (e) {
        confirmarMandarMensaje();
    });

    $('.spnEliominar').click(function (e) {

        e.preventDefault();

        var row = $(this).parents('h4');
        var id = row.data("id");
        var bm = row.data("bm");

        var form = $('#form-borrarmsg');

        var url = form.attr('action').replace('_ID', id).replace('_BM', bm);

        bootbox.confirm(message, function (res) {
            if (res == true) {

                document.forms['form-borrarmsg'].action = url;
                document.forms['form-borrarmsg'].submit();

            }
        });

    });

    $('.aTitulo').click(function (e) {

        var row = $(this).parents('h4');
        var leido = row.data("leido");
        var bm = row.data("bm");

        if (leido == false && bm == "r") {

            var id = row.data("id");

            var form = $('#form-marcarmsg');
            var url = form.attr('action').replace('_ID', id).replace('_BM', bm);

            document.forms['form-marcarmsg'].action = url;
            document.forms['form-marcarmsg'].submit();

        }

    });
});


function confirmarMandarMensaje() {

    bootbox.confirm({
        title: "Enviar mensaje",
        message: "Desea enviar el mensaje creado?",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                document.frmVerMensajes.submit();

            }
        }
    });

}

function responder(usuario, titulo) {

    document.getElementById("recipient-name").value = usuario;
    document.getElementById("affair-text").value = "re: " + titulo;

    document.getElementById("btnAbrirEnviar").click();

}

function marcarLeido(usuario, titulo) {


}