$(document).ready(function () {

    $('#cmbGuardarAbajo').click(function (e) {
        confirmarCrearTorneo(
            "Desea confirmar los resultados de la fecha?",
            "Los resultados se pueden cambiar hasta la fecha de cierre!",
            "guardar"
        );
    });
    $('#cmbGuardarArriba').click(function (e) {
        confirmarCrearTorneo(
            "Desea confirmar los resultados de la fecha?",
            "Los resultados se pueden cambiar hasta la fecha de cierre!",
            "guardar"
        );
    });

    $('#cmbEditarArriba').click(function (e) {
        habilitarCamposResultados(false);
    });
    $('#cmbEditarAbajo').click(function (e) {
        habilitarCamposResultados(false);
    });
    $('#spnOk').click(function (e) {
        e.preventDefault();

        var respuesta = validarValores();

        if (respuesta == true) {

            var titulo = ($('#exampleModalLabel1').hasClass('ocultar'))
                ? '�Desea modificar el nombre de la fecha?'
                : "�Desea modificar la fecha y hora de la fecha?";
            confirmarCrearTorneo(
                titulo,
                "�Los resultados se pueden cambiar hasta la fecha de cierre!",
                "modificar"
            )
            ;
        } else {

            var elemento = ($('#exampleModalLabel1').hasClass('ocultar'))
                ? "pError2" : "pError1";
            document.getElementById(elemento).innerHTML
                = respuesta;
            elemento = ($('#exampleModalLabel1').hasClass('ocultar'))
                ? "spnError2" : "spnError1";
            $("#" + elemento).removeClass("ocultar");
        }

    });
    $('.btnModifFecha').click(function (e) {

        var row = $(this);
        var lugar = row.data("lugar");
        $(".pnlFooterBtn").data('lug', lugar);

        ocultarValores("dia");
        ponerEnBlanco();
        if ($(".mdlCambiarFecha").hasClass("ocultar")) {
            $(".mdlCambiarFecha").removeClass("ocultar");
        } else {
            $(".mdlCambiarFecha").addClass("ocultar");
        }

    });
    $('.btnModifNombreFec').click(function (e) {

        var row = $(this);
        var lugar = row.data("lugar");
        $(".pnlFooterBtn").data('lug', lugar);

        ocultarValores('nombre');
        ponerEnBlanco();
        if ($(".mdlCambiarFecha").hasClass("ocultar")) {
            $(".mdlCambiarFecha").removeClass("ocultar");
        } else {
            $(".mdlCambiarFecha").addClass("ocultar");
        }

    });
    $('#spnCancel').click(function (e) {
        $(".mdlCambiarFecha").addClass("ocultar");
    });
});


function confirmarCrearTorneo(titulo, mensaje, metodo) {

    bootbox.confirm({
        title: titulo,
        message: mensaje,
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if (result == true) {

                accion(metodo);

            }
        }
    });

}

function accion(metodo) {

    if (metodo == "guardar") {
        document.frmResultadosTorneo.submit();
        habilitarCamposResultados(true);
    } else {

        procesarAjax("#form-modificarfecha");
    }

}

function habilitarCamposResultados(isHabilitar) {

    $(".txtLocalRes").removeAttr("readonly");
    $(".txtVisitaRes").removeAttr("readonly");
    document.getElementById("cmbGuardarAbajo").disabled = false;
    document.getElementById("cmbGuardarArriba").disabled = false;

}

function habilitarDefinicionPenales(isPenalty, golVisitante, valorLocalIda,
                                    valorVisitaIda, grupo, partido, isHabilitar) {

    var isGanoAlguien = false;

    if (!isPenalty) {
        var valorLocalVuelta = $('#txtResultadoLocal' + grupo + partido).val();
        var valorVisitaVuelta = $('#txtResultadoVisita' + grupo + partido).val();

        var isValid = devolverResultadosValidos(valorLocalVuelta, valorVisitaVuelta);

        if (isValid) {
            var valorEnTotalLocal = parseInt(valorLocalVuelta) + parseInt(valorVisitaIda);
            var valorEnTotalVisita = parseInt(valorVisitaVuelta) + parseInt(valorLocalIda);

            if (valorEnTotalLocal == valorEnTotalVisita) {

                if (golVisitante) {

                    valorEnTotalLocal = parseInt(valorEnTotalLocal) + parseInt(valorVisitaIda);
                    valorEnTotalVisita = parseInt(valorEnTotalVisita) + parseInt(valorVisitaVuelta);

                }

                if (isHabilitar == "true") {

                    deshabilitarPenales(grupo, partido, (valorEnTotalLocal != valorEnTotalVisita), false);

                }


                isGanoAlguien = (valorEnTotalLocal != valorEnTotalVisita);

            } else {

                isGanoAlguien = true;

            }

            eliminarClase(grupo, partido);
            if (isGanoAlguien) {

                var equipoGanador = devolverGanador(isGanoAlguien, grupo, partido,
                    valorEnTotalLocal, valorEnTotalVisita);

                $("#" + equipoGanador).addClass("negrita");
                deshabilitarPenales(grupo, partido, true, true);

            }
        } else {

            eliminarClase(grupo, partido);
            deshabilitarPenales(grupo, partido, true, true);

        }

        actualizarGlobal(valorLocalIda, valorVisitaIda, valorLocalVuelta,
            valorVisitaVuelta, isValid, grupo, partido);

    }

}

function actualizarGlobal(valorLocalIda, valorVisitaIda, valorLocalVuelta,
                          valorVisitaVuelta, isValid, grupo, partido) {

    var valorGlobal = (isValid)
        ? "Global: " + (parseInt(valorVisitaIda) + parseInt(valorLocalVuelta))
    + " - " + (parseInt(valorLocalIda) + parseInt(valorVisitaVuelta))
        : "Global: " + valorVisitaIda + " - " + valorLocalIda;

    document.getElementById("lblGlobal" + grupo + partido).innerHTML = valorGlobal;

}

function eliminarClase(grupo, partido) {

    $("#lblLocal" + grupo + partido).removeClass("negrita");
    $("#lblVisita" + grupo + partido).removeClass("negrita");
}

function deshabilitarPenales(grupo, partido, isHabilitar, isPonerBlanco) {

    document.getElementById('txtValorDefinicionLocal' + grupo + partido).readOnly
        = isHabilitar;
    document.getElementById('txtValorDefinicionVisita' + grupo + partido).readOnly
        = isHabilitar;

    if (isPonerBlanco) {
        document.getElementById("txtValorDefinicionLocal" + grupo + partido).value = "";
        document.getElementById("txtValorDefinicionVisita" + grupo + partido).value = "";
    }

}

function devolverResultadosValidos(valorLocalVuelta, valorVisitaVuelta) {

    var isValid = false;

    if (esEntero(valorLocalVuelta) && esEntero(valorVisitaVuelta)) {

        if (parseInt(valorLocalVuelta) >= 0 && parseInt(valorVisitaVuelta) >= 0) {

            isValid = true;

        }
    }

    return isValid;
}

function devolverGanador(isGanoAlguien, grupo, partido, valorLocal, valorVisita) {

    if (isGanoAlguien) {

        return (valorLocal > valorVisita)
            ? "lblLocal" + grupo + partido : "lblVisita" + grupo + partido;

    } else {

        var valorLocalPenales = $('#txtValorDefinicionLocal' + grupo + partido).val();
        var valorVisitaPenales = $('#txtValorDefinicionVisita' + grupo + partido).val();

        if (devolverResultadosValidos(valorLocalPenales, valorVisitaPenales)) {

            return (valorLocalPenales > valorVisitaPenales)
                ? "lblLocal" + grupo + partido : "lblVisita" + grupo + partido;

        }


    }
}

function procesarGanador(isGanoAlguien, grupo, partido) {

    eliminarClase(grupo, partido);

    var equipoGanador = devolverGanador(isGanoAlguien, grupo, partido,
        0, 0);

    $("#" + equipoGanador).addClass("negrita");
}

function procesarAjax(formEntrada) {

    var row = $(".pnlFooterBtn");

    var idf = row.data("idf");
    var lugar = row.data("lug");

    var form = $(formEntrada);
    var url = form.attr('action').replace('_IDFECHA', idf).replace('_LUGAR', lugar);

    var data = form.serialize();

    $.post(url, data, function (result) {

        if (result.error) {

            var elemento = ($('#exampleModalLabel1').hasClass('ocultar'))
                ? "pError2" : "pError1";
            document.getElementById(elemento).innerHTML
                = result.mensaje;
            elemento = ($('#exampleModalLabel1').hasClass('ocultar'))
                ? "spnError2" : "spnError1";
            $("#" + elemento).removeClass("ocultar");

        } else {

            var elemento = result.elemento;
            document.getElementById(elemento).innerHTML = result.mensaje;
            $(".mdlCambiarFecha").addClass("ocultar");

        }

    }).fail(function () {
        document.getElementById("spnError").innerHTML
            = "Hubo un error en la modificaci�n";
        $("#spnError").removeClass("ocultar");
    });


}

function validarValores() {

    if ($('#exampleModalLabel1').hasClass('ocultar')) {

        if (!$('#txtNombreFecha').val()) {

            return "&#161Debe completar el nombre de la fecha!";

        } else if ($('#txtNombreFecha').val().length > 25) {

            return "&#161El nombre de la fecha debe tener menos de 25 caracteres!";

        }

    } else {

        if (!$('#txtFechaInicio').val() && !$('#txtHoraInicio').val()) {

            return "&#161Debe completar la fecha y hora!";

        } else if (!($('#txtFechaInicio').val())) {

            return "&#161Debe completar la fecha!";

        } else if (!($('#txtHoraInicio').val())) {

            return "&#161Debe completar la hora!";

        }

    }


    return true;

}

function ocultarValores(lugar) {

    if (lugar == 'nombre') {
        $("#exampleModalLabel2").removeClass("ocultar");
        $("#pnlMFValores2").removeClass("ocultar");
        $("#pnlMFValores1").addClass("ocultar");
        $("#exampleModalLabel1").addClass("ocultar");
    } else {
        $("#exampleModalLabel2").addClass("ocultar");
        $("#pnlMFValores2").addClass("ocultar");
        $("#pnlMFValores1").removeClass("ocultar");
        $("#exampleModalLabel1").removeClass("ocultar");
    }

}

function ponerEnBlanco() {
    $("#txtFechaInicio").val("");
    $("#txtHoraInicio").val("");
    $("#txtNombreFecha").val("");
}