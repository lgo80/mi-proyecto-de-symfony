$(document).ready(function () {
    $('#spnClose').click(function (e) {
        habilitarAccesoApuesta(false);
    });
    $('#aTitAA').click(function (e) {
        habilitarAccesoApuesta(true);
    });
    $('#pnlAccesoPizarraMsg').click(function (e) {
        habilitarAccesoPizarraMsg();
    });
});

function habilitarAccesoApuesta(isHabilitar) {

    if (isHabilitar == true) {
        $("#pnlAccesoApuesta").removeClass("ocultar");
    } else {
        $("#pnlAccesoApuesta").addClass("ocultar");
    }


}

function habilitarAccesoPizarraMsg() {

    if ($("#pnlPizarraMsg").hasClass('ocultar')){
        $("#pnlPizarraMsg").removeClass("ocultar");
    }else{
        $("#pnlPizarraMsg").addClass("ocultar");
    }

}